Архитектура высоких нагрузок
------------------------------------

Проект разрабатывается в рамках курса "Архитектура высоких нагрузок"

1. [Лекция 1 - Введение](introduction/README.md)
2. [Лекция 2 - Проблемы высоких нагрузок](hl_problems/README.md)
3. [Лекция 3 - Нагрузочное тестирование](load_testing/README.md)
4. [Лекция 4 - Индексы 1](indexes1/README.md)
5. [Лекция 5 - Индексы 2](indexes2/README.md)
6. [Лекция 6 - Репликация 1](replication1/README.md)
7. [Лекция 7 - Репликация 2](replication2/README.md)
8. [Лекция 8 - Репликация 3](replication3/README.md)
9. [Лекция 9 - Шардинг 1](sharding1/README.md)
10. [Лекция 10 - Шардинг 2](sharding2/README.md)
11. [Лекция 11 - Кеширование](caching/README.md)
12. [Лекция 12 - ACID Транзакции](transaction_ACID/README.md)
13. [Лекция 13 - Асинхронное выполнение](async_handling/README.md)
14. [Лекция 14 - In memory storage](InMemoryDB/README.md)
15. [Лекция 15 - OLTP и OLAP](OLTP_OLAP/README.md)
16. [Лекция 16 - OLTP и OLAP (clickhouse)](OLTAP_clickhouse/README.md)
17. [Лекция 17 - HTTP часть 1](http_1/README.md)
18. [Лекция 18 - HTTP часть 2](http_2/README.md)