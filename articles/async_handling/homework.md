Домашнее задание
------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> Домашнее задание

### Задание

Необходимо написать систему диалогов между пользователями. 
Обеспечить горизонтальное масштабирование хранилищ на запись с помощью шардинга. Предусмотреть:
Возможность решардинга
“Эффект Леди Гаги” (один пользователь пишет сильно больше среднего)
Наиболее эффективную схему.

Разработать ленту новостей. 
Создается отдельная страница, куда пишутся все обновления друзей. 
Для этого нужно хранить подписчиков. 
Лента формируется на уровне кешей. 
Формирование ленты производить через постановку задачи в очередь на часть подписчиков, чтобы избежать эффекта леди Гаги. 
В ленте держать последние 1000 обновлений друзей. 
Лента должна кешироваться.


### Выполнение

#### План

Сперва реализуем ленту новостей без шардинга
- Написать миграции БД для подписок пользователей и для постов на странице пользователя
- Добавить примитивный функционал в приложении для подписки на пользователя и оставлении постов на странице пользователя
- Добавить сервис который будет формировать ленту для пользователя асинхронно и класть в кэш
- Добавить страницу с лентой новостей, реализовать чтение постов из кеша
- Реализовать шардирование данных

#### Миграции БД

Таблица подписок:

```mysql
CREATE TABLE network_member_followers(
    member_id VARCHAR(36) NOT NULL,
    follower_id VARCHAR(36) NOT NULL,
    CONSTRAINT nmf_udx_member_follower UNIQUE INDEX (member_id, follower_id),
    CONSTRAINT nmf_fk_member_follower_member_id FOREIGN KEY (member_id) REFERENCES user_users(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci
```

Внешний ключ для follower_id не создаем так как при шардировании подписчик пользователя не факт что будет на одном шарде.

Таблица постов:

```mysql
CREATE TABLE network_posts(
    id VARCHAR(36) NOT NULL,
    target_user_id varchar(36) not null,
    author_id varchar(36) not null,
    content text,
    CONSTRAINT network_post_id_idx UNIQUE INDEX (id) USING HASH,
    CONSTRAINT network_post_fk_target_user_id FOREIGN KEY (target_user_id) REFERENCES user_users(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci
```

target_user_id - идентификатор пользователя на стене которого будет публиковаться пост.
Также для author_id не создаем внешний ключ, так как автор поста может быть на другом шарде


#### Функционал подписки и добавления поста

Приводимый код с учетом шардинга, так как отчет писался после реализации дз

Подписка:
- [Controller](./../../app/src/Controller/Member/MemberController.php#L85)
- [Сервис](./../../app/src/Model/Network/UseCase/Member/Follow/Handler.php)
- [Репозитории](./../../app/src/Model/Network/Repository/Member/SqlMemberRepository.php)

Пост пользователя:
- [Controller](./../../app/src/Controller/Member/PostsController.php)
- [Сервис](./../../app/src/Model/Network/UseCase/Post/Create/Handler.php)
- [Репозитории](./../../app/src/Model/Network/Repository/Post/SQLPostRepository.php)


#### Схема асинхронной обработки RabbitMQ

Общее для producer и consumer
- Создаем exchange c типом direct
- Создаем очередь с флагом durable
- Связываем exchange и очередь

Producer:
- Отправляем сообщения в exchange

Consumer:
- Подписывается на очередь и выполняет обработку, после успешной обработки, подтверждает обработку(ack)

#### Сервис формирующий ленту новостей

Описание кейса:
- После добавления нового поста, в очередь(RabbitMQ) отправляется сообщение с идентификатором поста 
    и с идентификатором пользователя на странице которго был добавлен пост
- Сервис подписывается на очередь и обрабатывает сообщения.
    Получает из базы данных всех подписчиков пользователя, обходит их в цикле и для каждого подписцика проверяет,
    есть ли в кеше лента. Если лента в кеше есть, то новый пост добавляется в первую позицию в ленте. Если ленты нет в кеше,
    то получаем всех поьзователей на кого подписан подписчик, получаем их последние посты и кладем в кеш
- Если у пользователя нет ленты в кеше и он заходит на страницу новостей, то приложение отправляет сообщение в очередь с идентификатором пользователя и выводит какое-нибудь сообщение пользователю.
    а сервис при получении такого сообщения получает последние новости от всех на кого он подписан и кладет в кеш.
- Также при регистрации на сайте отправить сообщение в очередь на формирование ленты заранее.

Код:
- [Сервис консьюмер](./../../gosrv/src/news-consumer)
- [Подключение к RabbitMQ](./../../app/src/Model/Network/Service/Publisher/Post/AmqpPostPublisherFactory.php)
- [Отправка ссобщения](./../../app/src/Model/Network/Service/Publisher/Post/AmqpPostPublisher.php)

Для хранения ленты используется Redis


#### Страница с лентой новостей

Код:
- [Controller](./../../app/src/Controller/Network/NewsController.php)
- [Сервис](./../../app/src/Model/Network/Service/News/NewsService.php)


При 2 млн пользователей и несколько сотен тысяч постов и несколько сотен подписок,
запрос на получение новостей для одного пользователя занима 40 сек. после добавления индексов

```mysql
SELECT id, content, target_user_id, author_id, uu.name as author_name, created_at
FROM network_posts np
INNER JOIN user_users uu ON uu.id = author_id 
WHERE target_user_id IN ((SELECT member_id FROM network_member_followers WHERE follower_id = ?))
	  OR (np.author_id = ? OR np.target_user_id = ?)
ORDER BY created_at DESC
LIMIT ? OFFSET ?
```

#### Шардирование данных

Выбрал следующую схему:
- 2 шарда
- шаридрования по хешу ключа
- умный клиент
- обычный mysql, без каких-либо дополнительных решений

Добавил класс DBController, который имеет коллекцию из mysql-коннектов 
и по ключю определяет номер коннекта

```go
package db

import (
	"crypto/md5"
	"encoding/hex"
)

type DBController struct {
	Pool []*DbConnection
}

func NewDbController() (DBController, error) {
	dbc := DBController{}
	return dbc, nil
}

func (dbc *DBController) GetSize() int {
	return len(dbc.Pool)
}

func (dbc *DBController) AddShard(conn *DbConnection) {
	dbc.Pool = append(dbc.Pool, conn)
}

func (dbc *DBController) Hash(key string) int {
	hasher := md5.New()
	bytes := []byte(key)
	hasher.Write(bytes)
	strmd := hex.EncodeToString(hasher.Sum(nil))
	bytes = []byte(strmd)
	number := ((int(bytes[3]) & 0xFF) << 24) | ((int(bytes[2]) & 0xFF) << 16) | ((int(bytes[1]) & 0xFF) << 8) | (int(bytes[0]) & 0xFF)

	return number
}

func (dbc *DBController) GetShardNumber(key string) int {
	hn := dbc.Hash(key)
	return (hn % dbc.GetSize()) + 1 
}

func (dbc *DBController) GetShardByKey(key string) *DbConnection {
	num := dbc.GetShardNumber(key)
	return dbc.Pool[num-1] 
}
```

```php
<?php
declare(strict_types=1);

namespace App\Tool;

use Doctrine\DBAL\Connection;

class DBController
{
    private $nodes;

    /**
     * DBController constructor.
     * @param Connection[] $nodes
     */
    public function __construct(array $nodes)
    {
        foreach ($nodes as $node) {
            $this->addShard($node);
        }
    }

    public function addShard(Connection $connection)
    {
        $this->nodes[] = $connection;
    }

    public function getShardNumber($key) {
        $hashId = $this->hash($key);
        return ($hashId % $this->getSize()) + 1;
    }

    public function getShardConnection($key): Connection
    {
        $num = $this->getShardNumber($key);
        return $this->nodes[$num-1];
    }

    protected function hash(string $key) {
        $md = md5($key);
        $bytes = unpack('C*', $md);

        return (($bytes[4] & 0xFF) << 24) | (($bytes[3] & 0xFF) << 16) | (($bytes[2] & 0xFF) << 8) | ($bytes[1] & 0xFF);
    }

    /**
     * @return Connection[]
     */
    public function nodes(): array
    {
        return $this->nodes;
    }

    public function getSize():int
    {
        return count($this->nodes);
    }
}
```

В основном в качестве ключа шардирования используется идентификатор пользователя. 
Пользователь, подписки на пользователя и его посты хранятся на одном шарде

Переделал почти все запросы, пришлось избавиться от join`ов. Некоторые запросы разделил на две отдельные запросы, 
для некоторых запросов провел денормализацию данных.

Добавил колонку с именем автора поста в таблице постов

```mysql
ALTER TABLE network_posts ADD COLUMN author_name varchar(255)  NULL;
```

Данные распределяются равномерно:

При генерации 2мл+ пользователей
- в первой ноде - 999005 пользователей
- во второй ноде - 1001095 пользователей

Мой пользователь находится на первом шарде, пользователи на которых я подписан распределены между шардами и на каждом шарде примерно почти по 500

На первом шарде:
```mysql
select count(1) from network_member_followers where follower_id = '8aaaed1d-704e-416c-a19f-a1b3c13d692f';
+----------+
| count(1) |
+----------+
|      502 |
+----------+
1 row in set (0.00 sec)
```

На втором шарде:
```mysql
select count(1) from network_member_followers where follower_id = '8aaaed1d-704e-416c-a19f-a1b3c13d692f';
+----------+
| count(1) |
+----------+
|      500 |
+----------+
1 row in set (0.00 sec)
```

Количество постов на первом шарде:
```mysql
mysql> select count(1) from network_posts;
+----------+
| count(1) |
+----------+
|   704203 |
+----------+
1 row in set (1.14 sec)
```

Количество постов на втором шарде:
```mysql
mysql> select count(1) from network_posts;
+----------+
| count(1) |
+----------+
|   700000 |
+----------+
1 row in set (1.73 sec)
```

Замерим время выполнения кода из сервиса по получению постов для пользователя:
```go
...
    start := time.Now().UnixNano() / int64(time.Millisecond)
    posts, err := postsRepository.GetMemberNews(memberId, service.Cache.Limit ,0)
    end := time.Now().UnixNano() / int64(time.Millisecond)
    raz := end - start
    fmt.Println(raz)
...
```
Вывод:
```bash
2052
```

Грубо говоря 2.052 секунды на получение новостей из базы для моего пользователя с его количеством подписок

Не знаю именно шардирование такой эффект дало или нет. Предполагаю что на резлуьтат также повлияло избавление от джоинов и денормализация таблицы постов.

Буду развивать и исследовать тему дальше

### TODO

- Исследовать отдельно тему шардирования подробнее
- Продолжить тему асинхронной обработки более подробно
- Реализовать очередь через Apache Kafka
- Увеличить число консьюмеров
- Предусмотреть ошибочную обработку сообщения, действовать зависимости от характера ошибки. 
    Если ошибка временная, то перепубликуем сообщение в exchange увеличивая ttl. 
    Если ttl превышен или ошибка невременная, то уничтожаем сообщение из очереди.
- Также расмотреть реализацию ограничения по количеству попыток обработки

