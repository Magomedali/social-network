Очереди и асинхронная обработка
------------------------

-> [Главная](../../README.md) -> Лекция

- [Домашнее задание](./homework.md)


### Доп. материал

- [CMAK (Cluster Manager for Apache Kafka, previously known as Kafka Manager)](https://github.com/yahoo/CMAK)
- [Confluent Platform Quick Start](https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html)
- [RabbitMQ Tutorials](https://www.rabbitmq.com/getstarted.html)
- [RabbitMQ против Kafka: два разных подхода к обмену сообщениями](https://habr.com/ru/company/itsumma/blog/416629/)