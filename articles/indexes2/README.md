Урок 5 - Индексы Часть 2
--------------------------------------------------

-> [Главная](../../README.md) -> Лекция

## Содержание:

- [Домашнее задание](homework.md)


### Виды индексов в MySql

- Btree - более распространенный и используется для дисковый бд
- Rtree - для индексации гео данных
- Hash - (не работают для диапозонных сравнении)
- FULLTEXT - для полнотекстового поиска

Btree индекс не указывает на структуру хранения индекса, 
то есть btree индекс не говорит о том что индекс имеет структуру btree дерево


### Btree индекс

В MySql Btree индекс хранится в структуре B+tree.

Структура B+tree отличается от Btree тем что имеет двунаправленные связи между соседними блоками одного уровня.
B+tree упрощает поиск по диапозону, тем что переход к соседнему блоку осуществляется за одну операцию, 
а не через общего родителя как в Btree структуре.


### Отличия движков MyIsam и Innodb

- в myisam в индексе в качестве значения хранится указатель на область памяти на диске, где храниться кортеж;
- в innodb индекс хранит в качестве значения значение первичного ключа кортежа.

В innodb таблице если первичный ключ отсутствует, то бд сама создаст суррогатный ключ - уточнить!!!
Поэтому рекомендуется для innodb таблиц самостоятельно создавать какой-нибудь первичный ключ.



### Практика