Изучение  Kubernetes
===========================
-> [Главная](../../README.md) -> Изучение  Kubernetes


### Материал:

- [Обзор и сравнение контроллеров Ingress для Kubernetes](https://habr.com/ru/company/flant/blog/447180/)
- [Kubernetes Ingress — Simplified](https://medium.com/swlh/kubernetes-ingress-simplified-e0b9dc32f9fd)
- [KONG — The Microservice API Gateway](https://medium.com/@far3ns/kong-the-microservice-api-gateway-526c4ca0cfa6)
