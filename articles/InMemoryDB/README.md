In Memory database
------------------------

-> [Главная](../../README.md) -> Лекция

- [Домашнее задание](./homework.md)

### Цель занатия:

- Понять зачем нужны In-memory СУБД
- Изучить базовые понятия tarantool
- Рассмотреть особенности работы tarantool в боевом окружении
- Изучить базовые операции работы с данными
- Научиться использовать транзакции
- Посмотреть, как использовать тарантул как реплику MySQL

### Для чего нужны

- Ускорение работы
- Избегаем проблем кешей
- Экономия денег
- Горизонтальное масштабирование

### Tarantool

- написан в Mail.ru на C
- хранит данные в памяти (основной сценарий)
- ACID
- однопоточный (на самом деле нет)
- полноценный lua application-server
- умеет репликацию
- умеет шардинг

### Когда не использовать тарантул?

- Хранение логов
- Аналитические запросы
- Тяжелые запросы

### Терминология

- space - таблица
- tuple - строка таблицы


**Создание space:**

```lua
 box.schema.space.create('myspace', {if_not_exists=true})
```

**Создание индекса:**

```lua
space.myspace:create_index('primary', {type="TREE", unique=true, parts={1, 'unsigned', 2, 'string'},  if_not_exists=true})
```
parts={1, 'unsigned', 2, 'string'} - Индекс из двух колонок, тип первой unsigned, тип второй  string

**Параметры индексов:**
- type - тип индекса: TREE, HASH, RTREE, BITMAP
- unique - уникальность индекса
- parts - индексные поля.
- Важно - можно использовать префикс индекса

### Вставка данных 

```cmd
 > box.space.myspace:insert({"test", 3, 6})
---
- error: 'Tuple field 1 type does not
match one required by operation: expected unsigned'
...
> box.space.myspace:insert({1, 'qwerty', 6})
---
- [1, 'qwerty', 6]
...
> box.space.myspace:insert({1, 10, 6})
---
- error: Duplicate key exists in unique index
'primary' in space 'myspace'
...
> box.space.myspace:insert({2, 10, 6})
---
- [2, 10, 6]
...
> box.space.myspace:insert({3, 10, 6, 98})
---
- [3, 10, 6, 98]
...
```

### Вставка данных - выводы

- В кортеже может быть переменное число столбцов
- В кортеж можно вставлять сложные типы данных
- Типы данных в неиндексных полях могут несовпадать.

### Изменение данных

```bash
 > box.space.myspace:update({2}, {{'+', 3, 1}})
---
- [2, 10, 7]
...
> box.space.myspace:update({2}, {{'+', 3, 1}})
---
- [2, 10, 8]
...
> box.space.myspace:update({2}, {{'+', 3, 10}})
---
- [2, 10, 18]
...
> box.space.myspace:update({2}, {{'-', 3, 3}})
---
- [2, 10, 15]
...
> box.space.myspace:update({2}, {{'=', 3, 148}})
---
- [2, 10, 148]
```
**Важно: менять можно только по уникальному индексу.**


### Чтение данных

```bash
box.space.myspace:create_index('tindex', {type="TREE", parts={3, 'unsigned'}, unique=false})
box.space.myspace:create_index('hindex', {type="HASH", parts={1, 'unsigned'}})
box.space.myspace:select({})
box.space.myspace.index.tindex:select({})
box.space.myspace.index.tindex:select({148})
box.space.myspace.index.tindex:select({148}, {iterator='GT'})
box.space.myspace.index.tindex:select({148}, {iterator='GE'})
box.space.myspace.index.hindex:select({3}, {iterator='GE'})
```

### Другие операции

- replace
- delete
- upsert

### Хранимые процедуры

```bash
function myproc(id)
    local tup = box.space.myspace.index.tindex:select({id}, {iterator='GE'})
    return tup[2]
end
```

### Переключение контекста

Не забывайте, что тарантул однопоточен. 

Тарантул передает управление сам при:
- Модифицирующих операциях 
- Блокирующих системных вызовах

SELECT не передает управление

Во время долгих циклов пользуйтесь:

```bash
 fiber.yield()
```

### Транзакции

- Начало транзакции - box.begin()
- Откат транзакции - box.rollback()
- Подтверждение транзакции - box.commit()

**Важно: блокирующие операции откатывают транзакцию автоматически**

### Практика

Обзор сервиса профилей Почта@Mail.ru
[https://github.com/mailru/tntlua/blob/master/pro le17.lua](https://github.com/mailru/tntlua/blob/master/profile17.lua)

### На занятии

- Поняли зачем нужны In-memory СУБД
- Изучили базовые понятия tarantool
- Рассмотрели особенности работы tarantool в боевом окружении
- Изучили базовые операции работы с данными
- Научились использовать транзакции
- Посмотрели, как ипользовать тарантул как реплику MySQL

### Доп материал

- [Сравниваем Tarantool с Redis и Memcached](https://habr.com/ru/company/mailru/blog/352760/)
- [Tarantool: как сэкономить миллион долларов на БД / Денис Аникин (Mail.Ru)](https://otus.ru/learning/40437/#/)
- [Эффективное хранение: как мы из 50 Пб сделали 32 Пб](https://habr.com/ru/company/mailru/blog/316740/)
- [VShard — горизонтальное масштабирование в Tarantool](https://habr.com/ru/company/mailru/blog/436916/)
- [Репликация из MySQL в Tarantool](https://habr.com/ru/company/mailru/blog/323870/)
- [Что особенного в СУБД для данных в оперативной памяти / Константин Осипов (Tarantool)](https://www.youtube.com/watch?v=yrTF3qH8ey8)

