Лекция 15 - OLTP и OLAP
------------------------

-> [Главная](../../README.md) -> Лекция


#### Доп. материал:
- [Все о Data Vault](http://www.dwh-club.com/ru/dwh-bi-articles/data-vault-terminy-obekty-osnovy-arhitektury.html)
- [Введение в Data Vault](https://habr.com/ru/post/348188/)
- [Архитектура хранилищ данных: традиционная и облачная](https://habr.com/ru/post/441538/)
- [The Data Vault Architecture](https://bluesoft.com/en/data-vault-architecture-2/)
- [A brief introduction to two data processing architectures — Lambda and Kappa for Big Data](https://towardsdatascience.com/a-brief-introduction-to-two-data-processing-architectures-lambda-and-kappa-for-big-data-4f35c28005bb)
- [What is Kappa Architecture](https://milinda.pathirage.org/kappa-architecture.com/)
- [Applying the Kappa architecture in the telco industry](https://www.oreilly.com/content/applying-the-kappa-architecture-in-the-telco-industry/)
- [Lambda architecture для realtime-аналитики — риски и преимущества / Николай Голов (Avito)](https://www.youtube.com/watch?v=_-T0HQaUTWY&feature=emb_logo)


