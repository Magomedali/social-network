Урок 8 - Репликация Часть 3
--------------------------------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> Percona Cluster

### Задание
-  Провести тест на потерю транзакции, при сбое мастера в multi-master репликации
-  Провести тест на потерю транзакции, при сбое мастера в master-slave репликации


### Выполнение

### Multi-Master репликации

#### Исходное состояние:
- Три мастера в Percona кластере;
- Приложение работает с кластером через ProxySQL;
- Таблица постов network_posts пуста.

Состояние ProxySQL - все хосты находятся в группе записи(master):
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.00 sec)

mysql> select * from runtime_mysql_galera_hostgroups\G
*************************** 1. row ***************************
       writer_hostgroup: 10
backup_writer_hostgroup: 12
       reader_hostgroup: 11
      offline_hostgroup: 13
                 active: 1
            max_writers: 1000000
  writer_is_also_reader: 0
max_transactions_behind: 100
                comment: NULL
1 row in set (0.00 sec)
```

В кластере 3 ноды и пустая таблица network_posts, в которую будем записывать данные:
```mysql
mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 3                                    |
| wsrep_cluster_conf_id    | 35                                   |
| wsrep_cluster_size       | 3                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.01 sec)

mysql> SELECT COUNT(1) FROM network_posts;
+----------+
| count(1) |
+----------+
|        0 |
+----------+
1 row in set (0.00 sec)
```

Сценарии теста:
- запускаем команду выполняющую post http запрос на вставку записи 10000 раз параллельно 100
- выключаем одну из нод mysql в кластере
- фиксируем состояние кластера и ProxySQL
- восстанавливаем ноду
- проверяем состояние кластера и ProxySQL


Команда: 
```cmd
$ cat postfile.data
target_user_id=00434edf-76c9-4c0b-ae19-a5eff21883d9&actor=00434edf-76c9-4c0b-ae19-a5eff21883d9&content=multimastertest
$
$ ab -n 10000 -c 100 -r -p postfile.data -T 'application/x-www-form-urlencoded' -m POST -A web-ali@yandex.ru:12345 http://network/members/posts
```

Выключаем ноду:
```bash
% systemctl stop mysql
```

Смотрим состояние proxySQL:
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status  | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           | <node-2>     | 3306 | 0         | SHUNNED | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.01 sec)
```

Состояние кластера:
```mysql
mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 2                                    |
| wsrep_cluster_conf_id    | 36                                   |
| wsrep_cluster_size       | 2                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.01 sec)
```

Видим что одна нода выключена

Не останавливая команду записи, восстановим ноду
```bash
$ systemctl start mysql
```

Нода запустилась, заходим в ноду:
```bash
$ mysql -u user -p network
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

```

На этом моменте зависло... В кластере показывает что три ноды, но в ProxySQL нода до сих пор выключена

```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status  | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           | <node-2>     | 3306 | 0         | SHUNNED | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.01 sec)

mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 3                                    |
| wsrep_cluster_conf_id    | 39                                   |
| wsrep_cluster_size       | 3                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.00 sec)
```

Делаю вывод что, при восстановлении ноды, нода включается в кластер и начинает догонят другие ноды из кластера 
и пока нода догоняет остальные ноды в ProxySQL она в статусе оффлайн. 
Когда нода догоняет остальные ноды ProxySQL переводит ноду в стату онлайн.

Остановил команду на запись и попробовал повторно зайти. Зашли и смотрим количество записей на всех нодах
```mysql
mysql> select count(1) from network_posts;
+----------+
| count(1) |
+----------+
|     1585 |
+----------+
1 row in set (0.00 sec)
```

```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.00 sec)
```

Все ноды в рабочем состоянии и на всех нодах количество записей одинаковое. 

Отчет теста apache benchmark:
```bash
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking network (be patient)


Server Software:        nginx/1.15.12
Server Hostname:        network
Server Port:            80

Document Path:          /members/posts
Document Length:        458 bytes

Concurrency Level:      100
Time taken for tests:   754.971 seconds
Complete requests:      1491
Failed requests:        6
   (Connect: 0, Receive: 0, Length: 6, Exceptions: 0)
Non-2xx responses:      1491
Total transferred:      1325952 bytes
Total body sent:        542531
HTML transferred:       685116 bytes
Requests per second:    1.97 [#/sec] (mean)
Time per request:       50635.190 [ms] (mean)
Time per request:       506.352 [ms] (mean, across all concurrent requests)
Transfer rate:          1.72 [Kbytes/sec] received
                        0.70 kb/s sent
                        2.42 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   24  18.6     18     333
Processing:   825 48941 6242.7  50054   58370
Waiting:      816 48939 6242.8  50052   58368
Total:        851 48965 6241.8  50073   58532

Percentage of the requests served within a certain time (ms)
  50%  50072
  66%  51398
  75%  52018
  80%  52633
  90%  53335
  95%  53763
  98%  54168
  99%  54390
 100%  58532 (longest request)

```

Запись воспроизводилась очень медленно. И не смотря на то что запросов завершенных 1491, а записей в таблицах 1585

Попробуем повторить все заново, только без конкурентных запросов

```bash
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking network (be patient)


Server Software:        nginx/1.15.12
Server Hostname:        network
Server Port:            80

Document Path:          /members/posts
Document Length:        458 bytes

Concurrency Level:      1
Time taken for tests:   166.590 seconds
Complete requests:      270
Failed requests:        1
   (Connect: 0, Receive: 0, Length: 1, Exceptions: 0)
Non-2xx responses:      270
Total transferred:      240084 bytes
Total body sent:        92411
HTML transferred:       124033 bytes
Requests per second:    1.62 [#/sec] (mean)
Time per request:       617.002 [ms] (mean)
Time per request:       617.002 [ms] (mean, across all concurrent requests)
Transfer rate:          1.41 [Kbytes/sec] received
                        0.54 kb/s sent
                        1.95 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       15   20  11.6     17     109
Processing:   466  595 108.9    595    1402
Waiting:      465  593 108.6    593    1400
Total:        482  616 111.5    614    1434

Percentage of the requests served within a certain time (ms)
  50%    614
  66%    615
  75%    617
  80%    625
  90%    716
  95%    820
  98%    937
  99%   1075
 100%   1434 (longest request)
```

```mysql
mysql> select count(1) from network_posts;
+----------+
| count(1) |
+----------+
|      270 |
+----------+
1 row in set (0.00 sec)
```

Видим что при 270 заверщенных запросов 270 записей в таблице

#### Итого при multi-master репликации во время падения одной из ноды:
- при конкурентных http запросах есть вероятность дубликации данных
- потерянных транзакции нет
- при последовательных http запросах аномалий не обнаружено


### Single-Master репликации

Переконфигурировали кластер с помощью proxysql-admin, переключили на master-slave схему

```bash
$ proxysql-admin --config-file /etc/proxysql-admin.conf --mode singlewrite --enable
```

Смотрим состояние кластера на proxysql:
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 12           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 12           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
6 rows in set (0.00 sec)
```

Видим что 3 нода является мастером остальные используются для чтения и бэкапов

Сценария теста:
- Запускаем выполнение 1000 запросов последовательно
- Выключаем мастер(нода-3)
- Фиксируем состояние кластера
- Восстанавливаем ноду-3
- Фиксируем состояние кластера и проверяем потерю транзакции


Запускаем тест:

```bash
$ ab -n 1000 -p postfile.data -T 'application/x-www-form-urlencoded' -m POST -A web-ali@yandex.ru:12345 http://network/members/posts > report
```

Выключаем мастер
```bash
$ systemctl stop mysql
```

Смотрим состояние proxysql
```mysql

mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status  | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 12           | <node-1>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           | <node-3>     | 3306 | 0         | SHUNNED | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-2>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-1>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-2>     | 3306 | 0         | ONLINE  | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+---------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
5 rows in set (0.00 sec)
```

Смотрим состояние кластера
```mysql
mysql> show status like 'wsrep_c%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cert_deps_distance | 4.330152                             |
| wsrep_commit_oooe        | 0.000000                             |
| wsrep_commit_oool        | 0.001285                             |
| wsrep_commit_window      | 1.003303                             |
| wsrep_cert_index_size    | 69                                   |
| wsrep_cert_bucket_count  | 62244                                |
| wsrep_causal_reads       | 0                                    |
| wsrep_cert_interval      | 0.023491                             |
| wsrep_cluster_weight     | 2                                    |
| wsrep_cluster_conf_id    | 54                                   |
| wsrep_cluster_size       | 2                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
| wsrep_connected          | ON                                   |
+--------------------------+--------------------------------------+
14 rows in set (0.01 sec)
```

Видим что в кластере остались две ноды, мастером стала вторая нода, а 3 нода в оффлайне

Остановили запись и посмотрим сколько запросов выполнилось и сколько записей в таблице
```bash
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking network (be patient)


Server Software:        nginx/1.15.12
Server Hostname:        network
Server Port:            80

Document Path:          /members/posts
Document Length:        458 bytes

Concurrency Level:      1
Time taken for tests:   234.216 seconds
Complete requests:      392
Failed requests:        1
   (Connect: 0, Receive: 0, Length: 1, Exceptions: 0)
Non-2xx responses:      392
Total transferred:      348420 bytes
Total body sent:        134013
HTML transferred:       179909 bytes
Requests per second:    1.67 [#/sec] (mean)
Time per request:       597.490 [ms] (mean)
Time per request:       597.490 [ms] (mean, across all concurrent requests)
Transfer rate:          1.45 [Kbytes/sec] received
                        0.56 kb/s sent
                        2.01 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   21  23.8     17     438
Processing:   406  576 120.1    553    1303
Waiting:      406  573 120.2    551    1303
Total:        422  597 124.1    591    1324

Percentage of the requests served within a certain time (ms)
  50%    591
  66%    614
  75%    615
  80%    617
  90%    718
  95%    881
  98%   1000
  99%   1126
 100%   1324 (longest request)
```

```mysql
mysql> select count(1) from network_posts;
+----------+
| count(1) |
+----------+
|      392 |
+----------+
1 row in set (0.00 sec)
```

Видим что записей в таблице 392, столько же сколько завершенных запросов и 1 запрос зафейленный.
Несмотря на то что 1 запрос зафейлился, все записи дошли до базы и нет потерянных транзакций

Данную сценарию теста провел 5-6 раз и в некоторых случаях были потери транзакции

Вывод:
- Минимум один запрос фейлится при падении мастера
- В некоторых случаях не было обнаружено потеря транзакции и бил случаи с потерей транзакции
- Возможно можно добиться лучшего эффекта настроив более грамотно percona кластер и
  с помощью вертикального масштабирования (Мастер нода, proxysql и приложение на одно ядерном сервере работают)
