Урок 8 - Репликация Часть 3
--------------------------------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> Percona Cluster

### Задание
-  Развернуть Percona Cluster + ProxySQL.


## Выполнение

Будет поднимать Percona cluster из трех нод. Два виртуальных сервера у нас есть, подключим и настроим третий.

### Настройка VPS

Арендовали еще один виртуальный сервер с теми характеристиками как и предыдущие два.

 - OC: Ubuntu 18
 - CPU: Intel(R) Xeon(R) CPU E5-2699 v4 @ 2.20GHz
 - Кол-во ядер: 1
 - RAM: 1 GB
 - SSD 15 GB

На одном сервере стоит OS Centos 7

### Сетевые настройки между нодами

Разрешим через файрволл общаться нодам между собой по порту 3306.

Настройки файрволла на первой ноде:
```bash
$ iptables -I INPUT -p tcp --dport=3306 -j DROP
$ iptables -I INPUT -p tcp --dport=3306 -s <ip-node-2> -j ACCEPT
$ iptables -I INPUT -p tcp --dport=3306 -s <ip-node-3> -j ACCEPT
$ iptables-save
``` 

Разрешили для двух других нод обращаться по 3306 порту. Аналогичным образом ограничиваем доступ на двух других нодах.

## Установка Percona кластера

Необходимо открыть порты: 3306, 4444, 4567, 4578, которые требуются для Percona Cluster. 
Откроем их только для нод, находящиеся в кластере:
```bash
$ iptables -I INPUT -p tcp --dport=4578 -j DROP
$ iptables -I INPUT -p tcp --dport=4578 -s <ip-node-2> -j ACCEPT
$ iptables -I INPUT -p tcp --dport=4578 -s <ip-node-3> -j ACCEPT
$ iptables -I INPUT -p tcp --dport=4567 -j DROP
$ iptables -I INPUT -p tcp --dport=4567 -s <ip-node-2> -j ACCEPT
$ iptables -I INPUT -p tcp --dport=4567 -s <ip-node-3> -j ACCEPT
$ iptables -I INPUT -p tcp --dport=4444 -j DROP
$ iptables -I INPUT -p tcp --dport=4444 -s <ip-node-2> -j ACCEPT
$ iptables -I INPUT -p tcp --dport=4444 -s <ip-node-3> -j ACCEPT
$ iptables-save
```

### Подключение DEB репозитория

```bash
$ sudo apt-get update
$ sudo apt-get install -y wget gnupg2 lsb-release
$
$ wget https://repo.percona.com/apt/percona-release_latest.generic_all.deb
$
$ dpkg -i percona-release_latest.generic_all.deb
$ apt-get update
```

### Установка Percona  xtradb cluster


#### Настройка первой ноды

```bash
$ apt-get install percona-xtradb-cluster-57
```

При установке, мастер установки попросит ввести пароль для root. После установки, mysql запускается автоматически,
остановим сервис MySQL. Необходимо переконфигурировать mysql и запустить заново.

```bash
$ systemctl stop mysql
```

Файл конфигурации на Debian и Ubuntu расположен в **/etc/mysql/percona-xtradb-cluster.conf.d/wsrep.cnf**

- [Конфигурация](https://www.percona.com/doc/percona-xtradb-cluster/LATEST/configure.html)

```cnf
[mysqld]
# Path to Galera library
wsrep_provider=/usr/lib/galera3/libgalera_smm.so

# Cluster name
wsrep_cluster_name=network-cluster
# Cluster connection URL contains IPs of nodes
#If no IP is found, this implies that a new cluster needs to be created,
#in order to do that you need to bootstrap this node
wsrep_cluster_address=gcomm://<ip-node1>,<ip-node2>,<ip-node3>

# In order for Galera to work correctly binlog format should be ROW
binlog_format=ROW

# MyISAM storage engine has only experimental support
default_storage_engine=InnoDB

# Slave thread to use
wsrep_slave_threads= 8

wsrep_log_conflicts

# This changes how InnoDB autoincrement locks are managed and is a requirement for Galera
innodb_autoinc_lock_mode=2

# Node IP address
wsrep_node_address=<ip-current-node>
#If wsrep_node_name is not specified,  then system hostname will be used
wsrep_node_name=network-cluster-node-1

#pxc_strict_mode allowed values: DISABLED,PERMISSIVE,ENFORCING,MASTER
pxc_strict_mode=ENFORCING

# SST method
wsrep_sst_method=xtrabackup-v2

#Authentication for SST method
#wsrep_sst_auth="sstuser:s3cretPass"
```

- В параметре **wsrep_cluster_name** - указываем название кластера
- В параметре **wsrep_cluster_address** - указываем ip адреса нод кластера
- В параметре **wsrep_node_name** - указываем название текущей ноды
- В параметре **wsrep_node_address** - указываем ip адрес текущей ноды

Далее нужно инициализировать кластер следующей командой(Debian and Ubuntu):
```bash
$ /etc/init.d/mysql bootstrap-pxc
```

Для RHEL or CentOS 7, для инициализации используется следующая команда:
```bash
$ systemctl start mysql@bootstrap.service
```

Далее запустим mysql сервис:
```bash
$ systemctl start mysql
$ systemctl status mysql
● mysql.service - LSB: Start and stop the mysql (Percona XtraDB Cluster) daemon
   Loaded: loaded (/etc/init.d/mysql; generated)
   Active: active (exited) since Sat 2020-02-15 20:57:13 UTC; 7s ago
     Docs: man:systemd-sysv-generator(8)
  Process: 5827 ExecStop=/etc/init.d/mysql stop (code=exited, status=0/SUCCESS)
  Process: 8478 ExecStart=/etc/init.d/mysql start (code=exited, status=0/SUCCESS)
```

Видим что сервис запущен. Посмотри статус кластера

```mysql
mysql> show status like 'wsrep%';
...
| wsrep_gcomm_uuid                 | 70cba5ed-5035-11ea-882a-cf05adc37160 |
| wsrep_cluster_conf_id            | 1                                    |
| wsrep_cluster_size               | 1                                    |
| wsrep_cluster_state_uuid         | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status             | Primary                              |
| wsrep_connected                  | ON                                   |
| wsrep_local_bf_aborts            | 0                                    |
| wsrep_local_index                | 0                                    |
| wsrep_provider_name              | Galera                               |
| wsrep_provider_vendor            | Codership Oy <info@codership.com>    |
| wsrep_provider_version           | 3.41(rb3295e6)                       |
| wsrep_ready                      | ON                                   |
+----------------------------------+--------------------------------------+
71 rows in set (0.00 sec)
```

Видим что кластер заработал:
- wsrep_cluster_conf_id - идентификатор кластера
- wsrep_cluster_size - размер кластера. Пока одна нода

Перед тем как подключить в кластер дополнительные ноды, необходимо создать пользователя для SST(State Snapshot Transfer),
cо следующими правами RELOAD, LOCK TABLES, PROCESS, REPLICATION CLIENT :
```mysql
mysql> CREATE USER 'sstuser'@'localhost' IDENTIFIED BY 'password';
mysql> GRANT RELOAD, LOCK TABLES, PROCESS, REPLICATION CLIENT ON *.* TO 'sstuser'@'localhost';
mysql> FLUSH PRIVILEGES; 
```

Далее в конфигах расскоментируем параметр **wsrep_sst_auth** и укажем соответствующие значения и переинициализируем кластер.

#### Настройка остальных нод

Кластер у нас уже запущен на первой ноде. Далее подключим остальные ноды в кластер

Выполняем все теже действия на второй и третей ноде. В конфигах **/etc/mysql/percona-xtradb-cluster.conf.d/wsrep.cnf**
в параметрах **wsrep_node_address** и **wsrep_node_name** указываем соответствующие значения текущей ноды, 
все остальные параметры такие же как и для первой ноды.


После установки пришлось все ноды перезагрузить иначе остальные ноды не запускались.

Запускаем ноды:
```bash
$ systemctl start mysql
```

И смотрим статус кластера:
```mysql
mysql> SHOW STATUS LIKE 'wsrep%';
....
| wsrep_evs_state                  | OPERATIONAL                                           |
| wsrep_gcomm_uuid                 | 3f7c1333-504a-11ea-9e86-a21d571b0b48                  |
| wsrep_cluster_conf_id            | 15                                                    |
| wsrep_cluster_size               | 3                                                     |
| wsrep_cluster_state_uuid         | 5d8ab8cd-502a-11ea-a090-8abbc228fd50                  |
| wsrep_cluster_status             | Primary                                               |
| wsrep_connected                  | ON                                                    |
| wsrep_local_bf_aborts            | 0                                                     |
| wsrep_local_index                | 0                                                     |
| wsrep_provider_name              | Galera                                                |
| wsrep_provider_vendor            | Codership Oy <info@codership.com>                     |
| wsrep_provider_version           | 3.41(rb3295e6)                                        |
| wsrep_ready                      | ON                                                    |
+----------------------------------+-------------------------------------------------------+
71 rows in set (0.00 sec)
```

Видим что в кластере 3 ноды. Все они работают - изменения на одной ноде передаются на другие ноды синхронно.
На этом у нас есть кластер с мульти мастер, синхронной репликацией.

Теперь можно запросы(на запись и чтение) от приложения распределять рандомно между нодами. 
Также есть Load Balancer ProxySQL  для распределения запросов от приложение по нодам кластера. Далее настроим ProxySQL

### Установка ProxySQL

Будем устанавливать вторую версию ProxySQL. Для ProxySQL  лучще выделить отдельную ноду. 
Но мы обойдемся без дополнительной ноды и установим его на ноду 3 там же где и само приложение.
На ноде 3 OS стоит Centos 7:
```bash
$ yum install proxysql2
``` 

Для Debian:
```bash
$ apt-get install proxysql2
```

ProxySQL v2 включает в себя утилиту proxysql-admin для автоконфигурирования proxysql и percona xtradb cluster.

Прежде чем запускать proxysql, переконфигурируем его. Конфигурационные файлы:
- /etc/proxysql.cnf - конфиг для proxysql;
- /etc/proxysql-admin.cnf - конфиг для proxysql-admin, с помощью которого инициализируется proxysql.

- [Параметры конфигурации](https://github.com/sysown/proxysql/blob/master/doc/global_variables.md)

Важная заметка:
```cnf
#file proxysql.cfg

########################################################################################
# This config file is parsed using libconfig , and its grammar is described in:
# http://www.hyperrealm.com/libconfig/libconfig_manual.html#Configuration-File-Grammar
# Grammar is also copied at the end of this file
########################################################################################

########################################################################################
# IMPORTANT INFORMATION REGARDING THIS CONFIGURATION FILE:
########################################################################################
# On startup, ProxySQL reads its config file (if present) to determine its datadir.
# What happens next depends on if the database file (disk) is present in the defined
# datadir (i.e. "/var/lib/proxysql/proxysql.db").
#
# If the database file is found, ProxySQL initializes its in-memory configuration from
# the persisted on-disk database. So, disk configuration gets loaded into memory and
# then propagated towards the runtime configuration.
#
# If the database file is not found and a config file exists, the config file is parsed
# and its content is loaded into the in-memory database, to then be both saved on-disk
# database and loaded at runtime.
#
# IMPORTANT: If a database file is found, the config file is NOT parsed. In this case
#            ProxySQL initializes its in-memory configuration from the persisted on-disk
#            database ONLY. In other words, the configuration found in the proxysql.cnf
#            file is only used to initial the on-disk database read on the first startup.
#
# In order to FORCE a re-initialise of the on-disk database from the configuration file
# the ProxySQL service should be started with "service proxysql initial".
#
########################################################################################
```

Proxysql использует конфиги из файла при первом запуске, прокси создает базу и заполняет его данными из конфига.
При необходимости поменять конфиги для прокси, нужно дать команду прокси заного прочитать файл конфигов и записать в базу еще раз.
Это можно сделать запустив proxysql с флагом --initial:

В конфигах поменял доступы администратора поумолчанию на свои.

Запустим proxysql:
```bash
$ service proxysql start
```

Далее чтоб ProxySQL работал с кластером необходимо связать. Для этого воспользуемся программой proxysql-admin.
В настройках для proxysqladmin зададим креденшиалс для пользователей прокси, кластера, мониторинга и приложения.
Также переключим режим прокси с singlewrite(Один мастер) на loadbal(Load Balancing) 

```cnf
/etc/proxysql-admin.cnf

# proxysql admin interface credentials.
export PROXYSQL_DATADIR='/var/lib/proxysql'
export PROXYSQL_USERNAME='proxy-user'
export PROXYSQL_PASSWORD='pass'
export PROXYSQL_HOSTNAME='localhost'
export PROXYSQL_PORT='6032'

# PXC admin credentials for connecting to pxc-cluster-node.
export CLUSTER_USERNAME=cluster-user'
export CLUSTER_PASSWORD='pass'
export CLUSTER_HOSTNAME='localhost'
export CLUSTER_PORT='3306'

# proxysql monitoring user. proxysql admin script will create this user in pxc to monitor pxc-nodes.
export MONITOR_USERNAME='monitor-user'
export MONITOR_PASSWORD='pass'

# Application user to connect to pxc-node through proxysql
export CLUSTER_APP_USERNAME='app_user'
export CLUSTER_APP_PASSWORD='pass'

# ProxySQL hostgroup IDs
export WRITER_HOSTGROUP_ID='10'
export READER_HOSTGROUP_ID='11'
export BACKUP_WRITER_HOSTGROUP_ID='12'
export OFFLINE_HOSTGROUP_ID='13'

# ProxySQL read/write configuration mode.
export MODE="loadbal"

# Determines if a node should be added to the reader hostgroup if it has
# been promoted to the writer hostgroup.
# If set to 'yes', then all writers (including backup-writers) are added to
# the read hostgroup.
# If set to 'no', then none of the writers (including backup-writers) are added.
# If set to 'backup', then only the backup-writers will be added to
# the read hostgroup.
export WRITERS_ARE_READERS="yes"
```

Далее запустим процесс автоконфигурирования proxysql и percona cluster:
```bash
$ proxysql-admin --config-file=/etc/proxysql-admin.cnf --enable
....
ProxySQL configuration completed!

ProxySQL has been successfully configured to use with Percona XtraDB Cluster

You can use the following login credentials to connect your application through ProxySQL
....
```

Теперь зайдем в прокси и посмотри какие ноды добавились для проксирования
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 13           |  <ip-node-1> | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           |  <ip-node-2> | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           |  <ip-node-3> | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.01 sec)

mysql> select * from mysql_galera_hostgroups\G
*************************** 1. row ***************************
       writer_hostgroup: 10
backup_writer_hostgroup: 12
       reader_hostgroup: 11
      offline_hostgroup: 13
                 active: 1
            max_writers: 1000000
  writer_is_also_reader: 0
max_transactions_behind: 100
                comment: NULL
1 row in set (0.00 sec)
```

Видим все три ноды добавились и находятся в статусе **ONLINE**, но находятся в группе оффлайновских хостов(СТРАННО!!!) 

```mysql
mysql> select * from mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <ip-node-1>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <ip-node-2>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <ip-node-3>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.00 sec)

mysql> LOAD MYSQL SERVERS TO RUNTIME;
Query OK, 0 rows affected (0.01 sec)

mysql> SAVE MYSQL SERVERS TO DISK;
Query OK, 0 rows affected (0.03 sec)
```
Видим что в таблице **mysql_servers** ноды в группе writer.

**LOAD MYSQL SERVERS TO RUNTIME** не помог, в реалтайме все ноды группе оффлайна. 
Перезагрузил сервер и переустановил ProxySQL так как не смог запустить systemd сервис.

Перед настройкой прокси, переключил на режим singlewrite(Один мастер) и запустил сервис и настройку с помощью proxyadmin:
```bash
$ proxy-admin --config-file=/etc/proxysql-admin.cnf --enable
```

```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 12           | <ip-node-1>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <ip-node-2>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <ip-node-1>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <ip-node-3>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 12           | <ip-node-2>  | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
5 rows in set (0.00 sec)
```

Видим что все ноды в рабочих группах и в онлайне. Один мастер(нода-3) остальные два в группах для чтения и бекапа.

Теперь можно перенаправить приложение на прокси и тестировать следующие кейсы:
- (Master-slave) Включить генерацию пользователей и в процессе отключить мастер. Проверка потерянных транзакций
- (Master-slave) Тест на нагрузку при чтении
- (Multi-Master) Включить генерацию пользователей и в процессе отключить мастер. Проверка потерянных транзакций

### Подключение приложения к прокси

Для подключения клиентских приложении ProxySQL открывает порт 6033. 
Переключили приложение на этот порт и указали пользователя.

### Тесты

#### (Master-slave) Включить генерацию пользователей и в процессе отключить мастер. Проверка потерянных транзакций

Начальное состояние кластера:
- таблица пользователей пуста
- в кластере 3 ноды

```mysql
mysql> select count(1) from user_users;
+----------+
| count(1) |
+----------+
|        0 |
+----------+
1 row in set (0.00 sec)

mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 3                                    |
| wsrep_cluster_conf_id    | 23                                   |
| wsrep_cluster_size       | 3                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.01 sec)
```

Состояние нод в ProxySQL:
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 12           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 12           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
5 rows in set (0.01 sec)

mysql> select * from mysql_galera_hostgroups\G
*************************** 1. row ***************************
       writer_hostgroup: 10
backup_writer_hostgroup: 12
       reader_hostgroup: 11
      offline_hostgroup: 13
                 active: 1
            max_writers: 1
  writer_is_also_reader: 2
max_transactions_behind: 100
                comment: NULL
1 row in set (0.00 sec)
```

- нода 3 в группе для записи
- нода 1 и 2 в группе для чтения

##### План:
- запустить генерацию 1 млн пользователей через прокси
- в процессе генерации отключить ноду 3
- зафиксировать статус отключенной ноды
- включить ноду обратно


##### Процесс

Запустил генерацию пользователей и через какое-то время отключил мастер ноду и скрипт упал(естественно) **Error 2013: Lost connection to MySQL server during query**

ProxySQL:
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 12           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 11           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
4 rows in set (0.00 sec)
```

Видим нода-3 в группе офлайна, нода-2 стала мастером, а нода 1 осталось в группе чтения


Percona Cluster: остались две ноды
```mysql
mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 2                                    |
| wsrep_cluster_conf_id    | 24                                   |
| wsrep_cluster_size       | 2                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.00 sec)
```

В кластере осталось 2 ноды

Вывод: 
- В кейсе Master-slave репкликации в Percona кластере + ProxySQLб транзакция которая выполнялась на момент падении мастера, потеряется,
  но последующие транзакции должный пройти успешно так как ProxySQL для записи переключиться на другую рабочую ноду.


#### (Master-slave) Тест на нагрузку при чтении

##### Состояние:
- в кластере 3 ноды(1 мастер - 2 слейва)
- в таблице 1 210 000 пользователей
- индексы которые были слзданы в ДЗ по индексам

Повторим тесты с урока по индексам

##### Профиль пользователя

###### Apache Benchmark - ab
 
Результаты теста до репликации:
- Time taken for tests:   15.901 seconds
- Requests per second:    62.89 [#/sec] (mean)
- Time per request:       1590.068 [ms] (mean)
- Time per request:       15.901 [ms] (mean, across all concurrent requests)

```bash
$ ab -n 1000 -r -c 100 http://5.181.108.42/members/show?id=00f14dd4-0656-4c99-a424-136b9fa0957f
Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /members/show?id=00f14dd4-0656-4c99-a424-136b9fa0957f
Document Length:        1572 bytes

Concurrency Level:      100
Time taken for tests:   13.754 seconds
Complete requests:      1000
Failed requests:        0
Total transferred:      1846000 bytes
HTML transferred:       1572000 bytes
Requests per second:    72.70 [#/sec] (mean)
Time per request:       1375.426 [ms] (mean)
Time per request:       13.754 [ms] (mean, across all concurrent requests)
Transfer rate:          131.07 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   26  24.5     18     246
Processing:    55 1297 256.0   1331    1662
Waiting:       45 1296 256.1   1329    1661
Total:         71 1324 251.1   1350    1681
```

В общем результаты такие же как и при обычной мастер славе репликации. 
Дальше не вижу смысла продолжать тесты на чтение. Перейдем на следующий тест

#### (Multi-Master) Включить генерацию пользователей и в процессе отключить мастер. Проверка потерянных транзакций

Переконфигурируем ProxySQL c singlewrite на loadbal

```bash
$ proxysql-admin --config-file /etc/proxysql-admin.cnf --disable
$ 
$ proxysql-admin --config-file /etc/proxysql-admin.cnf --mode=loadbal --enable
```

Посмотрим какие сервера в какую группу попали:
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.00 sec)
```

Видим все три ноды попали в группу мастер(10).

Очистим таблицу с пользователями:
```mysql
mysql> truncate user_users;
Query OK, 0 rows affected (0.10 sec)

mysql> select count(1) from user_users;
+----------+
| count(1) |
+----------+
|        0 |
+----------+
1 row in set (0.00 sec)
```

Запустим генерацию 1млн пользователей
```bash
$ go run main.go
```
Скрипт упал так как не мог подключиться прокси по указанному пользователю. 
После переконфигурации proxySql пользователь из самой прокси пропал(хз), пришлось повторно создать пользователя в прокси.

**Чтоб приложение подключалось к кластеру через прокси, необходимо создать пользователя и в кластере и в прокси**

Запустим генерацию 1млн пользователей заново:
```bash
$ go run main.go
```
Процесс пошел, и заметно медленее чем при асинхронной репликации

Отключаем одну ноду:
```bash
$ systemctl stop mysql
```

Смотри в кластрере осталось 2 ноды
```mysql
mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 3                                    |
| wsrep_cluster_conf_id    | 29                                   |
| wsrep_cluster_size       | 3                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.02 sec)
```


Смотрим в прокси, что нода-2 в офлайне
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.00 sec)
```

**Это фиаско братан**: Произошла коллизия UUID и скрипт упал
```bash
Error 1062: Duplicate entry '2576214a-7c59-480c-8da6-cdbaca90c99b' for key 'user_idx'
exit status 1
```

Но после восстановления упавшей ноды, количество данных на всех нодах были равными

Попробуем заново:

```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-2>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.01 sec)
```

Все ноды на готове, база очищена.

При повторном запуске, ситуация повторилась. При восстановлении ноды была попытка добавить повторно запись,
поэтому процесс генерации пользователей пал. (Подумать почему так произошло)


Повторим процесс еще раз, но не станем восстанавливать работу ноды, пока генерация не завершится.


Запустим генерацию 1млн пользователей заново:
```bash
$ go run main.go
```


```bash
# systemctl stop mysql
```

```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | <node-1>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 13           | <node-2>     | 3306 | 0         | SHUNNED| 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | <node-3>     | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.00 sec)
```

После завершения скрипта, восстановил работу ноды и зафиксировал следующую ситуацию:

- В прокси нода-2 до сих пор в оффлайне, а статус SHUNNED(?)
- Нода-2 отстала от други нод
- Хотя нода-2 в кластере, но на нее не реплицируются данные

Статус мастера на восстановившейся ноде:
```mysql
mysql> show master status;
+---------------------+----------+--------------+------------------+-------------------+
| File                | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------------+----------+--------------+------------------+-------------------+
| devalico-bin.000001 |      154 |              |                  |                   |
+---------------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

Статус мастера на другой ноде:
```mysql
mysql> show master status;
+----------------+------------+--------------+------------------+-------------------+
| File           | Position   | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+----------------+------------+--------------+------------------+-------------------+
| Da2-bin.000011 | 1013438199 |              |                  |                   |
+----------------+------------+--------------+------------------+-------------------+
1 row in set (0.01 sec)
```

Статус кластера:
```mysql
mysql> show status like 'wsrep_cl%';
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_weight     | 3                                    |
| wsrep_cluster_conf_id    | 33                                   |
| wsrep_cluster_size       | 3                                    |
| wsrep_cluster_state_uuid | 5d8ab8cd-502a-11ea-a090-8abbc228fd50 |
| wsrep_cluster_status     | Primary                              |
+--------------------------+--------------------------------------+
5 rows in set (0.01 sec)
```

На ноде-2 при запросе количество данных в таблице пользователей выдает ошибку:
```mysql
mysql> select count(1) from network.user_users;
ERROR 1047 (08S01): WSREP has not yet prepared node for application use
```
- [На офф. сайте](https://www.percona.com/blog/2015/12/21/mysql-and-percona-xtradb-cluster-even-higher-availability-with-correct-application-design/)


Видимо было запрещено читать неактуальные данные.

Разрешим:
```mysql
mysql> set wsrep_dirty_reads=ON;
mysql> select count(1) from network.user_users;
+----------+
| count(1) |
+----------+
|  1000000 |
+----------+
1 row in set (2.63 sec)
```

Видим 1млн записей

На ProxySQL:
```mysql
mysql> select * from runtime_mysql_servers;
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname     | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 10           | node-1       | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | node-2       | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
| 10           | node-3       | 3306 | 0         | ONLINE | 1000   | 0           | 1000            | 0                   | 0       | 0              |         |
+--------------+--------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.01 sec)
```

Заметил: после **set wsrep_dirty_reads=ON**,  нода - 2 опять в строю. Но если опять отключить wsrep_dirty_reads, нода-2 будет в офлайн группе



Статус кластера на первой ноде:
```mysql
mysql> show status like 'wsrep%';
+----------------------------------+-------------------------------------------------------+
| Variable_name                    | Value                                                 |
+----------------------------------+-------------------------------------------------------+
| wsrep_local_state_uuid           | 5d8ab8cd-502a-11ea-a090-8abbc228fd50                  |
| wsrep_protocol_version           | 9                                                     |
| wsrep_last_applied               | 2335                                                  |
| wsrep_last_committed             | 2335                                                  |
| wsrep_replicated                 | 127                                                   |
| wsrep_replicated_bytes           | 215542520                                             |
| wsrep_repl_keys                  | 1120243                                               |
| wsrep_repl_keys_bytes            | 8965888                                               |
| wsrep_repl_data_bytes            | 206568069                                             |
| wsrep_repl_other_bytes           | 0                                                     |
| wsrep_received                   | 3857                                                  |
| wsrep_received_bytes             | 843382505                                             |
| wsrep_local_commits              | 112                                                   |
| wsrep_local_cert_failures        | 1                                                     |
| wsrep_local_replays              | 0                                                     |
| wsrep_local_send_queue           | 0                                                     |
| wsrep_local_send_queue_max       | 1                                                     |
| wsrep_local_send_queue_min       | 0                                                     |
| wsrep_local_send_queue_avg       | 0.000000                                              |
| wsrep_local_recv_queue           | 0                                                     |
| wsrep_local_recv_queue_max       | 2                                                     |
| wsrep_local_recv_queue_min       | 0                                                     |
| wsrep_local_recv_queue_avg       | 0.000259                                              |
| wsrep_local_cached_downto        | 2267                                                  |
| wsrep_flow_control_paused_ns     | 0                                                     |
| wsrep_flow_control_paused        | 0.000000                                              |
| wsrep_flow_control_sent          | 0                                                     |
| wsrep_flow_control_recv          | 0                                                     |
| wsrep_flow_control_interval      | [ 173, 173 ]                                          |
| wsrep_flow_control_interval_low  | 173                                                   |
| wsrep_flow_control_interval_high | 173                                                   |
| wsrep_flow_control_status        | OFF                                                   |
| wsrep_cert_deps_distance         | 8.764479                                              |
| wsrep_apply_oooe                 | 0.003432                                              |
| wsrep_apply_oool                 | 0.001716                                              |
| wsrep_apply_window               | 1.005148                                              |
| wsrep_commit_oooe                | 0.000000                                              |
| wsrep_commit_oool                | 0.000429                                              |
| wsrep_commit_window              | 1.003003                                              |
| wsrep_local_state                | 4                                                     |
| wsrep_local_state_comment        | Synced                                                |
| wsrep_cert_index_size            | 30002                                                 |
| wsrep_cert_bucket_count          | 62244                                                 |
| wsrep_gcache_pool_size           | 134178240                                             |
| wsrep_causal_reads               | 0                                                     |
| wsrep_cert_interval              | 0.000000                                              |
| wsrep_open_transactions          | 0                                                     |
| wsrep_open_connections           | 0                                                     |
| wsrep_ist_receive_status         |                                                       |
| wsrep_ist_receive_seqno_start    | 0                                                     |
| wsrep_ist_receive_seqno_current  | 0                                                     |
| wsrep_ist_receive_seqno_end      | 0                                                     |
| wsrep_incoming_addresses         | 5.181.108.42:3306,45.67.57.214:3306,45.84.225.62:3306 |
| wsrep_cluster_weight             | 3                                                     |
| wsrep_desync_count               | 0                                                     |
| wsrep_evs_delayed                |                                                       |
| wsrep_evs_evict_list             |                                                       |
| wsrep_evs_repl_latency           | 0/0/0/0/0                                             |
| wsrep_evs_state                  | OPERATIONAL                                           |
| wsrep_gcomm_uuid                 | 3f7c1333-504a-11ea-9e86-a21d571b0b48                  |
| wsrep_cluster_conf_id            | 33                                                    |
| wsrep_cluster_size               | 3                                                     |
| wsrep_cluster_state_uuid         | 5d8ab8cd-502a-11ea-a090-8abbc228fd50                  |
| wsrep_cluster_status             | Primary                                               |
| wsrep_connected                  | ON                                                    |
| wsrep_local_bf_aborts            | 0                                                     |
| wsrep_local_index                | 1                                                     |
| wsrep_provider_name              | Galera                                                |
| wsrep_provider_vendor            | Codership Oy <info@codership.com>                     |
| wsrep_provider_version           | 3.41(rb3295e6)                                        |
| wsrep_ready                      | ON                                                    |
+----------------------------------+-------------------------------------------------------+
71 rows in set (0.01 sec)
```


Статус кластера на второй ноде:
```mysql
mysql> show status like 'wsrep%';
+----------------------------------+-------------------------------------------------------+
| Variable_name                    | Value                                                 |
+----------------------------------+-------------------------------------------------------+
| wsrep_local_state_uuid           | 5d8ab8cd-502a-11ea-a090-8abbc228fd50                  |
| wsrep_protocol_version           | 9                                                     |
| wsrep_last_applied               | 2335                                                  |
| wsrep_last_committed             | 2335                                                  |
| wsrep_replicated                 | 0                                                     |
| wsrep_replicated_bytes           | 0                                                     |
| wsrep_repl_keys                  | 0                                                     |
| wsrep_repl_keys_bytes            | 0                                                     |
| wsrep_repl_data_bytes            | 0                                                     |
| wsrep_repl_other_bytes           | 0                                                     |
| wsrep_received                   | 1                                                     |
| wsrep_received_bytes             | 322                                                   |
| wsrep_local_commits              | 0                                                     |
| wsrep_local_cert_failures        | 0                                                     |
| wsrep_local_replays              | 0                                                     |
| wsrep_local_send_queue           | 0                                                     |
| wsrep_local_send_queue_max       | 1                                                     |
| wsrep_local_send_queue_min       | 0                                                     |
| wsrep_local_send_queue_avg       | 0.000000                                              |
| wsrep_local_recv_queue           | 0                                                     |
| wsrep_local_recv_queue_max       | 1                                                     |
| wsrep_local_recv_queue_min       | 0                                                     |
| wsrep_local_recv_queue_avg       | 0.000000                                              |
| wsrep_local_cached_downto        | 0                                                     |
| wsrep_flow_control_paused_ns     | 0                                                     |
| wsrep_flow_control_paused        | 0.000000                                              |
| wsrep_flow_control_sent          | 0                                                     |
| wsrep_flow_control_recv          | 0                                                     |
| wsrep_flow_control_interval      | [ 173, 173 ]                                          |
| wsrep_flow_control_interval_low  | 173                                                   |
| wsrep_flow_control_interval_high | 173                                                   |
| wsrep_flow_control_status        | OFF                                                   |
| wsrep_cert_deps_distance         | 0.000000                                              |
| wsrep_apply_oooe                 | 0.000000                                              |
| wsrep_apply_oool                 | 0.000000                                              |
| wsrep_apply_window               | 0.000000                                              |
| wsrep_commit_oooe                | 0.000000                                              |
| wsrep_commit_oool                | 0.000000                                              |
| wsrep_commit_window              | 0.000000                                              |
| wsrep_local_state                | 1                                                     |
| wsrep_local_state_comment        | Joining: receiving State Transfer                     |
| wsrep_cert_index_size            | 0                                                     |
| wsrep_cert_bucket_count          | 22                                                    |
| wsrep_gcache_pool_size           | 1528                                                  |
| wsrep_causal_reads               | 0                                                     |
| wsrep_cert_interval              | 0.000000                                              |
| wsrep_open_transactions          | 0                                                     |
| wsrep_open_connections           | 0                                                     |
| wsrep_ist_receive_status         | 0% complete, received seqno 2245 of 2246-2335         |
| wsrep_ist_receive_seqno_start    | 2246                                                  |
| wsrep_ist_receive_seqno_current  | 2245                                                  |
| wsrep_ist_receive_seqno_end      | 2335                                                  |
| wsrep_incoming_addresses         | 5.181.108.42:3306,45.67.57.214:3306,45.84.225.62:3306 |
| wsrep_cluster_weight             | 3                                                     |
| wsrep_desync_count               | 0                                                     |
| wsrep_evs_delayed                |                                                       |
| wsrep_evs_evict_list             |                                                       |
| wsrep_evs_repl_latency           | 0/0/0/0/0                                             |
| wsrep_evs_state                  | OPERATIONAL                                           |
| wsrep_gcomm_uuid                 | 110399fb-528e-11ea-9a26-8680a20ea0a6                  |
| wsrep_cluster_conf_id            | 33                                                    |
| wsrep_cluster_size               | 3                                                     |
| wsrep_cluster_state_uuid         | 5d8ab8cd-502a-11ea-a090-8abbc228fd50                  |
| wsrep_cluster_status             | Primary                                               |
| wsrep_connected                  | ON                                                    |
| wsrep_local_bf_aborts            | 0                                                     |
| wsrep_local_index                | 0                                                     |
| wsrep_provider_name              | Galera                                                |
| wsrep_provider_vendor            | Codership Oy <info@codership.com>                     |
| wsrep_provider_version           | 3.41(rb3295e6)                                        |
| wsrep_ready                      | OFF                                                   |
+----------------------------------+-------------------------------------------------------+
71 rows in set (0.01 sec)
```

Видим что **wsrep_ready:OFF** ?????

Решил рестартануть mysql на ноде-2, стартует очень ДОЛГО.

После рестарта нода заработала как надо, в прокси все ноды в группе матеров 

### Полезные ссылки:
- [Документация](https://www.percona.com/doc/percona-xtradb-cluster/LATEST/index.html)
- [О  Percona Cluster](https://www.percona.com/doc/percona-xtradb-cluster/LATEST/intro.html)
- [Ограничения Percona кластера](https://www.percona.com/doc/percona-xtradb-cluster/LATEST/limitation.html)
- [Установка ProxySQL](https://www.percona.com/doc/percona-xtradb-cluster/LATEST/howtos/proxysql.html#load-balancing-with-proxysql)
- [Мониторинг кластера](https://www.percona.com/software/database-tools/percona-monitoring-and-management)


