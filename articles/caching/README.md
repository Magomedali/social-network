Кеширование
------------------------

-> [Главная](../../README.md) -> Лекция

- [Домашнее задание](./homework.md)

### Доп материал

- [Web, кэширование и memcached](https://smira.ru/wp-content/uploads/2008/10/web-caching-and-memcached.pdf)
- [Кеширование с HTTP Etag](https://ruhighload.com/%D0%9A%D0%B5%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5+%D1%81+http+etag)
- [Nginx cache: всё новое — хорошо забытое старое](https://habr.com/ru/post/428127/)

