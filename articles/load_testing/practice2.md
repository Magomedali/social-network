Практика 2
---------------------------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> [Домашнее задание](homework.md)

Описание системы на момент выполнения практики:
- MySql - база, 1млн пользователей
- Nginx - веб сервер
- php-fpm

Страницы сайта:
- Профиль пользователя: поиск пользователя по id. В качестве id используется UUID v4 тип поля varchar(36)
- Поиск: отдает пользователей по префиксному соответствию по имени или фамилии и сортируя по имени
- Кэш MySQL отключен

### План Выполнения:
1. Протестировать профиль пользователя инструментом wrk и ab
2. Протестировать страницу поиска инструментом wrk и ab
3. Протестировать после индексов


### Профиль пользователя- /

#### Apache Benchmark - ab

##### Общее количество - 1000 запросов, одновременно 100 запросов
```bash
$ ab -n 1000 -r -c 100 http://5.181.108.42/members/show?id=b71ab99b-c4ef-4997-98f8-3a14e1f89002
Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /members/show?id=b71ab99b-c4ef-4997-98f8-3a14e1f89002
Document Length:        1565 bytes

Concurrency Level:      100
Time taken for tests:   671.637 seconds
Complete requests:      1000
Failed requests:        941
   (Connect: 0, Receive: 0, Length: 941, Exceptions: 0)
Non-2xx responses:      941
Total transferred:      415267 bytes
HTML transferred:       250423 bytes
Requests per second:    1.49 [#/sec] (mean)
Time per request:       67163.657 [ms] (mean)
Time per request:       671.637 [ms] (mean, across all concurrent requests)
Transfer rate:          0.60 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   24  43.0     19    1329
Processing:  1413 61437 11219.5  60108   91319
Waiting:     1401 61436 11219.5  60108   91317
Total:       1430 61462 11219.0  60133   91333

Percentage of the requests served within a certain time (ms)
  50%  60133
  66%  61132
  75%  62056
  80%  63080
  90%  67148
  95%  91117
  98%  91139
  99%  91194
 100%  91333 (longest request)
```
Итог:
- Time taken for tests:   671.637 seconds
- Requests per second:    1.49 [#/sec] (mean)
- Time per request:       67163.657 [ms] (mean)
- Time per request:       671.637 [ms] (mean, across all concurrent requests)


![Мониторинг](./beforeIndex_n1000_c100.png)


### Поиск пользователя - ?key=<searchkey>

#### Apache Benchmark - ab

##### Общее количество - 100 запросов, одновременно 10 запросов
```bash
$ ab -n 100 -r -c 10 http://5.181.108.42/?key=O%27Conner&limit=50
Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /?key=O%27Conner
Document Length:        14847 bytes

Concurrency Level:      10
Time taken for tests:   63.795 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      1512100 bytes
HTML transferred:       1484700 bytes
Requests per second:    1.57 [#/sec] (mean)
Time per request:       6379.472 [ms] (mean)
Time per request:       637.947 [ms] (mean, across all concurrent requests)
Transfer rate:          23.15 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       15   30  41.1     24     316
Processing:  1164 6036 1053.3   6122    9296
Waiting:     1160 6032 1052.8   6117    9294
Total:       1180 6066 1057.4   6142    9317

Percentage of the requests served within a certain time (ms)
  50%   6142
  66%   6443
  75%   6456
  80%   6463
  90%   6777
  95%   6862
  98%   8302
  99%   9317
 100%   9317 (longest request)
```

Итог:
- Time taken for tests:   63.795 seconds
- Requests per second:    1.57 [#/sec] (mean)
- Time per request:       6379.472 [ms] (mean)
- Time per request:       637.947 [ms] (mean, across all concurrent requests)

### Повтор теста после создания индексов

#### Профиль пользователя

#### Apache Benchmark - ab

```bash
$ ab -n 1000 -r -c 100 http://5.181.108.42/members/show?id=b71ab99b-c4ef-4997-98f8-3a14e1f89002

Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /members/show?id=b71ab99b-c4ef-4997-98f8-3a14e1f89002
Document Length:        1565 bytes

Concurrency Level:      100
Time taken for tests:   15.901 seconds
Complete requests:      1000
Failed requests:        0
Total transferred:      1839000 bytes
HTML transferred:       1565000 bytes
Requests per second:    62.89 [#/sec] (mean)
Time per request:       1590.068 [ms] (mean)
Time per request:       15.901 [ms] (mean, across all concurrent requests)
Transfer rate:          112.94 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   24  15.3     18     154
Processing:    56 1487 262.5   1510    1841
Waiting:       47 1486 262.6   1509    1840
Total:         75 1510 262.5   1534    1900

Percentage of the requests served within a certain time (ms)
  50%   1534
  66%   1576
  75%   1637
  80%   1669
  90%   1754
  95%   1796
  98%   1818
  99%   1833
 100%   1900 (longest request)
```

Итог:
- Time taken for tests:   15.901 seconds
- Requests per second:    62.89 [#/sec] (mean)
- Time per request:       1590.068 [ms] (mean)
- Time per request:       15.901 [ms] (mean, across all concurrent requests)

#### Поиск пользователей

#### Apache Benchmark - ab

##### Общее количество - 100 запросов, одновременно 10 запросов
```bash
$ ab -n 100 -r -c 10 http://5.181.108.42/?key=O%27Conner&limit=50

Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /?key=O%27Conner
Document Length:        14847 bytes

Concurrency Level:      10
Time taken for tests:   4.804 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      1512100 bytes
HTML transferred:       1484700 bytes
Requests per second:    20.82 [#/sec] (mean)
Time per request:       480.383 [ms] (mean)
Time per request:       48.038 [ms] (mean, across all concurrent requests)
Transfer rate:          307.39 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   21   9.6     18      62
Processing:   206  361  95.1    349     946
Waiting:      205  359  94.9    346     942
Total:        223  383  95.4    368     963

Percentage of the requests served within a certain time (ms)
  50%    368
  66%    417
  75%    439
  80%    453
  90%    477
  95%    506
  98%    560
  99%    963
 100%    963 (longest request)
```

Итог:
- Time taken for tests:   4.804 seconds;
- Requests per second:    20.82 [#/sec] (mean)
- Time per request:       480.383 [ms] (mean)
- Time per request:       48.038 [ms] (mean, across all concurrent requests)


![Мониторинг](./after_index.png)

### Заключение

Профиль пользователя:
- Time taken for tests:
  - 671.637 seconds -> 15.901 seconds
  
- Requests per second:    
  - 1.49 [#/sec] (mean) -> 62.89 [#/sec] (mean)

- Time per request:
  - 67163.657 [ms] (mean) -> 1590.068 [ms] (mean)

- Time per request:
  - 671.637 [ms] -> 15.901 [ms]  (mean, across all concurrent requests)
  
Поиск пользователей:
- Time taken for tests:
  - 63.795 seconds -> 4.804 seconds
  
- Requests per second:    
  - 1.49 [#/sec] (mean) -> 20.82 [#/sec] (mean)

- Time per request:
  - 6379.472 [ms] (mean) -> 480.383 [ms] (mean)

- Time per request:
  - 6379.472 [ms] -> 48.038 [ms]  (mean, across all concurrent requests)