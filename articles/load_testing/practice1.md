Практика 1
---------------------------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> [Домашнее задание](homework.md)


### План Выполнения:
1. Описать характеристики VPS
2. Провести тест диска
3. Провести тест процессора
4. Провести тест сети
5. Нагрузочный тест сервера

### Характеристики VPS

 - OC: Centos 7
 - CPU: Intel(R) Xeon(R) CPU E5-2699 v4 @ 2.20GHz
 - Кол-во ядер: 1
 - RAM: 1 GB
 - SSD 15 GB
 
 
### Тестирование диска

**Задача**: Провести тестирование диска с помощью программы fio - Flexible I/O Tester

Case: Maximum write throughput

```ini
[max_w_throughput]
name=seqwrite
ioengine=sync
direct=0
fsync_on_close=1
randrepeat=0
nrfiles=1
rw=write
blocksize=1m
size=5G
end_fsync=1
fallocate=none
overwrite=0
numjobs=1
directory=/mnt/gcfs
loops=10
```

```
seqwrite: (g=0): rw=write, bs=(R) 1024KiB-1024KiB, (W) 1024KiB-1024KiB, (T) 1024KiB-1024KiB, ioengine=sync, iodepth=1
fio-3.7
Starting 1 process
seqwrite: Laying out IO file (1 file / 5120MiB)

seqwrite: (groupid=0, jobs=1): err= 0: pid=31273: Tue Jan 21 22:20:31 2020
  write: IOPS=799, BW=800MiB/s (839MB/s)(50.0GiB/64024msec)
    clat (usec): min=492, max=51138, avg=1187.18, stdev=1872.41
     lat (usec): min=510, max=51157, avg=1218.18, stdev=1892.72
    clat percentiles (usec):
     |  1.00th=[  537],  5.00th=[  562], 10.00th=[  578], 20.00th=[  603],
     | 30.00th=[  627], 40.00th=[  660], 50.00th=[  701], 60.00th=[  758],
     | 70.00th=[  832], 80.00th=[  938], 90.00th=[ 1352], 95.00th=[ 4080],
     | 99.00th=[10683], 99.50th=[12387], 99.90th=[16450], 99.95th=[20055],
     | 99.99th=[38011]
   bw (  KiB/s): min=403456, max=1021984, per=98.83%, avg=809281.50, stdev=122643.62, samples=127
   iops        : min=  394, max=  998, avg=789.84, stdev=119.77, samples=127
  lat (usec)   : 500=0.01%, 750=58.31%, 1000=24.92%
  lat (msec)   : 2=9.17%, 4=2.52%, 10=3.87%, 20=1.15%, 50=0.05%
  lat (msec)   : 100=0.01%
  cpu          : usr=2.19%, sys=63.32%, ctx=43040, majf=0, minf=31
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=0,51200,0,10 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=1

Run status group 0 (all jobs):
  WRITE: bw=800MiB/s (839MB/s), 800MiB/s-800MiB/s (839MB/s-839MB/s), io=50.0GiB (53.7GB), run=64024-64024msec

Disk stats (read/write):
  vda: ios=10753/129925, merge=9312/68084, ticks=469303/846655, in_queue=1297778, util=88.79%
```


Case: Maximum write IOPS

```ini
[max_w_latency]
name=randwrite
ioengine=sync
direct=0
fsync_on_close=1
randrepeat=0
nrfiles=1
rw=randwrite
bs=4K
size=512m
end_fsync=1
fallocate=none
overwrite=0
numjobs=5
sync=1
directory=/mnt/standart
loops=10
```


Результат:
```
randwrite: (g=0): rw=randwrite, bs=(R) 4096B-4096B, (W) 4096B-4096B, (T) 4096B-4096B, ioengine=sync, iodepth=1
...
fio-3.7
Starting 5 processes
randwrite: Laying out IO file (1 file / 512MiB)
max_w_latency: Laying out IO file (1 file / 512MiB)
max_w_latency: Laying out IO file (1 file / 512MiB)
max_w_latency: Laying out IO file (1 file / 512MiB)
max_w_latency: Laying out IO file (1 file / 512MiB)

randwrite: (groupid=0, jobs=1): err= 0: pid=32733: Tue Jan 21 22:55:47 2020
  write: IOPS=1040, BW=4163KiB/s (4263kB/s)(5120MiB/1259300msec)
    clat (usec): min=130, max=99933, avg=953.91, stdev=813.01
     lat (usec): min=131, max=99934, avg=954.83, stdev=813.07
    clat percentiles (usec):
     |  1.00th=[  194],  5.00th=[  241], 10.00th=[  285], 20.00th=[  461],
     | 30.00th=[  619], 40.00th=[  750], 50.00th=[  848], 60.00th=[  947],
     | 70.00th=[ 1045], 80.00th=[ 1237], 90.00th=[ 1598], 95.00th=[ 2114],
     | 99.00th=[ 3425], 99.50th=[ 4080], 99.90th=[ 9503], 99.95th=[12518],
     | 99.99th=[20841]
   bw (  KiB/s): min=  166, max=10009, per=19.98%, avg=4159.25, stdev=836.18, samples=2518
   iops        : min=   41, max= 2502, avg=1039.78, stdev=209.05, samples=2518
  lat (usec)   : 250=6.12%, 500=15.49%, 750=18.75%, 1000=25.58%
  lat (msec)   : 2=28.46%, 4=5.06%, 10=0.45%, 20=0.08%, 50=0.01%
  lat (msec)   : 100=0.01%
  cpu          : usr=0.74%, sys=5.46%, ctx=3522112, majf=0, minf=33
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=0,1310720,0,10 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=1
max_w_latency: (groupid=0, jobs=1): err= 0: pid=32734: Tue Jan 21 22:55:47 2020
  write: IOPS=1045, BW=4181KiB/s (4281kB/s)(5120MiB/1253932msec)
    clat (usec): min=131, max=88716, avg=949.90, stdev=813.62
     lat (usec): min=132, max=88718, avg=950.81, stdev=813.71
    clat percentiles (usec):
     |  1.00th=[  194],  5.00th=[  239], 10.00th=[  281], 20.00th=[  445],
     | 30.00th=[  619], 40.00th=[  742], 50.00th=[  848], 60.00th=[  938],
     | 70.00th=[ 1045], 80.00th=[ 1237], 90.00th=[ 1598], 95.00th=[ 2089],
     | 99.00th=[ 3425], 99.50th=[ 4080], 99.90th=[ 9503], 99.95th=[12649],
     | 99.99th=[20579]
   bw (  KiB/s): min=  166, max= 7032, per=20.08%, avg=4179.29, stdev=806.63, samples=2507
   iops        : min=   41, max= 1758, avg=1044.79, stdev=201.66, samples=2507
  lat (usec)   : 250=6.30%, 500=15.72%, 750=18.74%, 1000=25.50%
  lat (msec)   : 2=28.20%, 4=5.00%, 10=0.45%, 20=0.08%, 50=0.01%
  lat (msec)   : 100=0.01%
  cpu          : usr=0.71%, sys=5.45%, ctx=3945705, majf=0, minf=32
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=0,1310720,0,10 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=1
max_w_latency: (groupid=0, jobs=1): err= 0: pid=32735: Tue Jan 21 22:55:47 2020
  write: IOPS=1043, BW=4172KiB/s (4272kB/s)(5120MiB/1256610msec)
    clat (usec): min=132, max=99883, avg=951.84, stdev=812.55
     lat (usec): min=132, max=99884, avg=952.76, stdev=812.63
    clat percentiles (usec):
     |  1.00th=[  194],  5.00th=[  241], 10.00th=[  281], 20.00th=[  453],
     | 30.00th=[  619], 40.00th=[  742], 50.00th=[  848], 60.00th=[  938],
     | 70.00th=[ 1045], 80.00th=[ 1237], 90.00th=[ 1598], 95.00th=[ 2114],
     | 99.00th=[ 3458], 99.50th=[ 4080], 99.90th=[ 9503], 99.95th=[12780],
     | 99.99th=[20841]
   bw (  KiB/s): min=  166, max= 9924, per=20.04%, avg=4170.97, stdev=825.46, samples=2513
   iops        : min=   41, max= 2481, avg=1042.71, stdev=206.37, samples=2513
  lat (usec)   : 250=6.14%, 500=15.74%, 750=18.78%, 1000=25.49%
  lat (msec)   : 2=28.27%, 4=5.04%, 10=0.46%, 20=0.08%, 50=0.01%
  lat (msec)   : 100=0.01%
  cpu          : usr=0.72%, sys=5.45%, ctx=3952140, majf=0, minf=32
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=0,1310720,0,10 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=1
max_w_latency: (groupid=0, jobs=1): err= 0: pid=32736: Tue Jan 21 22:55:47 2020
  write: IOPS=1044, BW=4179KiB/s (4279kB/s)(5120MiB/1254612msec)
    clat (usec): min=131, max=89297, avg=950.34, stdev=811.43
     lat (usec): min=132, max=89299, avg=951.26, stdev=811.80
    clat percentiles (usec):
     |  1.00th=[  196],  5.00th=[  241], 10.00th=[  281], 20.00th=[  449],
     | 30.00th=[  619], 40.00th=[  742], 50.00th=[  848], 60.00th=[  938],
     | 70.00th=[ 1045], 80.00th=[ 1221], 90.00th=[ 1598], 95.00th=[ 2089],
     | 99.00th=[ 3425], 99.50th=[ 4080], 99.90th=[ 9503], 99.95th=[12649],
     | 99.99th=[20317]
   bw (  KiB/s): min=  166, max= 7088, per=20.07%, avg=4177.45, stdev=806.47, samples=2509
   iops        : min=   41, max= 1772, avg=1044.32, stdev=201.62, samples=2509
  lat (usec)   : 250=6.18%, 500=15.76%, 750=18.78%, 1000=25.52%
  lat (msec)   : 2=28.21%, 4=5.00%, 10=0.45%, 20=0.08%, 50=0.01%
  lat (msec)   : 100=0.01%
  cpu          : usr=0.72%, sys=5.44%, ctx=3944360, majf=0, minf=32
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=0,1310720,0,10 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=1
max_w_latency: (groupid=0, jobs=1): err= 0: pid=32737: Tue Jan 21 22:55:47 2020
  write: IOPS=1042, BW=4170KiB/s (4270kB/s)(5120MiB/1257414msec)
    clat (usec): min=130, max=88819, avg=952.49, stdev=810.88
     lat (usec): min=130, max=88821, avg=953.40, stdev=810.94
    clat percentiles (usec):
     |  1.00th=[  196],  5.00th=[  243], 10.00th=[  285], 20.00th=[  457],
     | 30.00th=[  619], 40.00th=[  742], 50.00th=[  848], 60.00th=[  938],
     | 70.00th=[ 1045], 80.00th=[ 1237], 90.00th=[ 1598], 95.00th=[ 2089],
     | 99.00th=[ 3425], 99.50th=[ 4080], 99.90th=[ 9503], 99.95th=[12649],
     | 99.99th=[20579]
   bw (  KiB/s): min=  166, max= 9792, per=20.02%, avg=4166.85, stdev=819.54, samples=2514
   iops        : min=   41, max= 2448, avg=1041.69, stdev=204.89, samples=2514
  lat (usec)   : 250=5.93%, 500=15.79%, 750=18.81%, 1000=25.60%
  lat (msec)   : 2=28.30%, 4=5.02%, 10=0.45%, 20=0.08%, 50=0.01%
  lat (msec)   : 100=0.01%
  cpu          : usr=0.70%, sys=5.47%, ctx=3948379, majf=0, minf=31
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=0,1310720,0,10 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=1

Run status group 0 (all jobs):
  WRITE: bw=20.3MiB/s (21.3MB/s), 4163KiB/s-4181KiB/s (4263kB/s-4281kB/s), io=25.0GiB (26.8GB), run=1253932-1259300msec

Disk stats (read/write):
  vda: ios=9172/11809121, merge=1839/4211594, ticks=17892/2373747, in_queue=1582802, util=59.89%
```


###Тест процессора

**Задача** - Провести тест процессора на нагрузку

Информация о процессоре

```
  PROCESSOR:          Intel Xeon E5-2699 v4
    Core Count:       1                                        
    Extensions:       SSE 4.2 + AVX2 + AVX + RDRAND + FSGSBASE 
    Cache Size:       16384 KB                                 
    Microcode:        0x1                                      

  GRAPHICS:           Cirrus Logic GD 5446
    Screen:           1024x768         

  MOTHERBOARD:        QEMU Standard PC
    BIOS Version:     1.10.2-1ubuntu1         
    Chipset:          Intel 440FX 82441FX PMC 
    Network:          Red Hat Virtio device   

  MEMORY:             1 x 1024 MB RAM QEMU

  DISK:               15GB
    File-System:      ext4                     
    Mount Options:    data=ordered relatime rw 
    Disk Scheduler:   MQ-DEADLINE              

  OPERATING SYSTEM:   CentOS Linux 7
    Kernel:           3.10.0-957.21.3.el7.x86_64 (x86_64)                                             
    Compiler:         GCC 4.8.5 20150623                                                              
    System Layer:     KVM                                                                             
    Security:         l1tf: Mitigation of PTE Inversion                                               
                      + mds: Vulnerable: Clear buffers attempted no microcode; SMT Host state unknown 
                      + meltdown: Mitigation of PTI                                                   
                      + spec_store_bypass: Vulnerable                                                 
                      + spectre_v1: Mitigation of Load fences __user pointer sanitization             
                      + spectre_v2: Mitigation of Full retpoline IBPB 
```

Результат теста - phoronix-test-suite benchmark smallpt

```
Test 1 of 1
    Estimated Trial Run Count:    3                     
    Estimated Time To Completion: 6 Minutes [20:42 UTC] 
        Started Run 1 @ 20:37:17
        Started Run 2 @ 20:41:27
        Started Run 3 @ 20:45:44

    Global Illumination Renderer; 128 Samples:
        246.11
        252.646
        246.042

    Average: 248.266 Seconds
    Deviation: 1.53%
```

Также результат на сайте [https://openbenchmarking.org](https://openbenchmarking.org/result/2001225-HU-RESUTLPRO22)



###Тест сети

**Задача** - Провести тест сети, используя утилиту iperf3

Запуск iperf сервера на с VPS
```bash
$ iperf3 -s -D -p <port>
```

Запуск iperf клиента на локальной машине
```bash
$ iperf3 -c <host> -p <port>
```

Результат:

```
Connecting to host <host>, port <port>
[  5] local 192.168.1.68 port 63599 connected to <remote-host> port <port>
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-1.00   sec  11.8 MBytes  99.4 Mbits/sec                  
[  5]   1.00-2.00   sec  5.37 MBytes  45.0 Mbits/sec                  
[  5]   2.00-3.00   sec  6.68 MBytes  56.0 Mbits/sec                  
[  5]   3.00-4.00   sec  7.64 MBytes  63.9 Mbits/sec                  
[  5]   4.00-5.00   sec  8.58 MBytes  72.3 Mbits/sec                  
[  5]   5.00-6.00   sec  8.59 MBytes  71.9 Mbits/sec                  
[  5]   6.00-7.00   sec  6.61 MBytes  55.5 Mbits/sec                  
[  5]   7.00-8.00   sec  8.43 MBytes  70.7 Mbits/sec                  
[  5]   8.00-9.00   sec  9.65 MBytes  80.8 Mbits/sec                  
[  5]   9.00-10.00  sec  10.0 MBytes  84.1 Mbits/sec                  
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-10.00  sec  83.4 MBytes  70.0 Mbits/sec                  sender
[  5]   0.00-10.07  sec  82.5 MBytes  68.7 Mbits/sec                  receiver

iperf Done.
```



Провети анализ рещультатов


### Нагрузочный тест на сервер приложения

Тест с помощью программи httperf

```bash
> httperf --server <host> --port 80 --uri / --num-conns=100 --rate=10 
```

Результат
```
Total: connections 1000 requests 1000 replies 1000 test-duration 20.064 s

Connection rate: 49.8 conn/s (20.1 ms/conn, <=7 concurrent connections)
Connection time [ms]: min 55.5 avg 66.8 max 282.0 median 64.5 stddev 12.4
Connection time [ms]: connect 16.1
Connection length [replies/conn]: 1.000

Request rate: 49.8 req/s (20.1 ms/req)
Request size [B]: 65.0

Reply rate [replies/s]: min 49.4 avg 49.8 max 50.2 stddev 0.4 (4 samples)
Reply time [ms]: response 34.6 transfer 16.1
Reply size [B]: header 289.0 content 14537.0 footer 2.0 (total 14828.0)
Reply status: 1xx=0 2xx=1000 3xx=0 4xx=0 5xx=0

CPU time [s]: user 3.14 system 16.79 (user 15.7% system 83.7% total 99.4%)
Net I/O: 724.8 KB/s (5.9*10^6 bps)

Errors: total 0 client-timo 0 socket-timo 0 connrefused 0 connreset 0
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0
```


Тест с помощью программи apache benchmark (ab)
```bash
> ab -n 1000 -c 50 -r <host>
```

Результат
```
Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /
Document Length:        14537 bytes

Concurrency Level:      50
Time taken for tests:   16.126 seconds
Complete requests:      1000
Failed requests:        0
Total transferred:      14811000 bytes
HTML transferred:       14537000 bytes
Requests per second:    62.01 [#/sec] (mean)
Time per request:       806.304 [ms] (mean)
Time per request:       16.126 [ms] (mean, across all concurrent requests)
Transfer rate:          896.92 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       15   19   3.7     18      52
Processing:    60  766  98.6    775     948
Waiting:       57  764  98.7    773     945
Total:         77  785  98.2    793     967

Percentage of the requests served within a certain time (ms)
  50%    793
  66%    811
  75%    825
  80%    835
  90%    868
  95%    892
  98%    928
  99%    935
 100%    967 (longest request)
```


### Нагрузочный тест на базу данных Mysql

Тест производился программой sysbench

```bash
> sysbench --test=oltp --oltp-table-size=100000 --mysql-host=localhost --mysql-db=loadtesting --mysql-user=root --mysql-password=12345 --mysql-table-engine=innodb prepare
Creating table 'sbtest'...
Creating 100000 records in table 'sbtest'...
```

```bash
> sysbench --test=oltp --oltp-table-size=100000 --mysql-host=localhost --mysql-db=loadtesting --mysql-user=root --mysql-password=12345 --mysql-table-engine=innodb run
```
Результат

```
Running the test with following options:
Number of threads: 1

Doing OLTP test.
Running mixed OLTP test
Using Special distribution (12 iterations,  1 pct of values are returned in 75 pct cases)
Using "BEGIN" for starting transactions
Using auto_inc on the id column
Maximum number of requests for OLTP test is limited to 10000
Threads started!
Done.

OLTP test statistics:
    queries performed:
        read:                            140000
        write:                           50000
        other:                           20000
        total:                           210000
    transactions:                        10000  (213.63 per sec.)
    deadlocks:                           0      (0.00 per sec.)
    read/write requests:                 190000 (4059.04 per sec.)
    other operations:                    20000  (427.27 per sec.)

Test execution summary:
    total time:                          46.8091s
    total number of events:              10000
    total time taken by event execution: 46.7363
    per-request statistics:
         min:                                  3.17ms
         avg:                                  4.67ms
         max:                                 61.38ms
         approx.  95 percentile:               6.26ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   46.7363/0.00
```
