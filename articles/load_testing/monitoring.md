Настройка мониторинга
-----------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> [Домашнее задание](homework.md)

План:
1) Настроить Promotheus и Grafana
2) Настроить дашборды 
     
   2.1) Для сервера (cpu, io disk, net)
   2.2) Для nginx
   2.3) Mysql


### --------------------------------

Настраиваем все в докер контейнерах, заводим отдельный docker-compose.mon.yaml для сервисов мониторинга.

```yaml
version: '3'

volumes:
  grafana_data:

networks:
  net-monitoring:
  net-app:

services:
  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    volumes:
      - ./docker/monitoring/prometheus/:/etc/prometheus/
    command:
      - '--config.file=/etc/prometheus/prometheus.yaml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.console.libraries=/etc/prometheus/console_libraries'
      - '--web.console.templates=/etc/prometheus/consoles'
      - '--storage.tsdb.retention=200h'
      - '--web.enable-lifecycle'
    networks:
      - net-monitoring
    expose:
      - 9090
    ports:
      - 9090:9090
    depends_on:
      - exporter-node
      - exporter-nginx

  exporter-node:
    image: prom/node-exporter:latest
    container_name: exporter-node
    privileged: true
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.ignored-mount-points=^/(sys|proc|dev|host|etc)($$|/)'
    restart: unless-stopped
    networks:
      - net-monitoring
    expose:
      - 9100

  exporter-nginx:
    image: nginx/nginx-prometheus-exporter:0.5.0
    container_name: exporter-nginx
    restart: unless-stopped
    environment:
      - SCRAPE_URI=http://n-nginx/nginx_status
      - TELEMETRY_PATH=/metrics
      - NGINX_RETRIES=10
    ports:
      - 9113:9113
    expose:
      - 9113
    networks:
      - net-monitoring
      - net-app

  grafana:
    image: grafana/grafana
    container_name: grafana
    volumes:
      - ./docker/monitoring/grafana/datasources:/etc/grafana/datasources
      - ./docker/monitoring/grafana/dashboards:/etc/grafana/dashboards
      - grafana_data:/var/lib/grafana
    environment:
      - GF_INSTALL_PLUGINS=grafana-piechart-panel
      - GF_SECURITY_ADMIN_USER=${ADMIN_USER:-admin}
      - GF_SECURITY_ADMIN_PASSWORD=${ADMIN_PASSWORD:-admin}
      - GF_USERS_ALLOW_SIGN_UP=false
    restart: unless-stopped
    ports:
      - 3000:3000
    networks:
      - net-monitoring
    depends_on:
      - prometheus
```
