Урок 3 - Нагрузочное тестирования
--------------------------------------

-> [Главная](../../README.md) -> Лекция

##Содержание:
- [Домашнее задание](homework.md)

####Полезные ссылки:
* Документация fio - [https://fio.readthedocs.io/en/latest/fio_doc.html](https://fio.readthedocs.io/en/latest/fio_doc.html)
* Тест диска - [http://blog.aboutnetapp.ru/archives/1204](http://blog.aboutnetapp.ru/archives/1204)
* Тест диска - [https://habr.com/ru/post/154235/](https://habr.com/ru/post/154235/)
* Тест диска - [https://hd.oblakoteka.ru/kb/a93/unix-os-fio_.aspx](https://hd.oblakoteka.ru/kb/a93/unix-os-fio_.aspx)
* Тест процессора - [http://www.phoronix-test-suite.com](http://www.phoronix-test-suite.com)
* Тест сети - [iperf3 GitHub](https://github.com/esnet/iperf)
* Тест сети - [iperf3 Docs](https://fasterdata.es.net/performance-testing/network-troubleshooting-tools/iperf/)