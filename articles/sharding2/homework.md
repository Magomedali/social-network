Домашнее задание
------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> Домашнее задание

### Задание:
- Реализовать диалоги между 2 пользователями вашей социальной сети.
- В качестве хранилища использовать отдельную базу данных MySQL.
- Все сообщения сохраняем в одну таблицу.
- Выбрать наилучший ключ шардирования. Обосновать выбор. Учесть, что:
- Некоторым пользователям могут писать намного чаще.
- Пользователи из разных геозон могут общаться тоже.
- Для шардинга использовать Vitess.

План выполнения:

- Описание кейса
- Миграция схемы
- Реализация

### Описание кейса 

- Имеем диалог на который подписываются два и более пользователей;
- В качестве ключа шардирования будет выступать идентификатор диалога;
- Все сообщения и подписки пользователей на диалог, будут на одном шарде.

### Миграция схемы

Таблица диалогов:

```mysql
CREATE TABLE network_conversations(
    id VARCHAR(36) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT network_conv_id_idx UNIQUE INDEX (id) USING HASH
) CHARACTER SET utf8 COLLATE utf8_general_ci
```

Таблица подписок на диалог:

```mysql
CREATE TABLE network_conversation_members(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    conversation_id VARCHAR(36) NOT NULL,
    member_id VARCHAR(36) NOT NULL,
    UNIQUE INDEX (conversation_id, member_id) USING HASH,
    CONSTRAINT network_conversation_members_c_id_idx FOREIGN KEY (conversation_id) REFERENCES network_conversations(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci
```

Таблица сообщении в диалогах:
```mysql
CREATE TABLE network_conversation_messages(
    id VARCHAR(36) NOT NULL,
    conversation_id VARCHAR(36) NOT NULL,
    from_member VARCHAR(36) NOT NULL,
    message TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT network_conv_msg_id_idx UNIQUE INDEX (id) USING HASH,
    CONSTRAINT network_conversation_messages_c_id_idx FOREIGN KEY (conversation_id) REFERENCES network_conversations(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci
```

Внешний ключ для member_id и from_member не создаем, так как пользователи и диалоги могут находиться на разных шардах


Денормализация: добавим имя пользователя в подписки пользователей на диалоги. 
Чтоб при получении диалогов с пользователями, не приходилось идти за именем пользователя

```mysql
ALTER TABLE network_conversation_members ADD COLUMN member_name varchar(255)  NULL
```

### Реализация

- Использую два шарда, обычные mysql сервера.
- Умный клиент
- Шардирование на основе хеша ключа

Класс с логикой определения шарда для ключа

```php
<?php

declare(strict_types=1);

namespace App\Tool;

use Doctrine\DBAL\Connection;

class DBController
{
    private $nodes;

    /**
     * DBController constructor.
     * @param Connection[] $nodes
     */
    public function __construct(array $nodes)
    {
        foreach ($nodes as $node) {
            $this->addShard($node);
        }
    }

    public function addShard(Connection $connection)
    {
        $this->nodes[] = $connection;
    }

    public function getShardNumber($key) {
        $hashId = $this->hash($key);
        return ($hashId % $this->getSize()) + 1;
    }

    public function getShardConnection($key): Connection
    {
        $num = $this->getShardNumber($key);
        return $this->nodes[$num-1];
    }

    protected function hash(string $key) {
        $md = md5($key);
        $bytes = unpack('C*', $md);

        return (($bytes[4] & 0xFF) << 24) | (($bytes[3] & 0xFF) << 16) | (($bytes[2] & 0xFF) << 8) | ($bytes[1] & 0xFF);
    }

    /**
     * @return Connection[]
     */
    public function nodes(): array
    {
        return $this->nodes;
    }

    public function getSize():int
    {
        return count($this->nodes);
    }
}
```

Код:
- [Контроллер](./../../app/src/Controller/Network/ConversationsController.php#L50)
- [Сервис](./../../app/src/Model/Network/UseCase/Conversation/Open/Handler.php)
- [Репозитории](./../../app/src/Model/Network/Repository/Conversation/SqlConversationRepository.php)
- [Поиск диалогов](./../../app/src/ReadModel/Network/Conversation/SqlConversationReport.php)


### TODO

- Увеличение количество шардов, попробовать реализовать решардинг данных
- Попробовать остальные кейсы шардирования
- Попробовать различные способы работы приложения с кластером БД(Умный клиент, Прокси, Zookeeper)
- Попробовать vitess
- Попробовать шардинг на постгресе
- Исследовать тему подробнее 