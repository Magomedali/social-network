Домашнее задание
------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> Домашнее задание

## Задание

Сделать копию таблицы пользователей из MySQL в Clickhouse. 
Сделать отчет по распределению пользователей по возрасту и полу на MySQL и Clickhouse. 
Сравнить время построения отчета. 
Объяснить результат.