Домашнее задание
-----------------------

-> [Главная](../../README.md) -> [Лекция](./README.md) -> Домашнее задание

## Задание:
- Настраиваем асинхронную репликацию.
- Выбираем 2 любых запроса на чтения (в идеале самых частых и тяжелых по логике работы сайта) и переносим их на чтение со слейва.
- Делаем нагрузочный тест по странице, которую перевели на слейв до и после репликации. Замеряем нагрузку мастера (CPU, la, disc usage, memory usage).
- ОПЦИОНАЛЬНО: в качестве конфига, который хранит IP реплики сделать массив для легкого добавления реплики. Это не самый правильный способ балансирования нагрузки. Поэтому опционально.

[Настройка master-slave в докере](https://github.com/vbabak/docker-mysql-master-slave)

### План:
- Наладить связь между серверами - ограничить доступ по IP
- Сделать бекап базы
- Создать новую базу на другом сервер и загрузить бекап
- Настроить репликацию master - slave
- Реализовать механизм распределения запросов в приложении
- Провести повторные нагрузочные тесты


## Настройка сервера

Основное приложение работает на докер контейнерах, там же и контейнер c MYSQL.

Арендовали новый VPS такой же как и первый, только OS Ubuntu 18, а на первом Centos 7. 
На новом сервере настроим базу для master, slave настроим на старом там же где и само приложение,
чтоб приложение читало с локальной базы. 

Сразу настроим файрволл, вначале закроем mysql port для всех подключении из вне, а потом разрешим подключение из старого сервера:

```bash
$ iptables -I INPUT -p tcp --dport=mysql -j DROP
$ iptables -I INPUT -p tcp --dport=mysql -s <ip_server>  -j ACCEPT
```

Смотрим на созданные правила и их очередность:
```bash
$ iptables -L INPUT
ACCEPT     tcp  --  <ip_server>          anywhere             tcp dpt:mysql
DROP       tcp  --  anywhere             anywhere             tcp dpt:mysql
```

Сохраняем настройки файрволла
```bash
$ iptables-save
```

Тоже самое выполняем на старом сервере

На старом сервере разрешим подключение к базе данных с нового сервера
```bash
$ iptables -I DOCKER -p tcp --dport=mysql -s <ip_new_server> -j ACCEPT
$ iptables-save
```

Устанавливаем MYSQL

```bash
$ apt-get update -y
$ apt-get install -y mysql-server
```

Приложению надо дать возможность подключаться к mysql удаленно, для этого чуть подправим конфиги mysql.

В конфигурационном файле my.cnf укажем bind_address=0.0.0.0 и 
сразу же установим максимальный размер принимаемого пакет - это понадабится для загрузки бекапа
```
bind_address=0.0.0.0
max_allowed_packet=2GB
```

Добавим в MYSQL пользователя для приложения и дадим ему все права:
```sql
> CREATE USER 'app_user'@'%' IDENTIFIED BY 'password';
> GRANT ALL PRIVILEGES ON *.* TO 'app_user'@'%';
```

На этом заканчиваем с настройками доступа между серверами

## Бекап и восстановление базы

Создадим базу данных:
```sql
mysql> CREATE DATABASE network charset utf8;
```

Бекап будем делать с помощью утилиты mysqldump. На новом сервер выполняем команду:
```bash
$ mysqldump -h <ip_server> -u app_user -p<pass> --single-transaction --insert-ignore --lock-tables --routines <dbname> | gzip > backup.gz.sql
```

После того как бекап будет готов заливаем его в будущий мастер:
```bash
$ gunzip < backup.gz.sql | mysql -u <user_name> -p <db_name>
```

Теперь у нас две идентичные базы данных на двух разных серверах.


## Настройка репликации

В первую очередь переключим приложение на новый сервер(будущий мастер), чтоб записывали и читали из него.


### Настройка мастера

Добавим пользователя с правами для реплицирования базы данных:
```mysql
mysql> GRANT REPLICATION SLAVE ON *.* TO 'network_slave'@'%' IDENTIFIED BY "12345";
mysql> FLUSH PRIVILEGES;
```

Прописываем следующие настройки в my.cnf:
```
server-id               = 1
log_bin                 = /var/log/mysql/mysql-bin.log
binlog_do_db            = network
expire_logs_days        = 10
max_binlog_size         = 100M
```

Перезапустим mysql service:
```bash
$ systemctl restart mysql
```

Проверяем статус:
```mysql
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |      154 | network      |                  |                   |
+------------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

Видим binary log файл и текущая позиция(154)

### Настройка слейва

Настраиваем следующие параметры my.cnf на слейве:
```
server-id=2
relay-log=/var/log/mysql/mysql-relay-bin.log
log_bin=/var/log/mysql/mysql-bin.log
binlog_do_db=network
```

Перезапускаем и заходим в mysql. Связиваем с мастером:
```mysql
mysql> CHANGE MASTER TO MASTER_HOST='<ip_master_server>', MASTER_USER='network_slave', MASTER_PASSWORD='12345', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
mysql> START SLAVE;
mysql> SHOW SLAVE STATUS\G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: <ip_master>
                  Master_User: network_slave
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 154
               Relay_Log_File: mysql-relay-bin.000002
                Relay_Log_Pos: 320
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 154
              Relay_Log_Space: 527
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: 08cacb85-4b34-11ea-a269-00e781754ab9
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)
```

Потоки I/O и SQL запущены, репликация настроена. Посмотри процесс лист:
```mysql
mysql> SHOW PROCESSLIST;
+----+-------------+-----------+------+---------+------+--------------------------------------------------------+------------------+
| Id | User        | Host      | db   | Command | Time | State                                                  | Info             |
+----+-------------+-----------+------+---------+------+--------------------------------------------------------+------------------+
|  3 | root        | localhost | NULL | Query   |    0 | starting                                               | SHOW PROCESSLIST |
|  4 | system user |           | NULL | Connect |  305 | Waiting for master to send event                       | NULL             |
|  5 | system user |           | NULL | Connect |  305 | Slave has read all relay log; waiting for more updates | NULL             |
+----+-------------+-----------+------+---------+------+--------------------------------------------------------+------------------+
3 rows in set (0.00 sec)
```

Видим 2 процесса которые ждут пока мастер отправит новые события и поступят новые изменения в relay log, соответственно.

Посмотрим на мастере процессы:
```mysql
mysql> SHOW PROCESSLIST;
+----+--------------------+--------------------+------+-------------+------+---------------------------------------------------------------+------------------+
| Id | User               | Host               | db   | Command     | Time | State                                                         | Info             |
+----+--------------------+--------------------+------+-------------+------+---------------------------------------------------------------+------------------+
|  2 | root               | localhost          | NULL | Query       |    0 | starting                                                      | SHOW PROCESSLIST |
|  3 | network_slave      | <ip_slave>:49620   | NULL | Binlog Dump |  449 | Master has sent all binlog to slave; waiting for more updates | NULL             |
+----+--------------------+--------------------+------+-------------+------+---------------------------------------------------------------+------------------+
2 rows in set (0.00 sec)
```

Видим Binlog Dump который ждет новых изменении.

Попробуем изменить имя какого-нибудь пользователя на мастере:
```mysql
mysql> UPDATE user_users SET name='Ali' where id='b71046a4-d8bd-4344-9f7f-963f14a59eef';
```

Посмотрим статус на мастере:
```mysql
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |      838 | network      |                  |                   |
+------------------+----------+--------------+------------------+-------------------+
1 row in set (0.01 sec)
```

Позиция изменилась.

Посмотри статус слейва:
```mysql
mysql> SHOW SLAVE STATUS\G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: <ip_master>
                  Master_User: network_slave
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 838
               Relay_Log_File: mysql-relay-bin.000002
                Relay_Log_Pos: 1004
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 838
              Relay_Log_Space: 1211
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: 08cacb85-4b34-11ea-a269-00e781754ab9
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)
```

Read_Master_Log_Pos изменился с 154 на 838;

Смотрим на слейве, измененную запись на мастер:

```mysql
mysql> SELECT id,name FROM user_users where id='b71046a4-d8bd-4344-9f7f-963f14a59eef';
+--------------------------------------+------+
| id                                   | name |
+--------------------------------------+------+
| b71046a4-d8bd-4344-9f7f-963f14a59eef | Ali  |
+--------------------------------------+------+
1 row in set (0.00 sec)
```

Репликация настроена.


## Реализовать механизм распределения запросов в приложении

Прописываем конфиги для соединения с Базами данных, в слейвах указываем и мастера, будем использовать его и для чтения.

```yaml
db.default:
    class: Doctrine\DBAL\Connections\MasterSlaveConnection
    factory: Doctrine\DBAL\DriverManager::getConnection
    arguments:
      $params:
        wrapperClass: Doctrine\DBAL\Connections\MasterSlaveConnection
        driver: pdo_mysql
        master:
          host: '%env(MASTER_DB_HOST)%'
          port: ~
          user: '%env(MASTER_DB_USERNAME)%'
          password: '%env(MASTER_DB_PASSWORD)%'
          dbname: '%env(MASTER_DB_NAME)%'
          charset: UTF8
        slaves:
          -
            host: '%env(MASTER_DB_HOST)%'
            port: ~
            user: '%env(MASTER_DB_USERNAME)%'
            password: '%env(MASTER_DB_PASSWORD)%'
            dbname: '%env(MASTER_DB_NAME)%'
            charset: UTF8
          -
            host: '%env(SLAVE_DB_HOST)%'
            port: ~
            user: '%env(SLAVE_DB_USERNAME)%'
            password: '%env(SLAVE_DB_PASSWORD)%'
            dbname: '%env(SLAVE_DB_NAME)%'
            charset: UTF8
```


## Повтор нагрузочных тестов

### Состояние до теста

![Состояние до теста](./testBeforeRepl.png)

### Профиль пользователя

### Apache Benchmark - ab

Результаты теста до репликации:
- Time taken for tests:   15.901 seconds
- Requests per second:    62.89 [#/sec] (mean)
- Time per request:       1590.068 [ms] (mean)
- Time per request:       15.901 [ms] (mean, across all concurrent requests)

```bash
$ ab -n 1000 -r -c 100 http://5.181.108.42/members/show?id=b71ab99b-c4ef-4997-98f8-3a14e1f89002
Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /members/show?id=b71ab99b-c4ef-4997-98f8-3a14e1f89002
Document Length:        1565 bytes

Concurrency Level:      100
Time taken for tests:   13.975 seconds
Complete requests:      1000
Failed requests:        0
Total transferred:      1839000 bytes
HTML transferred:       1565000 bytes
Requests per second:    71.56 [#/sec] (mean)
Time per request:       1397.479 [ms] (mean)
Time per request:       13.975 [ms] (mean, across all concurrent requests)
Transfer rate:          128.51 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       14   20   7.7     17      72
Processing:    66 1310 226.2   1353    1495
Waiting:       56 1310 226.2   1353    1495
Total:         83 1330 223.9   1371    1548

Percentage of the requests served within a certain time (ms)
  50%   1371
  66%   1409
  75%   1430
  80%   1444
  90%   1463
  95%   1476
  98%   1494
  99%   1502
 100%   1548 (longest request)
```

Результаты теста после репликации
- Time taken for tests:   13.975 seconds
- Requests per second:    71.56 [#/sec] (mean)
- Time per request:       1397.479 [ms] (mean)
- Time per request:       13.975 [ms] (mean, across all concurrent requests)


#### Поиск пользователей

#### Apache Benchmark - ab

##### Общее количество - 100 запросов, одновременно 10 запросов


Результаты теста до репликации:
- Time taken for tests:   4.804 seconds;
- Requests per second:    20.82 [#/sec] (mean)
- Time per request:       480.383 [ms] (mean)
- Time per request:       48.038 [ms] (mean, across all concurrent requests)

```bash
$ ab -n 100 -r -c 10 http://5.181.108.42/?key=O%27Conner&limit=50
Server Software:        nginx/1.15.12
Server Hostname:        5.181.108.42
Server Port:            80

Document Path:          /?key=O%27Conner
Document Length:        14847 bytes

Concurrency Level:      10
Time taken for tests:   3.926 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      1512100 bytes
HTML transferred:       1484700 bytes
Requests per second:    25.47 [#/sec] (mean)
Time per request:       392.559 [ms] (mean)
Time per request:       39.256 [ms] (mean, across all concurrent requests)
Transfer rate:          376.16 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       15   20   4.2     19      38
Processing:    48  271 348.5    148    1383
Waiting:       45  268 348.3    147    1380
Total:         77  290 348.6    168    1403

Percentage of the requests served within a certain time (ms)
  50%    168
  66%    195
  75%    223
  80%    261
  90%   1166
  95%   1268
  98%   1401
  99%   1403
 100%   1403 (longest request)
```

Результаты теста после репликации
- Time taken for tests:   3.926 seconds
- Requests per second:    25.47 [#/sec] (mean)
- Time per request:       392.559 [ms] (mean)
- Time per request:       39.256 [ms] (mean, across all concurrent requests)


### Состояние после теста

![Состояние до теста](./testAfterRepl.png)

Итог:
- Ускорение не большое
- Нагрузка на сервер с приложением значительно снизилась