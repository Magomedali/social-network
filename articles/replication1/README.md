Урок 6 - Репликация Часть 1
--------------------------------------------------

-> [Главная](../../README.md) -> Лекция 

- [Домашнее задание](./homework.md)

## Репликация

### Цель занятия:
- Понять, что такое репликация и зачем она нужна
- Обсудить виды репликации и связанные механизмы
- Понять разницу между подходами к репликации
- Сравнить особенности репликации в MySQL и PostgreSQL
- Познакомиться с групповой репликацией в MySQL

Репликая - это запущенный процесс копирования(перенос) данных с одной базы данных на другой во время работы приложения.
Один из способов масштабирования системы и распределения нагрузки. 

Репликацию - это не бекап, некоторые путают с резервным копированием данных. 
Репкликацию можно использовать для повышения определенной надежности, 
то есть держать запасной сервер базы данных для подмены основной в случае отказа второго.
Репликация позволяет распределить нагрузку чтения на несколько серверов.
За дополнительные сервера придеться заплатить


##### Репликая - это:
- один из способов масштабирования
- можно использовать много серверов для обработки запросов
- не backup
- не магия
- вам придеться заплатить


### Масштабирование

Репликация не позволяет ускорить запись, так как репликация подразумевает, 
что все ноды базы данных в конечном итоге должны иметь идентичную копию друг друга. 
При записи в один сервер, придеться записывать на все остальные сервера. Поэтому репликация не помогает ускорить запись.

Но репликация позволяет ускорить чтение, можно распределить запросы на чтения между серверами и помогает при падениях одного из сервера базы данных


- не ускоряет запись
- ускоряет чтение
- помогает при падениях сервера БД


## Виды репликации по схемам

### Схема Master - Slave

Master-Slave репликация - это схема репликации где запись происходит в один сервер(Master), а чтение осуществляется из других серверов(Slave).
Из Master сервера также возможно чтение. 

Slave - сервера следуют за Master сервером. При изменении состояния мастера, мастер передает изменения на слейвы.

Распределение запросов реализуется на уровне приложения.

- один источник данных для slave`ов
- наиболее распространенная схема репликации
- относительно простая схема
- отключить/включить реплику
- master иногда падает

Плюсы:
- легко подключить еще один slave - распределить нагрузку на чтение
- при падении одного slave приложение продолжит работать без сбоев, уменьшится скорость обработки запросов на чтение

Минусы:
- Единая точка отказа - при падении мастера, при записи приложение даст сбой
- Ручное назначение master`а и переконфигурция slave серверов


### Схема Master - Master

Master-Master - схема репликации в которой учавствуют два master сервера. запись и чтение осуществляется с двух серверов. 
Относительно сложная схема и менее распространенная схема. 

Нет единой точки отказа, но добавляются некоторые проблемы. Сложно гарантировать консистентность между серверами, 
данные могут съехаться если попытаться изменить одни и теже данные одновременно на двух серверах. 
Каждая нода попытается реплицировать свои изменения в другую.

Плюсы:
- нет единой точки отказа - при падении одного сервера, приложение может работать без перебоев

Минусы
- Нарушение консистентности данных

На практике при использовании данной схеме используют дополнительные инструмент, 
которые помогают обеспечить консистентность данных. 

Например ZooKeeper. (Изучить данный вопрос дополнительно)

- нет единой точки отказа
- постоянное время работы
- легкий failover
- нет консистентности (все сломается обязательно)
- group replication?


## Виды репликации по синхронизации между нодами

### Синхронизация

#### Sync - синхроная репликация. 

При поступлении запроса на запись, в первую очередь запись осуществляется локально - закомитили локально,
потом запись отправляется на реплицируемые ноды - закомили удаленно на всех серверах,
потом возвращается ответ приложению о том что все записано.

То есть, пользователю ответ придет только после того как запись будет закомичена на всех серверах

Плюс:
- надежность - пока данные успешно не запишутся на всех нодах, запись не подтвердиться
- гарантия консистентности - сделанные изменения видны на всех нодах

Минусы:
- затрачивается больше времени на обработку записи

Синхронная репликая используется в PostgreSQL (по умолчанию)


#### Async - асинхронная репликация

При посуплении запроса на запись, запись осуществляется локально и возвращается ответ пользователю, что все ок
и далее в фоне отправляет изменения на реплицируемые сервера.
Нет гарантии, что запись дойтет до реплицируемых серверов и запишутся успешно.
При записи в мастер и при попытке прочитать эти данные из слейва, этих данных там может не оказаться, по разным причинам.

- Либо данные не дошли до слейва;
- Данные дошли но не записались;
- Либо данные еще не успели записаться на слейве.

**Один из вариантов избежать последнюю проблему - это чтение только что записанных данных из мастера**

Плюсы:
- быстрый ответ клиенту

Минусы:
- Возможность нарушения консистентности
- вероятность прочитать неактуальных данных

Используется в MySQL, PostgreSQL.


#### Semi-Sync - полусинхронная репликация

При поступлении запроса на запись, запись обрабатывается локально, далее запись доставляется до слейвов. 
Слейвы в свою очередь отвечают мастеру с подтверждением о получении записи, а далее слейвы обрабатывают полученню запись.
А мастер при получении подтверждения (ACK) от слейвов коммитит запись и отвечает клиенту что все ок. Но нет гарантии что слейвы 
после получения записи успешно его обработали.


Используется в MySQL.

В какой-то мере semi-sync позволяет организовать балансированную репликацию между синхронной и асинхронно,
с плюсами и минусами обоих видов репликации.


## Репликация в MySQL


Master нода - принимает запрос на запись, выполняет его и кладет изменения в свой binary log
Slave нода - на слейв ноде запускаются два треда(потока):
- Тред I/O, который следит за binary логом на ноде мастера. I/O тред при обнаружении информации от мастера(master info) кладет их у себя на ноде
  в relay log
- Тред SQL, следит за relay log`ом при появлении новый изменении, выполняет у себя эти изменения.

Master:
- обрабатывает запросы
- делает транзакции
- пишет binary logs

Slave:
- стягивает изменения с мастера и кладет их в relay log
- читает данные из relay log и применяет изменения(накатывает на базу)


Потоки:
- master: binlog dump thread (SHOW PROCESSLIST на мастере)
- slave: I/O thread (SHOW SLAVE STATUS на слейве)
- slave: SQL thread

[Подробное описание репликации в MYSQL](https://dev.mysql.com/doc/refman/5.7/en/replication-implementation-details.html)

## Форматы репликации

Речь пойдет о форматах изменений, которые кладутся в binary log для дальнейшего реплицирования на слейвы.

Форматы:
- statement based: в binary log кладутся самы запросы которые привели к изменению
- row based: в binary log кладутся сами измененные данные
- mixed: комбинированный формат из обоих двух форматов - база сама решает какой формат использовать для конкретных изменении


MySQL по умолчанию использует mixed, а в 8 версии row based;
[Форматы](https://dev.mysql.com/doc/refman/5.7/en/binary-log-setting.html)

### Statement based binary logs (SBR)

В binary logs записываются сами SQL запросы, которые привели к изменениям. 
Слейвы в смовю очередь получают эти запросы и выполняют на своей стороне.

Преимущества:
- По сети отправляются сами запросы, на много меньше самих данных - нагрузка на сеть маленькая
- Все запросы в логе - легко анализировать

Недостатки:
- Запросы выполнятся столько раз сколько и серверов в общем. Например небольшой запрос который приведет к изменениию 1млн строк, 
  будет выполняться на каждом из слейвов - многократное выполнение запроса
- Если запросы содержат функции которые определяют текущую дату, генерирует случайную строку, uuid и т.д., то на каждом из слейве
  эти функции дадут разное значение - нарушение нконсистентности данных

[Опасные и безопасные выражения для (SBR)](https://dev.mysql.com/doc/refman/8.0/en/replication-rbr-safe-unsafe.html)

### Row Based Replication (RBR)

В binary log записываются сами измененные данные и реплицируются уже готовые данные. На слейвах не нужно выполнять повторно запросы.
Данные хранятся и передаются в бинарном формате

Преимущества:
- Бинарный формат
- Передаются уже готовые данные


Недостатки:
- по сети передаются большие данные
- непонятны границы statementa - т.е. нет четких границ, которыйе определяет какие запросы привели к изменениям. (Где Insert был, где update был и т.д.)
- трудно читать - анализировать
- before/after image - данные до и после, то есть в binary logs будет записи до имзенения и после - дублирование

Проблему с before/after image  можно уже решить. 

Параметр binlog_row_image:
- full - полная: при изменении одного поля у кортежа, запишется весь кортеж до и после
- minimal - минимальный: запишется только поле которое изменилось
- noblob - похожа на full, но поля типа BLOB и TEXT игнорируются если не были изменены

[binlog_row_image](https://dev.mysql.com/doc/refman/5.7/en/replication-options-binary-log.html#sysvar_binlog_row_image)


### Mixed

База комбинирует между SBR и RBR. 

База дополнительно отслеживает опасные функции(UUID, NOW и т.д.) для таких запросов используется RBR.

## Фильтры репликации

По умолчанию реплицируются все данные. Можно реплицировать часть данных(Конкретную базу данных, таблицы). 
Для этого используются фильтры.

Нужно использовать осторожно.

Опции:
- replicate_do_db
- replicate_ignore_db
- replicate_do_table
- ...

[https://dev.mysql.com/doc/refman/5.7/en/change-replication-filter.html](https://dev.mysql.com/doc/refman/5.7/en/change-replication-filter.html)


## Позиционирование

Слейв ноды должны как-то отслеживать изменения на мастере, для этого мастер делится со слейвами определенно информацией характеризующая
текущее положение мастера

### Binary log position

**Старый решение. До 8 версии используется по умолчанию**

Файл с позицией отклонения Например файл mysql-bin.00078 : позиция 44
Если слейв видит что позиция изменилась то слейв запрашивает данные

Недостатки:
- если файл потеряется то репликация сломается
- локальный для сервера
- обязательно разъедется

### GTID (SOURCE_ID:TRANSACTION_ID)


SOURCE_ID - идентификатор кластера
TRANSACTION_ID - id транзакции, инкремент

- Глобален на кластер,  генерируется автоматически при коммите
- бесплатная трасировка
- простой slave promotion - простая трансформация слейва до мастера
- используйте его

[https://dev.mysql.com/doc/refman/5.7/en/replication-gtids.html](https://dev.mysql.com/doc/refman/5.7/en/replication-gtids.html)



## Паралельная репликация

Поумолчанию используется однопоточная репликация.  В MySQL можно настроить параллельную(многопоточную) репликацию.
Возможность параллелизации связано исключительно из-за архитектуры MySQL и реализации репликации в нем.

- обычно используется однопоточная репликация всех данных
- с MySQL5.6 можно реплицировать параллельно несколько баз данных
- с MySQL5.7 можно реплицировать параллельно одни и те же таблицы

Полезные опции: 
- slave-parallel-workers
- slave-parallel-type (DATABASE|LOGICAL_CLOCK)

[https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html)

Сценарии:
- 1 мастер и 3 слейва
- Первый реплицирует в 1 потоков
- Второй реплицирует в 20 потоков
- Третий реплицирует в 30 потоков
- вставка в 25 различных таблиц внутри одной базы в 100 потоков (sysbench)

* в много поточном режиме выполняется работа, который выполнял SQL поток в слейве. (Напомним что на slave два потоко(I/O Thread and SQL Thread))

Спойлер: 
- В данном случае при 20 потоков был самый маленький slave lag(отставание от мастера). 
  Не всегда количество потоков играет роль, чем больше потоков, тем больше context switch`ов

[https://www.percona.com/blog/2016/02/10/estimating-potential-for-mysql-5-7-parallel-replication/](https://www.percona.com/blog/2016/02/10/estimating-potential-for-mysql-5-7-parallel-replication/)

### Пару слов об архитектуре MySQL

В MySQL за репликацию(binary log) и за записи данных на диск отвечают две разные уровни абстракции.
За запись на диск отвечает I/O, а за репликацию отвечает CPU - т.е. репликация организовано на логическом уровне, 
поэтому в MySQL есть поддержка разных форматов репликации и т.д.

Такая архитектура позволяет выполнять задачу(работа CPU) несколькими потоками параллельно.

## Репликация в Postgres

В Postgres есть Write-Ahead Logging(WAL) - это журнал, то куда в первую очередь записываются изменения произведенные SQL запросам. 
Только после того как данные будут зафиксированы в WAL(на физическом устройствем), данные будут записаны в самой базе.

Такой механизм дает возможность воссоздать потерянные данные вовремя сбоя. Также репликация реализовано на уровне WAL - WAL Sender.

На мастере WAL Sendor отправляет WAL records на слейвы. На слейвах WAL Receiver получает данные и записывает. 
Реплицируются блоки измененных физических страниц. 
Репликация в Postgres связана с I/O, поэтому нет параллельных репликации и разных форматов и тд

Write-Ahead log:
- физические изменения страниц (значение в блоке 2564 равно 154)
- сюда попадают абсолютно все операции
- один журная на все

[https://wiki.postgresql.org/images/a/af/FOSDEM-2015-New-WAL-format.pdf](https://wiki.postgresql.org/images/a/af/FOSDEM-2015-New-WAL-format.pdf)


### в MySQL

- есть binlog
- у некоторых движков есть аналог WAL (InnoDB Undo/Redo Log)
- с точки зрения MySQL - это разные логи!
- нарушение абстракций

MySQL пишет больше данных для репликации (~1.5 раза)
[https://dev.mysql.com/doc/refman/5.7/en/innodb-undo-logs.html](https://dev.mysql.com/doc/refman/5.7/en/innodb-undo-logs.html)


### Логическая репликация
- тот самый row based формат
- работает с кортежами данных
- не знает, как они хранятся на диске
- CPU-bound, можно параллелить по процессорам

В Postgres10+ тоже есть - [https://www.postgresql.org/docs/10/logical-replication.html](https://www.postgresql.org/docs/10/logical-replication.html)


### Физическая репликация
- работает со страницами
- slave = master байт в байт
- Postgres WAL, InnoDB Undo/RedoLog - как примеры работающих со страницами
- IO-bound, нет смысла параллелить

## Введение в групповую репликацию

Групповая репликация в MySQL реализуется с помощью дополнительного плагина.
Это группа нод(n <= 9), который решает следующие проблемы:
- реализовать синхронную репликацию
- реализовать master-master схему - можно писать в любую из нод(По умолчанию в одну ноду)
- flow control - если нода слишком отстала, то выходит из группы
- обеспечение консистентности данных

Плагин начиная с версии MySQL5.7

Можно попробовать групповую репликацю в докере:
- [https://mysqlhighavailability.com/setting-up-mysql-group-replication-with-mysql-docker-images/](https://mysqlhighavailability.com/setting-up-mysql-group-replication-with-mysql-docker-images/)


Документация:
- [https://dev.mysql.com/doc/refman/5.7/en/group-replication.html](https://dev.mysql.com/doc/refman/5.7/en/group-replication.html)
- [Обзор, бенчмарки, сравнения](http://mysqlhighavailability.com/performance-evaluation-mysql-5-7-group-replication/)


## Доп материалы:
- [MySQL Replication — Advanced Features / Петр Зайцев (Percona)](https://www.youtube.com/watch?v=ppI74hTuXO0)
- [Асинхронная репликация без цензуры / Олег Царёв (Mail.Ru Group)](https://www.youtube.com/watch?v=_r8vLPTl0PE)
- [Отладка и устранение проблем в PostgreSQL Streaming Replication](https://habr.com/ru/company/oleg-bunin/blog/414111/)
- [An Overview of Logical Replication in PostgreSQL](https://severalnines.com/database-blog/overview-logical-replication-postgresql)
- документация MySQL
- документация Postgres
- блог Percona