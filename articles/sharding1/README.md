Шардирование часть 1
------------------------

-> [Главная](../../README.md) -> Лекция

## Цели занятия:
- Узнать что такое шардирование
- Понять какую проблему решает и для чего нужно
- Познакомиться с видами и стратегиями шардирования
- Обсудить проблемы и узкие места различных подходов
- Поговорить о решардинге, консистентном хешировании

## Проблема:
- бизнес растет -> данные растут
- время обработки запросов растет
- сервера ограничены (CPU, RAM, Disk, ...)

## Решение проблемы с помощью вертикального масштабирования:
- докупить то, чего не хватает (CPU, RAM, Disk, ...)
- поможет (скорее всего временно)
- есть предел


## Решение проблемы с помощью горизонтального масштабирования:

### Партиционирование

- Разделяем данные по определенному признаку и храним раздельно
- разделенные данные физически лежат отдельно
- все в пределах одного сервера!
- бывает разных типов
- в теории, должно работать быстрее (?)

#### Основные типы партиционирования

- key - разделение данных по какому-нибудь ключу
- range - разделение данных по диапозону
- list - 
- hash - разделение данных по хэшу

####  Пример: MYSQL

```mysql
 CREATE TABLE members (
    firstname VARCHAR(25) NOT NULL,
    lastname VARCHAR(25) NOT NULL,
    username VARCHAR(16) NOT NULL,
    email VARCHAR(35),
    joined DATE NOT NULL
)
PARTITION BY RANGE( YEAR(joined) ) (
    PARTITION p0 VALUES LESS THAN (1960),
    PARTITION p1 VALUES LESS THAN (1970),
    PARTITION p2 VALUES LESS THAN (1980),
    PARTITION p3 VALUES LESS THAN (1990),
    PARTITION p4 VALUES LESS THAN MAXVALUE
);
```

[https://dev.mysql.com/doc/refman/8.0/en/partitioning.html](https://dev.mysql.com/doc/refman/8.0/en/partitioning.html)

#### Пример: PostgreSQL

```postgres-psql
 CREATE TABLE measurement (
        city_id int not null,
        logdate date not null,
        peaktemp int,
        unitsales int
) PARTITION BY RANGE (logdate);
CREATE TABLE measurement_y2006m02 PARTITION OF measurement
    FOR VALUES FROM ('2006-02-01') TO ('2006-03-01');
CREATE TABLE measurement_y2006m03 PARTITION OF measurement
    FOR VALUES FROM ('2006-03-01') TO ('2006-04-01');
```

[https://www.postgresql.org/docs/10/ddl-partitioning.html](https://www.postgresql.org/docs/10/ddl-partitioning.html)


#### Что можем получить от партицирования

- может ускорить запросы - при поиске данных по ренжированному полу и тд
- может замедлить запросы - при обновлении данных, если партиции очень много, поиск данных не потому признаку по которому были разделены данные

Не решает проблему ограниченности сервера!


### Шардирование

Шардирование - разделение данных по нескольким физическим серверам

- один из вариантов масштабирования
- разбиение данных на куски
- данные живут физически на разных серверах (ну обычно так)
- не репликация - !
- не партиционирование - !

#### Для чего использовать шардирование:
- горизонтальное масштабирование
- ускорить обработку запросов (особенно запись!)
- повысить отказоустойчивость(*)

#### Как шардировать данные
- посмотрите на свои данные
- посмотрите на свои запросы
- выберите правильно критерий шардирования - вам с ним жить!

Говорят шардирование и его поддержка очень трудоемкое удовольствие, используйте шардирование только в крайнем случае, 
когда индексы, репликация и партиционирование не помогают

#### Типы шардирования

##### Key or Hash based Sharding

- еще называют hash based
- формула примерно такая: F(key) -> shard_id F и key очень важны
- наиболее распространенный способ

Плюсы и минусы:
- просто и понятно (+)
- равномерное распределение (+) 
- алгоритмическое распределение (+) 
- добавление/удаление шарда всегда боль (-)

#### Range Based Sharding - характеристики

- может называться table function/virtual bucket
- статический конфиг range -> shard
- формула примерно такая: hash(key) -> virtual_bucket -> shard_id

- прост в реализации (+)
- потенциально неравномерная нагрузка (-) 
- обновление (-)

#### Directory Based Sharding - характеристики
- похож на range based 
- отображение key -> shard_id
- low cardinality для key - наш случай


- гибкость (+) 
- обновление (-) 
- SPOF (-)

### Routing

Варианты:
- из приложения (умный клиент) 
- прокси
- координатор
- есть еще...

#### Умный клиент
- простой метод
- нет лишних хопов
- как обновлять/решардить? 

#### Прокси
- приложение вообще не знает о шардинге 
- код не меняется
- лишний хоп (*)
- могут падать
- им нужно общаться

[https://www.proxysql.com https://shardingsphere.apache.org/document/current/en/manual/sharding-proxy](https://www.proxysql.com https://shardingsphere.apache.org/document/current/en/manual/sharding-proxy)

#### Координатор

- приложение вообще не знает о шардинге 
- код не меняется
- лишний хоп
- нагрузка
- SPOF


### Проблемы шардинга

Плохо распределили данные:
- Выход:
  - подобрать лучший ключ/функцию шардирования
  - решардинг (если имеет смысл)
  
JOIN:
- Выход:
  - держать нужные данные на одной машине
  - делать вычисления на одной машине
  

### Решардинг

- добавление/удаление нод
- потребовалось "передвинуть данные" 
- скорее всего придется решардить когда-то

#### Решардинг "в лоб"
В лоб:
- в худшем случае нужно перемешать все данные 
- если степени 2ки - уже неплохо (переместить ~50%)

Пример:
- формула shard_id % count
- 16 записей на 8 шардов => 2 записи на шард 
- 16 записей на 16 шардов => 1 запись на шард

#### Consistent hashing

- используем одну hash-функцию для нод и данных 
- выбирается ближайший по часовой стрелке узел
- при добавлении/удалении затрагивается только часть 
- формально не больше K/N (K-ключи, N-сервера)

Данные могут быть распределены неравномерно.

#### Consistent hashing - Virtual Nodes

- помогает разделить данные более равномерно 
- количество физических серверов не меняется

### Шардирование и производительность запросов

- запросы по ключу шардирования вероятно ускорятся 
- запросы не по ключу обойдут все шарды
- запросы по диапазону ключей могут обойти все шарды 
- aggregating, joinы - отдельная тема

### Оценка времени

Было:
- Total = Serial + Parallel
Стало:
- Total = Serial + Parallel/N + Xserial

[https://en.wikipedia.org/wiki/Amdahl%27s_law](https://en.wikipedia.org/wiki/Amdahl%27s_law)


### Базы и шардинг
- MySQL не умеет автошардинг (NDB ?) 
- Postgres умеет автошардинг (FDW) 
- Cassandra умеет автошардинг 
- MongoDB умеет автошардинг

### Postgres FDW

```postgres-psql
 CREATE TABLE temperature (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  city_id INT NOT NULL,
  timestamp TIMESTAMP NOT NULL,
  temp DECIMAL(5,2) NOT NULL
);
```

```postgres-psql
 CREATE TABLE temperature_201904 (
  id BIGSERIAL NOT NULL,
  city_id INT NOT NULL,
  timestamp TIMESTAMP NOT NULL,
  temp DECIMAL(5,2) NOT NULL
);
```

```postgres-psql
CREATE EXTENSION postgres_fdw;
GRANT USAGE ON FOREIGN DATA WRAPPER postgres_fdw to app_user;
CREATE SERVER shard02 FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (dbname 'postgres', host 'shard02', port
    '5432');
CREATE USER MAPPING for app_user SERVER shard02
    OPTIONS (user 'fdw_user', password 'secret');
```

```postgres-psql
CREATE FOREIGN TABLE temperature_201904 PARTITION OF temperature
    FOR VALUES FROM ('2019-04-01') TO ('2019-05-01')
    SERVER remoteserver01;
```

### Postgres

- [https://www.percona.com/blog/2018/08/21/foreign-data-wrappers-postgresql-postgres_fdw](https://www.percona.com/blog/2018/08/21/foreign-data-wrappers-postgresql-postgres_fdw)
- [https://www.percona.com/blog/2019/05/24/an-overview-of-sharding-in-postgresql-and-how-it-relates-to-mongodbs/](https://www.percona.com/blog/2019/05/24/an-overview-of-sharding-in-postgresql-and-how-it-relates-to-mongodbs/)


###  Доп материалы

- [https://www.youtube.com/watch?v=MhGO7BBqSBU](https://www.youtube.com/watch?v=MhGO7BBqSBU)
- [https://www.youtube.com/watch?v=xx_Lv1P_X_I](https://www.youtube.com/watch?v=xx_Lv1P_X_I)
- [https://www.youtube.com/watch?v=ihrPoHlDFkk](https://www.youtube.com/watch?v=ihrPoHlDFkk)
- [https://www.mysql.com/products/cluster/scalability.html](https://www.mysql.com/products/cluster/scalability.html)
- [https://www.postgresql.org/docs/9.5/postgres-fdw.html](https://www.postgresql.org/docs/9.5/postgres-fdw.html)
- [https://clickhouse.yandex/docs/en/operations/table_engines/distributed/](https://clickhouse.yandex/docs/en/operations/table_engines/distributed/)
- [Вертикальный шардинг](https://ruhighload.com/%d0%92%d0%b5%d1%80%d1%82%d0%b8%d0%ba%d0%b0%d0%bb%d1%8c%d0%bd%d1%8b%d0%b9+%d1%88%d0%b0%d1%80%d0%b4%d0%b8%d0%bd%d0%b3)
- [Шардинг и репликация](https://ruhighload.com/%d0%a8%d0%b0%d1%80%d0%b4%d0%b8%d0%bd%d0%b3+%d0%b8+%d1%80%d0%b5%d0%bf%d0%bb%d0%b8%d0%ba%d0%b0%d1%86%d0%b8%d1%8f)
- [Ребалансировка данных при шардинге](https://ruhighload.com/%d0%a0%d0%b5%d0%b1%d0%b0%d0%bb%d0%b0%d0%bd%d1%81%d0%b8%d1%80%d0%be%d0%b2%d0%ba%d0%b0+%d0%b4%d0%b0%d0%bd%d0%bd%d1%8b%d1%85+%d0%bf%d1%80%d0%b8+%d1%88%d0%b0%d1%80%d0%b4%d0%b8%d0%bd%d0%b3%d0%b5)
- [Горизонтальный шардинг](https://ruhighload.com/%d0%93%d0%be%d1%80%d0%b8%d0%b7%d0%be%d0%bd%d1%82%d0%b0%d0%bb%d1%8c%d0%bd%d1%8b%d0%b9+%d1%88%d0%b0%d1%80%d0%b4%d0%b8%d0%bd%d0%b3)
- [Habr: Sharding – patterns and antipatterns](https://habr.com/ru/company/oleg-bunin/blog/313366/)