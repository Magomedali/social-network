Практика 2
-----------------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> [Домашнее задание](homework.md) -> Практика 2

### Цель: Оптимизировать текущие запросы в системе

**Количество данных в момент проведения практики - 1млн пользователей**

### План:
- Проанализировать выполнение запросов в загатовке соц сети
- Создать индексы, которые ускорят выполнение запросов
- Сравнение результатов до и после

**Количество данных в таблицу около 1млн**

### Анализ запроса в БД для главной страницы сайта

База данный MySQL - работает в окружении докера, через volume.


#### Поиск по id (UUID)

Запрос:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT  * FROM user_users WHERE id = 'b632ece9-3b95-4d95-a0d2-5c001794b62f';
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "210702.80"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "rows_examined_per_scan": 986714,
      "rows_produced_per_join": 98671,
      "filtered": "10.00",
      "cost_info": {
        "read_cost": "190968.52",
        "eval_cost": "19734.28",
        "prefix_cost": "210702.80",
        "data_read_per_join": "352M"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ],
      "attached_condition": "(`network`.`user_users`.`id` = 'b632ece9-3b95-4d95-a0d2-5c001794b62f')"
    }
  }
```

Добавим HASH индекс для id
```sql
mysql> CREATE UNIQUE INDEX user_idx ON user_users(id) USING HASH;
 Query OK, 0 rows affected (13.35 sec)
 Records: 0  Duplicates: 0  Warnings: 0

mysql> show index from user_users;
+------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx |            1 | id          | A         |      986773 |     NULL | NULL   |      | BTREE      |         |               |
+------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
```

**Интересно почему создался индекс BTREE ????**

Повторим запрос:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT  * FROM user_users WHERE id = 'b6362f53-559a-4a94-9cc2-15c6dcf39c19';
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "1.00"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "const",
      "possible_keys": [
        "user_idx"
      ],
      "key": "user_idx",
      "used_key_parts": [
        "id"
      ],
      "key_length": "110",
      "ref": [
        "const"
      ],
      "rows_examined_per_scan": 1,
      "rows_produced_per_join": 1,
      "filtered": "100.00",
      "cost_info": {
        "read_cost": "0.00",
        "eval_cost": "0.20",
        "prefix_cost": "0.00",
        "data_read_per_join": "3K"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ]
    }
  }
```

Итог: до - **"query_cost": "210702.80"**; после - **"query_cost": "1.00"**


#### Поиск по email адресу

Запрос, который выполняется при авторизации пользователя на сайте
```sql
SELECT * FROM user_users WHERE email = 'rae@example.net'
```

```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE email = 'rae@example.net';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212201.60"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "rows_examined_per_scan": 986773,
      "rows_produced_per_join": 98677,
      "filtered": "10.00",
      "cost_info": {
        "read_cost": "192466.14",
        "eval_cost": "19735.46",
        "prefix_cost": "212201.60",
        "data_read_per_join": "352M"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ],
      "attached_condition": "(`network`.`user_users`.`email` = 'rae@example.net')"
    }
}
```

Добавим индекс для email

```sql
mysql> CREATE INDEX email_idx on user_users(email) USING BTREE;

 Query OK, 0 rows affected (7.11 sec)
 Records: 0  Duplicates: 0  Warnings: 0

mysql> SHOW INDEX FROM user_users;
+------------+------------+-----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name  | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+-----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx  |            1 | id          | A         |      986773 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | email_idx |            1 | email       | A         |      538948 |     NULL | NULL   |      | BTREE      |         |               |
+------------+------------+-----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
2 rows in set (0.03 sec)
```

Смотрим план после добавления индекса:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE email = 'web-ali@yandex.ru';
 {
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "60.00"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ref",
      "possible_keys": [
        "email_idx"
      ],
      "key": "email_idx",
      "used_key_parts": [
        "email"
      ],
      "key_length": "302",
      "ref": [
        "const"
      ],
      "rows_examined_per_scan": 50,
      "rows_produced_per_join": 50,
      "filtered": "100.00",
      "cost_info": {
        "read_cost": "50.00",
        "eval_cost": "10.00",
        "prefix_cost": "60.00",
        "data_read_per_join": "182K"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ]
    }
  }
} |
```

Итог: до - **"query_cost": "212201.60"**; после - **"query_cost": "60.00"**


#### Префиксный поиск по имени и фамилий

План до добавления индекса
```sql
mysql>  EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'ga%' OR last_name LIKE 'ga%';
"query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212201.60"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "rows_examined_per_scan": 986773,
      "rows_produced_per_join": 109630,
      "filtered": "11.11",
      "cost_info": {
        "read_cost": "190275.50",
        "eval_cost": "21926.10",
        "prefix_cost": "212201.60",
        "data_read_per_join": "391M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'ga%') or (`network`.`user_users`.`last_name` like 'ga%'))"
    }
  }
```

Создадим составной индекс:
```sql
mysql> CREATE INDEX name_lname_idx ON user_users(name,last_name) USING BTREE;
 Query OK, 0 rows affected (6.87 sec)
 Records: 0  Duplicates: 0  Warnings: 0

mysql> SHOW INDEX FROM user_users;
+------------+------------+----------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name       | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+----------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx       |            1 | id          | A         |      986773 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | email_idx      |            1 | email       | A         |      538948 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | name_lname_idx |            1 | name        | A         |        3007 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | name_lname_idx |            2 | last_name   | A         |      714072 |     NULL | NULL   |      | BTREE      |         |               |
+------------+------------+----------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
4 rows in set (0.03 sec)
```

План после добавления индекса
```sql
mysql>  EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'ga%' OR last_name LIKE 'ga%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212201.60"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "possible_keys": [
        "name_lname_idx"
      ],
      "rows_examined_per_scan": 986773,
      "rows_produced_per_join": 207081,
      "filtered": "20.99",
      "cost_info": {
        "read_cost": "170785.40",
        "eval_cost": "41416.20",
        "prefix_cost": "212201.60",
        "data_read_per_join": "739M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'gaadasdasd%') or (`network`.`user_users`.`last_name` like 'gaad%'))"
    }
  }
} |
```

Видим что база не пошла по индексу и просканировала всю таблицу.
Попробуем поискать только по name;

```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'ga%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "21681.41"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "range",
      "possible_keys": [
        "name_lname_idx"
      ],
      "key": "name_lname_idx",
      "used_key_parts": [
        "name"
      ],
      "key_length": "302",
      "rows_examined_per_scan": 15486,
      "rows_produced_per_join": 15486,
      "filtered": "100.00",
      "index_condition": "(`network`.`user_users`.`name` like 'ga%')",
      "cost_info": {
        "read_cost": "18584.21",
        "eval_cost": "3097.20",
        "prefix_cost": "21681.41",
        "data_read_per_join": "55M"
      },
      "used_columns": [...]
    }
  }
} |
```

База пошла по индексу, query_cost немаленький. Попробуем увеличить префикс.

```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "1328.21"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "range",
      "possible_keys": [
        "name_lname_idx"
      ],
      "key": "name_lname_idx",
      "used_key_parts": [
        "name"
      ],
      "key_length": "302",
      "rows_examined_per_scan": 948,
      "rows_produced_per_join": 948,
      "filtered": "100.00",
      "index_condition": "(`network`.`user_users`.`name` like 'Danie%')",
      "cost_info": {
        "read_cost": "1138.61",
        "eval_cost": "189.60",
        "prefix_cost": "1328.21",
        "data_read_per_join": "3M"
      },
      "used_columns": [...]
    }
  }
} |
```

Query_cost естественно уменьшился.

Попробуем создать FULLTEXT индекс:
```sql
mysql> DROP INDEX name_lname_idx ON user_users;
 
 Query OK, 0 rows affected (0.08 sec)
 Records: 0  Duplicates: 0  Warnings: 0

mysql> CREATE FULLTEXT INDEX name_lname_fdx on user_users(name,last_name);

 Query OK, 0 rows affected, 1 warning (29.60 sec)
 Records: 0  Duplicates: 0  Warnings: 1

mysql> SHOW INDEX FROM user_users;
+------------+------------+----------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name       | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+----------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx       |            1 | id          | A         |      986202 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | email_idx      |            1 | email       | A         |      451268 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | name_lname_fdx |            1 | name        | NULL      |      986202 |     NULL | NULL   |      | FULLTEXT   |         |               |
| user_users |          1 | name_lname_fdx |            2 | last_name   | NULL      |      986202 |     NULL | NULL   |      | FULLTEXT   |         |               |
+------------+------------+----------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
4 rows in set (0.04 sec)
```

Смотрим план запроса теперь:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%';
| {
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212727.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "possible_keys": [
        "name_lname_fdx"
      ],
      "rows_examined_per_scan": 986202,
      "rows_produced_per_join": 109567,
      "filtered": "11.11",
      "cost_info": {
        "read_cost": "190813.99",
        "eval_cost": "21913.41",
        "prefix_cost": "212727.40",
        "data_read_per_join": "391M"
      },
      "used_columns": [..],
      "attached_condition": "(`network`.`user_users`.`name` like 'Danie%')"
    }
  }
}

mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%' or last_name like 'Danie%';
| {
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212727.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "possible_keys": [
        "name_lname_fdx"
      ],
      "rows_examined_per_scan": 986202,
      "rows_produced_per_join": 206961,
      "filtered": "20.99",
      "cost_info": {
        "read_cost": "171335.16",
        "eval_cost": "41392.24",
        "prefix_cost": "212727.40",
        "data_read_per_join": "738M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'Danie%') or (`network`.`user_users`.`last_name` like 'Danie%'))"
    }
  }
}
```

Показывает что есть индекс по которому можно искать, но идет по всей таблице.

Попробуем создать индексы по отдельности для name и last_name

```sql
mysql> DROP INDEX name_lname_fdx ON user_users;
Query OK, 0 rows affected (0.10 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> CREATE INDEX name_idx ON user_users(name);
Query OK, 0 rows affected (6.60 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> CREATE INDEX last_name_idx ON user_users(last_name);
Query OK, 0 rows affected (6.91 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> SHOW INDEX FROM user_users;
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name      | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx      |            1 | id          | A         |      986202 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | email_idx     |            1 | email       | A         |      451268 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | name_idx      |            1 | name        | A         |        2945 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | last_name_idx |            1 | last_name   | A         |         475 |     NULL | NULL   |      | BTREE      |         |               |
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
4 rows in set (0.05 sec)
```


Попробуем запросы:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%' or last_name like 'Danie%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "8396.21"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "index_merge",
      "possible_keys": [
        "name_idx",
        "last_name_idx"
      ],
      "key": "sort_union(name_idx,last_name_idx)",
      "key_length": "302,302",
      "rows_examined_per_scan": 3018,
      "rows_produced_per_join": 3018,
      "filtered": "100.00",
      "cost_info": {
        "read_cost": "7792.61",
        "eval_cost": "603.60",
        "prefix_cost": "8396.21",
        "data_read_per_join": "10M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'Danie%') or (`network`.`user_users`.`last_name` like 'Danie%'))"
    }
  }
}

mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "1328.21"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "range",
      "possible_keys": [
        "name_idx"
      ],
      "key": "name_idx",
      "used_key_parts": [
        "name"
      ],
      "key_length": "302",
      "rows_examined_per_scan": 948,
      "rows_produced_per_join": 948,
      "filtered": "100.00",
      "index_condition": "(`network`.`user_users`.`name` like 'Danie%')",
      "cost_info": {
        "read_cost": "1138.61",
        "eval_cost": "189.60",
        "prefix_cost": "1328.21",
        "data_read_per_join": "3M"
      },
      "used_columns": [...]
    }
  }
}


mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE last_name LIKE 'Danie%';

{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "2899.01"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "range",
      "possible_keys": [
        "last_name_idx"
      ],
      "key": "last_name_idx",
      "used_key_parts": [
        "last_name"
      ],
      "key_length": "302",
      "rows_examined_per_scan": 2070,
      "rows_produced_per_join": 2070,
      "filtered": "100.00",
      "index_condition": "(`network`.`user_users`.`last_name` like 'Danie%')",
      "cost_info": {
        "read_cost": "2485.01",
        "eval_cost": "414.00",
        "prefix_cost": "2899.01",
        "data_read_per_join": "7M"
      },
      "used_columns": [...]
    }
  }
}

mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name like 'ga%' or last_name LIKE 'ga%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "46435.57"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "index_merge",
      "possible_keys": [
        "name_idx",
        "last_name_idx"
      ],
      "key": "sort_union(name_idx,last_name_idx)",
      "key_length": "302,302",
      "rows_examined_per_scan": 17664,
      "rows_produced_per_join": 17664,
      "filtered": "100.00",
      "cost_info": {
        "read_cost": "42902.77",
        "eval_cost": "3532.80",
        "prefix_cost": "46435.57",
        "data_read_per_join": "63M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'ga%') or (`network`.`user_users`.`last_name` like 'ga%'))"
    }
  }
}
```
Видим что при раздельных индексах большинство запросов идут по индексу.

Попробуем с индексами типа FULLTEXT:

```sql
mysql> DROP INDEX name_idx ON user_users;
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> DROP INDEX last_name_idx ON user_users;
Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> CREATE FULLTEXT INDEX name_fdx on user_users(name);
Query OK, 0 rows affected (5.91 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> CREATE FULLTEXT INDEX last_name_fdx on user_users(last_name);
Query OK, 0 rows affected (5.97 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> SHOW INDEX FROM user_users;
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name      | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx      |            1 | id          | A         |      986202 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | email_idx     |            1 | email       | A         |      451268 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | name_fdx      |            1 | name        | NULL      |      986202 |     NULL | NULL   |      | FULLTEXT   |         |               |
| user_users |          1 | last_name_fdx |            1 | last_name   | NULL      |      986202 |     NULL | NULL   |      | FULLTEXT   |         |               |
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
4 rows in set (0.01 sec)
```


Попробуем теже запросы:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%' or last_name like 'Danie%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212727.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "possible_keys": [
        "name_fdx",
        "last_name_fdx"
      ],
      "rows_examined_per_scan": 986202,
      "rows_produced_per_join": 206961,
      "filtered": "20.99",
      "cost_info": {
        "read_cost": "171335.16",
        "eval_cost": "41392.24",
        "prefix_cost": "212727.40",
        "data_read_per_join": "738M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'Danie%') or (`network`.`user_users`.`last_name` like 'Danie%'))"
    }
  }
}

mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name LIKE 'Danie%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212727.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "possible_keys": [
        "name_fdx"
      ],
      "rows_examined_per_scan": 986202,
      "rows_produced_per_join": 109567,
      "filtered": "11.11",
      "cost_info": {
        "read_cost": "190813.99",
        "eval_cost": "21913.41",
        "prefix_cost": "212727.40",
        "data_read_per_join": "391M"
      },
      "used_columns": [...],
      "attached_condition": "(`network`.`user_users`.`name` like 'Danie%')"
    }
  }
}

mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE name like 'ga%' or last_name LIKE 'ga%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "212727.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "possible_keys": [
        "name_fdx",
        "last_name_fdx"
      ],
      "rows_examined_per_scan": 986202,
      "rows_produced_per_join": 206961,
      "filtered": "20.99",
      "cost_info": {
        "read_cost": "171335.16",
        "eval_cost": "41392.24",
        "prefix_cost": "212727.40",
        "data_read_per_join": "738M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'ga%') or (`network`.`user_users`.`last_name` like 'ga%'))"
    }
  }
} |
```

Ниодин запрос не пошел по индексам.

!!!!Процесть про FULLTEXT

Итог: Оставим отдельные индексы BTREE для name и last_name

```sql
mysql> SHOW INDEX FROM user_users;
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name      | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | user_idx      |            1 | id          | A         |      986202 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | email_idx     |            1 | email       | A         |      451268 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | last_name_fdx |            1 | last_name   | A         |         475 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | name_fdx      |            1 | name        | A         |        2945 |     NULL | NULL   |      | BTREE      |         |               |
+------------+------------+---------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
4 rows in set (0.01 sec)

| {
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "46435.57"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "index_merge",
      "possible_keys": [
        "last_name_fdx",
        "name_fdx"
      ],
      "key": "sort_union(name_fdx,last_name_fdx)",
      "key_length": "302,302",
      "rows_examined_per_scan": 17664,
      "rows_produced_per_join": 17664,
      "filtered": "100.00",
      "cost_info": {
        "read_cost": "42902.77",
        "eval_cost": "3532.80",
        "prefix_cost": "46435.57",
        "data_read_per_join": "63M"
      },
      "used_columns": [...],
      "attached_condition": "((`network`.`user_users`.`name` like 'ga%') or (`network`.`user_users`.`last_name` like 'ga%'))"
    }
  }
} |
```

ps. Итог примерно в пять раз уменшился query_cost. До - 212727.40; После - 46435.57;