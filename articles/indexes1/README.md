Урок 4 - Индексы Часть 1
--------------------------------------------------

-> [Главная](../../README.md) -> Лекция

## Содержание:

- [Домашнее задание](./homework.md)

### Цели занятия

* Понять, что такое индексы и для чего они нужны;
* Узнать, что такое "бинарное дерево поиска", "сбалансированное дерево";
* Познакомиться с B-деревом и его основными свойствами;
* Научиться создавать и просматривать индексы на примере Postgres;
* Попробовать команду "explain" для анализа запросов.



#### Что такое индекс в БД

Это дополнительная структура данных в БД, благодаря которой поиск и выборка данных можно упростить,
то есть ускорить операцию поиска и выбора. 

Индексы хранятся отдельно от самих данных,и обновляются при модификации самих данных. 
Поэтому индексы требуют дополнительную памать и время на создание и обновление.

- дополнительная структура данных
- способ хранения ключа данных
- ускоряет поиск и сортировку
- требуют память
- требуют время на обновление каждый раз при обновлении данных

Пример: книга с оглавлениями - книга это данные, оглавления индексы.
Оглавление помогает быстро найти нужную страницу

Индексы можно разделить на различные классификации: по структуре хранения, по методу хранения и тд
По стандартной классификации индексы разделяются на следующие виды:
- Уникальными и не уникальные
- простые и составные
- кластерные и некластерные


Вопрос: Много индексов хорошо или плохо

Ответ: Вопрос несовсем корректный, так как индексы необходимо создавать исходя из количества данных и специфики запроса.
Создавать индексы просто так или на все поля не стоит, так как они занимают доп память и время на обработку запроса обновления данных.
Индексы нужно создавать проанализировав данные и запросы.



### Деревья, как структура для индексов

**Бинарное дерево** - это структура в которой у каждого элемента не более двух потомков,
левый и правый потомок. Все элементы в левом поддереве меньше исходного элемента и правые больше.  

+ гарантированный порядок  (+)
+ понятный поиск (бинарный поиск) (+)
+ в вырожденном случаепридеться посетить все элементы O(n) (-)

**Небалансированное дерево** - это дерево где разница между левым и правым высота поддерева больше единицы 1.

**Cбалансированное дерево** - разница между высотами левого и правого поддерева не больше 1

+ решает проблему вырожденного случая бинарного дерева (+)
+ поиск за O(высоты дерева) (+)
+ требует дополнительных усилий на балансировку (-)

Самое сбалансированное дерево - AVL дерево, красно-черное дерево

Проблемы:
- оперативная память ограничена
- будем использовать внешний носитель HDD
- Log N обращении к диску

**BTREE дерево** - сильно ветвистое сбалансированное дерево. 

Задается какой-то коэфициент t. И задаются условие относительно t:
- в узле несколько элементов - коллекция элементов;
- корневом узле должно быть не более 2t-1 элементов;
- узел может иметь более 2 дочерних узлов
- в дочерних узлах должно быть от t-1 до 2t-1 элементов
- также бинарная логика, т.е c лева меньшие элементы справа большие элементы


- Алгоритм поиска аналогичен бинарному поиску, но дальнейщий  выбор не из 2 элементов а из нескольких;
- поиск за O(t logt(n))
- НО обращении к диску O(logt(n))

Добавление в BTree:

- нельзя добавить ключ в уже заполненный узел;
- разбиение на 2 по t-1 элементу;
- может привести к увеличение высоты;
- добавление за O(t logt(n));
- НО обращений к диску O(logt(n)).

то есть из диска читается узел в котором несколько элементов, а далее в оперативке производится линейный поиск.


Какой t задать для Btree:

- больше t => меньше высота дерева;
- зависит от размера блока на диске;
- зависит от объема оперативной памяти;
- обычно от 50 до 2000;
- t=1001 и 1млрд записей => 3 операции для любого ключа.

т.е при t=1001 мы можем 1 млрд ключей хранить в btree дереве у которого высота не больше трех (3) на диске, 
а это максимум 3 обращения на диск и последующий линейный поиск из подходящего по диапазону узла в оперативке


Базы данных PostgreSql и MySQL по умолчанию создают именно btree индекс, если не указать другой.
Так как он более универсальный и подходит под большинство случаев.

В Btree индексе можно сравнивать на строгое равно(=), больше (>) меньше (<) и тд.
Например hash индекс позволяет только сравнивать на равенство (=)

--------проверить этот вывод по t=1001---------


### Практика


Создадим таблицу:

```bash
> pgsql -U test testindex -c 'CREATE TABLE orders(id serial PRIMARY KEY, price bigint, hash text)'
```

Напишем скрипт для генерации данных в таблицу orders:

```bash

#!/bin/bash 
# gen.sh
MIN=$1 
MAX=$2 
 
for (( i=$MIN; i<$MAX; i++ )) 
do 
   echo "i= $i" 
   psql -U test -d testindex -c "INSERT INTO orders(price,hash) VALUES(generate_series(100,1000),md5(random()::text))" 
done 

```

Запускаем генерацию 10 000 000 записей в orders

```bash
> ./gen.sh 1 10000000
```

Делаем бекап
```bash
> pg_dump -U test -h localhsot -c -C --no-comments -f testindex.bak -d testindex
```

Заливка бекапа

```bash
> psql -U test testindex < testindex.bak
```

Смотрим план выполнения запроса выборки по id

```bash
testindex=# explain(verbose, format json) select * from orders where id = 50;
                QUERY PLAN                
------------------------------------------
 [                                       +
   {                                     +
     "Plan": {                           +
       "Node Type": "Index Scan",        +
       "Parallel Aware": false,          +
       "Scan Direction": "Forward",      +
       "Index Name": "orders_pkey",      +
       "Relation Name": "orders",        +
       "Schema": "public",               +
       "Alias": "orders",                +
       "Startup Cost": 0.43,             +
       "Total Cost": 8.45,               +
       "Plan Rows": 1,                   +
       "Plan Width": 45,                 +
       "Output": ["id", "price", "hash"],+
       "Index Cond": "(orders.id = 50)"  +
     }                                   +
   }                                     +
 ]
(1 row)
```

Смотрим план с выполнением

```bash
testindex=# explain(analyse,verbose, format json) select * from orders where id =50;
                QUERY PLAN                
------------------------------------------
 [                                       +
   {                                     +
     "Plan": {                           +
       "Node Type": "Index Scan",        +
       "Parallel Aware": false,          +
       "Scan Direction": "Forward",      +
       "Index Name": "orders_pkey",      +
       "Relation Name": "orders",        +
       "Schema": "public",               +
       "Alias": "orders",                +
       "Startup Cost": 0.43,             +
       "Total Cost": 8.45,               +
       "Plan Rows": 1,                   +
       "Plan Width": 45,                 +
       "Actual Startup Time": 0.031,     +
       "Actual Total Time": 0.057,       +
       "Actual Rows": 1,                 +
       "Actual Loops": 1,                +
       "Output": ["id", "price", "hash"],+
       "Index Cond": "(orders.id = 50)", +
       "Rows Removed by Index Recheck": 0+
     },                                  +
     "Planning Time": 0.209,             +
     "Triggers": [                       +
     ],                                  +
     "Execution Time": 0.137             +
   }                                     +
 ]
(1 row)
```

Смотрим план выполнения запроса выборки по диапозону охватывающий всю таблицу, поэтому движок бд должен игнорировать индекс 
и сканировать всю таблицу

```bash
testindex=# explain(verbose, format json) select * from orders where id > 50 and id < 10000000;
                           QUERY PLAN                            
-----------------------------------------------------------------
 [                                                              +
   {                                                            +
     "Plan": {                                                  +
       "Node Type": "Seq Scan",                                 +
       "Parallel Aware": false,                                 +
       "Relation Name": "orders",                               +
       "Schema": "public",                                      +
       "Alias": "orders",                                       +
       "Startup Cost": 0.00,                                    +
       "Total Cost": 299205.11,                                 +
       "Plan Rows": 9935973,                                    +
       "Plan Width": 45,                                        +
       "Output": ["id", "price", "hash"],                       +
       "Filter": "((orders.id > 50) AND (orders.id < 10000000))"+
     },                                                         +
     "JIT": {                                                   +
       "Worker Number": -1,                                     +
       "Functions": 2,                                          +
       "Options": {                                             +
         "Inlining": false,                                     +
         "Optimization": false,                                 +
         "Expressions": true,                                   +
         "Deforming": true                                      +
       }                                                        +
     }                                                          +
   }                                                            +
 ]
(1 row)
```

Explain analyse

```bash

testindex=# explain(analyse,verbose, format json) select * from orders where id > 50 and id < 10000000;
                            QUERY PLAN                            
------------------------------------------------------------------
 [                                                               +
   {                                                             +
     "Plan": {                                                   +
       "Node Type": "Seq Scan",                                  +
       "Parallel Aware": false,                                  +
       "Relation Name": "orders",                                +
       "Schema": "public",                                       +
       "Alias": "orders",                                        +
       "Startup Cost": 0.00,                                     +
       "Total Cost": 299205.11,                                  +
       "Plan Rows": 9935973,                                     +
       "Plan Width": 45,                                         +
       "Actual Startup Time": 57.338,                            +
       "Actual Total Time": 83698.853,                           +
       "Actual Rows": 9999949,                                   +
       "Actual Loops": 1,                                        +
       "Output": ["id", "price", "hash"],                        +
       "Filter": "((orders.id > 50) AND (orders.id < 10000000))",+
       "Rows Removed by Filter": 1822072                         +
     },                                                          +
     "Planning Time": 0.273,                                     +
     "Triggers": [                                               +
     ],                                                          +
     "JIT": {                                                    +
       "Worker Number": -1,                                      +
       "Functions": 2,                                           +
       "Options": {                                              +
         "Inlining": false,                                      +
         "Optimization": false,                                  +
         "Expressions": true,                                    +
         "Deforming": true                                       +
       },                                                        +
       "Timing": {                                               +
         "Generation": 0.946,                                    +
         "Inlining": 0.000,                                      +
         "Optimization": 11.862,                                 +
         "Emission": 44.272,                                     +
         "Total": 57.081                                         +
       }                                                         +
     },                                                          +
     "Execution Time": 161553.090                                +
   }                                                             +
 ]
(1 row)
```


Анализ плана выбора по price

```bash
testindex=# explain(analyse, verbose, format json) select * from orders where price = 50;
                  QUERY PLAN                   
-----------------------------------------------
 [                                            +
   {                                          +
     "Plan": {                                +
       "Node Type": "Gather",                 +
       "Parallel Aware": false,               +
       "Startup Cost": 1000.00,               +
       "Total Cost": 185755.56,               +
       "Plan Rows": 13063,                    +
       "Plan Width": 45,                      +
       "Actual Startup Time": 1023.497,       +
       "Actual Total Time": 1076.179,         +
       "Actual Rows": 0,                      +
       "Actual Loops": 1,                     +
       "Output": ["id", "price", "hash"],     +
       "Workers Planned": 2,                  +
       "Workers Launched": 2,                 +
       "JIT": {                               +
         "Worker Number": 0,                  +
         "Functions": 2,                      +
         "Options": {                         +
           "Inlining": false,                 +
           "Optimization": false,             +
           "Expressions": true,               +
           "Deforming": true                  +
         },                                   +
         "Timing": {                          +
           "Generation": 6.579,               +
           "Inlining": 0.000,                 +
           "Optimization": 0.265,             +
           "Emission": 2.988,                 +
           "Total": 9.832                     +
         }                                    +
       },                                     +
       "JIT": {                               +
         "Worker Number": 1,                  +
         "Functions": 2,                      +
         "Options": {                         +
           "Inlining": false,                 +
           "Optimization": false,             +
           "Expressions": true,               +
           "Deforming": true                  +
         },                                   +
         "Timing": {                          +
           "Generation": 0.309,               +
           "Inlining": 0.000,                 +
           "Optimization": 0.333,             +
           "Emission": 3.240,                 +
           "Total": 3.882                     +
         }                                    +
       },                                     +
       "Single Copy": false,                  +
       "Plans": [                             +
         {                                    +
           "Node Type": "Seq Scan",           +
           "Parent Relationship": "Outer",    +
           "Parallel Aware": true,            +
           "Relation Name": "orders",         +
           "Schema": "public",                +
           "Alias": "orders",                 +
           "Startup Cost": 0.00,              +
           "Total Cost": 183449.26,           +
           "Plan Rows": 5443,                 +
           "Plan Width": 45,                  +
           "Actual Startup Time": 964.652,    +
           "Actual Total Time": 964.659,      +
           "Actual Rows": 0,                  +
           "Actual Loops": 3,                 +
           "Output": ["id", "price", "hash"], +
           "Filter": "(orders.price = 50)",   +
           "Rows Removed by Filter": 3940674, +
           "Workers": [                       +
             {                                +
               "Worker Number": 0,            +
               "Actual Startup Time": 912.886,+
               "Actual Total Time": 912.893,  +
               "Actual Rows": 0,              +
               "Actual Loops": 1              +
             },                               +
             {                                +
               "Worker Number": 1,            +
               "Actual Startup Time": 958.958,+
               "Actual Total Time": 958.965,  +
               "Actual Rows": 0,              +
               "Actual Loops": 1              +
             }                                +
           ]                                  +
         }                                    +
       ]                                      +
     },                                       +
     "Planning Time": 0.160,                  +
     "Triggers": [                            +
     ],                                       +
     "JIT": {                                 +
       "Worker Number": -1,                   +
       "Functions": 6,                        +
       "Options": {                           +
         "Inlining": false,                   +
         "Optimization": false,               +
         "Expressions": true,                 +
         "Deforming": true                    +
       },                                     +
       "Timing": {                            +
         "Generation": 7.120,                 +
         "Inlining": 0.000,                   +
         "Optimization": 1.190,               +
         "Emission": 17.686,                  +
         "Total": 25.995                      +
       }                                      +
     },                                       +
     "Execution Time": 1076.555               +
   }                                          +
 ]
(1 row)
```

Добавим индекс для price
```sql
> create index orders_price on orders using btree(price);
```

Пробуем теперь

```bash
testindex=# explain(analyse, verbose, format json) select * from orders where price = 50;
                  QUERY PLAN                   
-----------------------------------------------
 [                                            +
   {                                          +
     "Plan": {                                +
       "Node Type": "Bitmap Heap Scan",       +
       "Parallel Aware": false,               +
       "Relation Name": "orders",             +
       "Schema": "public",                    +
       "Alias": "orders",                     +
       "Startup Cost": 245.67,                +
       "Total Cost": 38140.71,                +
       "Plan Rows": 13063,                    +
       "Plan Width": 45,                      +
       "Actual Startup Time": 0.096,          +
       "Actual Total Time": 0.118,            +
       "Actual Rows": 0,                      +
       "Actual Loops": 1,                     +
       "Output": ["id", "price", "hash"],     +
       "Recheck Cond": "(orders.price = 50)", +
       "Rows Removed by Index Recheck": 0,    +
       "Exact Heap Blocks": 0,                +
       "Lossy Heap Blocks": 0,                +
       "Plans": [                             +
         {                                    +
           "Node Type": "Bitmap Index Scan",  +
           "Parent Relationship": "Outer",    +
           "Parallel Aware": false,           +
           "Index Name": "orders_price",      +
           "Startup Cost": 0.00,              +
           "Total Cost": 242.41,              +
           "Plan Rows": 13063,                +
           "Plan Width": 0,                   +
           "Actual Startup Time": 0.049,      +
           "Actual Total Time": 0.070,        +
           "Actual Rows": 0,                  +
           "Actual Loops": 1,                 +
           "Index Cond": "(orders.price = 50)"+
         }                                    +
       ]                                      +
     },                                       +
     "Planning Time": 0.187,                  +
     "Triggers": [                            +
     ],                                       +
     "Execution Time": 0.291                  +
   }                                          +
 ]
(1 row)
```


Пример когда бд движок игнорирует индекс и сканирует полностью таблицу

```bash
testindex=# explain(analyse, verbose, format json) select * from orders where price > 20 and price < 300;
                            QUERY PLAN                             
-------------------------------------------------------------------
 [                                                                +
   {                                                              +
     "Plan": {                                                    +
       "Node Type": "Seq Scan",                                   +
       "Parallel Aware": false,                                   +
       "Relation Name": "orders",                                 +
       "Schema": "public",                                        +
       "Alias": "orders",                                         +
       "Startup Cost": 0.00,                                      +
       "Total Cost": 299207.32,                                   +
       "Plan Rows": 2624038,                                      +
       "Plan Width": 45,                                          +
       "Actual Startup Time": 123.375,                            +
       "Actual Total Time": 30746.131,                            +
       "Actual Rows": 2624200,                                    +
       "Actual Loops": 1,                                         +
       "Output": ["id", "price", "hash"],                         +
       "Filter": "((orders.price > 20) AND (orders.price < 300))",+
       "Rows Removed by Filter": 9197821                          +
     },                                                           +
     "Planning Time": 0.437,                                      +
     "Triggers": [                                                +
     ],                                                           +
     "JIT": {                                                     +
       "Worker Number": -1,                                       +
       "Functions": 2,                                            +
       "Options": {                                               +
         "Inlining": false,                                       +
         "Optimization": false,                                   +
         "Expressions": true,                                     +
         "Deforming": true                                        +
       },                                                         +
       "Timing": {                                                +
         "Generation": 1.056,                                     +
         "Inlining": 0.000,                                       +
         "Optimization": 11.303,                                  +
         "Emission": 90.687,                                      +
         "Total": 103.046                                         +
       }                                                          +
     },                                                           +
     "Execution Time": 53813.853                                  +
   }                                                              +
 ]
(1 row)
```

Можем настроить принудительное использование индекса если он есть
```bash
> set enable_seqscan=oof;
```

Результат

```bash
testindex=# explain(analyse, verbose, format json) select * from orders where price > 19 and price < 300;
                                QUERY PLAN                                
--------------------------------------------------------------------------
 [                                                                       +
   {                                                                     +
     "Plan": {                                                           +
       "Node Type": "Bitmap Heap Scan",                                  +
       "Parallel Aware": false,                                          +
       "Relation Name": "orders",                                        +
       "Schema": "public",                                               +
       "Alias": "orders",                                                +
       "Startup Cost": 55720.82,                                         +
       "Total Cost": 317833.42,                                          +
       "Plan Rows": 2624038,                                             +
       "Plan Width": 45,                                                 +
       "Actual Startup Time": 446.164,                                   +
       "Actual Total Time": 21330.550,                                   +
       "Actual Rows": 2624200,                                           +
       "Actual Loops": 1,                                                +
       "Output": ["id", "price", "hash"],                                +
       "Recheck Cond": "((orders.price > 19) AND (orders.price < 300))", +
       "Rows Removed by Index Recheck": 0,                               +
       "Exact Heap Blocks": 40038,                                       +
       "Lossy Heap Blocks": 0,                                           +
       "Plans": [                                                        +
         {                                                               +
           "Node Type": "Bitmap Index Scan",                             +
           "Parent Relationship": "Outer",                               +
           "Parallel Aware": false,                                      +
           "Index Name": "orders_price",                                 +
           "Startup Cost": 0.00,                                         +
           "Total Cost": 55064.82,                                       +
           "Plan Rows": 2624038,                                         +
           "Plan Width": 0,                                              +
           "Actual Startup Time": 438.207,                               +
           "Actual Total Time": 438.214,                                 +
           "Actual Rows": 2624200,                                       +
           "Actual Loops": 1,                                            +
           "Index Cond": "((orders.price > 19) AND (orders.price < 300))"+
         }                                                               +
       ]                                                                 +
     },                                                                  +
     "Planning Time": 0.829,                                             +
     "Triggers": [                                                       +
     ],                                                                  +
     "JIT": {                                                            +
       "Worker Number": -1,                                              +
       "Functions": 2,                                                   +
       "Options": {                                                      +
         "Inlining": false,                                              +
         "Optimization": false,                                          +
         "Expressions": true,                                            +
         "Deforming": true                                               +
       },                                                                +
       "Timing": {                                                       +
         "Generation": 1.836,                                            +
         "Inlining": 0.000,                                              +
         "Optimization": 0.000,                                          +
         "Emission": 0.000,                                              +
         "Total": 1.836                                                  +
       }                                                                 +
     },                                                                  +
     "Execution Time": 41446.915                                         +
   }                                                                     +
 ]
```


Можем посмотреть статистику использования индекса:
```sql
> select * from pg_stat_user_indexes;
 relid | indexrelid | schemaname | relname | indexrelname | idx_scan | idx_tup_read | idx_tup_fetch 
-------+------------+------------+---------+--------------+----------+--------------+---------------
 16385 |      16394 | public     | orders  | orders_pkey  |       24 |        10920 |         10920
 16385 |      16397 | public     | orders  | orders_price |       10 |      6560504 |             4
```


Посмотрим на btree
Устанавливаем встроенное расширение PostgreSql
```bash
> create extension pageinspect;
```

Смотрим мета информацию индекса по price:

```sql
> select * from bt_metap('orders_price');
 magic  | version | root | level | fastroot | fastlevel | oldest_xact | last_cleanup_num_tuples 
--------+---------+------+-------+----------+-----------+-------------+-------------------------
 340322 |       4 |  209 |     2 |      209 |         2 |           0 |                      -1
(1 row)
```

level - это высота btree дерева
root - корневой элемент

Посмотрим статистику корневого узла:

```sql
> select * from bt_page_stats('orders_price',209);
 blkno | type | live_items | dead_items | avg_item_size | page_size | free_size | btpo_prev | btpo_next | btpo | btpo_flags 
-------+------+------------+------------+---------------+-----------+-----------+-----------+-----------+------+------------
   209 | r    |        160 |          0 |            23 |      8192 |      3684 |         0 |         0 |    2 |          2
(1 row)
```

page_size - 8192byte 8KB
live_items - 160 элементов на этом узле

```sql
select * from bt_page_stats('orders_price',210);
 blkno | type | live_items | dead_items | avg_item_size | page_size | free_size | btpo_prev | btpo_next | btpo | btpo_flags 
-------+------+------------+------------+---------------+-----------+-----------+-----------+-----------+------+------------
   210 | l    |        367 |          0 |            16 |      8192 |       800 |       207 |       211 |    0 |          1
(1 row)
```


page_size - 8192byte 8KB
free_size - 800byte - осталось 800 байт
live_items - 367 элементов на этом узле


Смотрим данные на узле(только 10 выбираем):
```sql
> select * from bt_page_items('orders_price',209) limit 10;
 itemoffset |    ctid     | itemlen | nulls | vars |                      data                       
------------+-------------+---------+-------+------+-------------------------------------------------
          1 | (3,0)       |       8 | f     | f    | 
          2 | (208,4097)  |      24 | f     | f    | 69 00 00 00 00 00 00 00 00 00 01 00 61 3b 09 00
          3 | (413,4097)  |      24 | f     | f    | 6f 00 00 00 00 00 00 00 00 00 00 00 b6 9a 5e 00
          4 | (617,4097)  |      24 | f     | f    | 74 00 00 00 00 00 00 00 00 00 01 00 21 d6 21 00
          5 | (821,4097)  |      24 | f     | f    | 7a 00 00 00 00 00 00 00 00 00 01 00 77 35 15 00
          6 | (1025,4097) |      24 | f     | f    | 80 00 00 00 00 00 00 00 00 00 00 00 cd 94 09 00
          7 | (1229,4097) |      24 | f     | f    | 85 00 00 00 00 00 00 00 00 00 01 00 37 d0 2d 00
          8 | (1433,4097) |      24 | f     | f    | 8b 00 00 00 00 00 00 00 00 00 01 00 8d 2f 21 00
          9 | (1637,4097) |      24 | f     | f    | 91 00 00 00 00 00 00 00 00 00 00 00 e3 8e 15 00
         10 | (1841,4097) |      24 | f     | f    | 96 00 00 00 00 00 00 00 00 00 01 00 4d ca 39 00
(10 rows)
```

#### Hash индекс

Создадим hash индекс для поля hash
```sql
> create index orders_hash_idx on orders using hash(hash);
```
Напомним hash индекс позволяет использовать только равеносто(=), если сравнивать < или >, то движок БД не будет использовать индекс, 
а просканирует всю таблицу.


Посмотрим на анализ запроса с условием равенста:
```sql
testindex=# explain(analyse, verbose, format json) select * from orders where hash = 'adhakldalksdasd';
                           QUERY PLAN                           
----------------------------------------------------------------
 [                                                             +
   {                                                           +
     "Plan": {                                                 +
       "Node Type": "Index Scan",                              +
       "Parallel Aware": false,                                +
       "Scan Direction": "NoMovement",                         +
       "Index Name": "orders_hash_idx",                        +
       "Relation Name": "orders",                              +
       "Schema": "public",                                     +
       "Alias": "orders",                                      +
       "Startup Cost": 0.00,                                   +
       "Total Cost": 8.02,                                     +
       "Plan Rows": 1,                                         +
       "Plan Width": 45,                                       +
       "Actual Startup Time": 0.119,                           +
       "Actual Total Time": 0.135,                             +
       "Actual Rows": 0,                                       +
       "Actual Loops": 1,                                      +
       "Output": ["id", "price", "hash"],                      +
       "Index Cond": "(orders.hash = 'adhakldalksdasd'::text)",+
       "Rows Removed by Index Recheck": 0                      +
     },                                                        +
     "Planning Time": 3.180,                                   +
     "Triggers": [                                             +
     ],                                                        +
     "Execution Time": 0.862                                   +
   }                                                           +
 ]
(1 row)
```

Посмотри на запрос с условием < 
```sql
                         QUERY PLAN                         
------------------------------------------------------------
 [                                                         +
   {                                                       +
     "Plan": {                                             +
       "Node Type": "Seq Scan",                            +
       "Parallel Aware": false,                            +
       "Relation Name": "orders",                          +
       "Schema": "public",                                 +
       "Alias": "orders",                                  +
       "Startup Cost": 10000000000.00,                     +
       "Total Cost": 10000269652.26,                       +
       "Plan Rows": 7933205,                               +
       "Plan Width": 45,                                   +
       "Actual Startup Time": 89.456,                      +
       "Actual Total Time": 84759.030,                     +
       "Actual Rows": 8033257,                             +
       "Actual Loops": 1,                                  +
       "Output": ["id", "price", "hash"],                  +
       "Filter": "(orders.hash < 'adhakldalksdasd'::text)",+
       "Rows Removed by Filter": 3788764                   +
     },                                                    +
     "Planning Time": 21.529,                              +
     "Triggers": [                                         +
     ],                                                    +
     "JIT": {                                              +
       "Worker Number": -1,                                +
       "Functions": 2,                                     +
       "Options": {                                        +
         "Inlining": true,                                 +
         "Optimization": true,                             +
         "Expressions": true,                              +
         "Deforming": true                                 +
       },                                                  +
       "Timing": {                                         +
         "Generation": 2.331,                              +
         "Inlining": 28.691,                               +
         "Optimization": 20.638,                           +
         "Emission": 32.130,                               +
         "Total": 83.790                                   +
       }                                                   +
     },                                                    +
     "Execution Time": 155718.082                          +
   }                                                       +
 ]
(1 row)
```

Мета информация по hash индексу
```sql
> select * from hash_page_stats(get_raw_page('orders_hash_idx',1));
 live_items | dead_items | page_size | free_size | hasho_prevblkno | hasho_nextblkno | hasho_bucket | hasho_flag | hasho_page_id 
------------+------------+-----------+-----------+-----------------+-----------------+--------------+------------+---------------
        174 |          0 |      8192 |      4668 |           40959 |      4294967295 |            0 |          2 |         65408
(1 row)
```

Содержимое индекса hash
```sql
> select * from hash_page_items(get_raw_page('orders_hash_idx',1)) limit 10;
 itemoffset |    ctid     |   data    
------------+-------------+-----------
          1 | (64275,33)  |   5898240
          2 | (119416,22) |  12320768
          3 | (53268,28)  |  54853632
          4 | (56012,23)  |  82182144
          5 | (80888,41)  |  83034112
          6 | (56923,10)  |  89063424
          7 | (8103,73)   | 169082880
          8 | (76533,92)  | 174260224
          9 | (62127,64)  | 186056704
         10 | (65531,20)  | 191496192
(10 rows)
```

Удалим индекс hash и создадим индекс btree для hash
```sql
> drop index orders_hash_idx;
> create index orders_hash_idx on orders using btree(hash);
```

Проверим оператор LIKE:
```sql
testindex=# explain(analyse, verbose, format json) select * from orders where hash LIKE 'qwerty';
                      QUERY PLAN                       
-------------------------------------------------------
 [                                                    +
   {                                                  +
     "Plan": {                                        +
       "Node Type": "Index Scan",                     +
       "Parallel Aware": false,                       +
       "Scan Direction": "Forward",                   +
       "Index Name": "orders_hash_idx",               +
       "Relation Name": "orders",                     +
       "Schema": "public",                            +
       "Alias": "orders",                             +
       "Startup Cost": 0.56,                          +
       "Total Cost": 8.58,                            +
       "Plan Rows": 1,                                +
       "Plan Width": 45,                              +
       "Actual Startup Time": 0.101,                  +
       "Actual Total Time": 0.113,                    +
       "Actual Rows": 0,                              +
       "Actual Loops": 1,                             +
       "Output": ["id", "price", "hash"],             +
       "Index Cond": "(orders.hash = 'qwerty'::text)",+
       "Rows Removed by Index Recheck": 0,            +
       "Filter": "(orders.hash ~~ 'qwerty'::text)",   +
       "Rows Removed by Filter": 0                    +
     },                                               +
     "Planning Time": 1.210,                          +
     "Triggers": [                                    +
     ],                                               +
     "Execution Time": 0.186                          +
   }                                                  +
 ]
(1 row)
```

Проверим оператор LIKE '%qwert':

```sql
testindex=# explain(analyse, verbose, format json) select * from orders where hash LIKE '%qwerty';
                     QUERY PLAN                      
-----------------------------------------------------
 [                                                  +
   {                                                +
     "Plan": {                                      +
       "Node Type": "Seq Scan",                     +
       "Parallel Aware": false,                     +
       "Relation Name": "orders",                   +
       "Schema": "public",                          +
       "Alias": "orders",                           +
       "Startup Cost": 10000000000.00,              +
       "Total Cost": 10000269652.26,                +
       "Plan Rows": 1182,                           +
       "Plan Width": 45,                            +
       "Actual Startup Time": 7258.679,             +
       "Actual Total Time": 7258.698,               +
       "Actual Rows": 0,                            +
       "Actual Loops": 1,                           +
       "Output": ["id", "price", "hash"],           +
       "Filter": "(orders.hash ~~ '%qwerty'::text)",+
       "Rows Removed by Filter": 11822021           +
     },                                             +
     "Planning Time": 0.210,                        +
     "Triggers": [                                  +
     ],                                             +
     "JIT": {                                       +
       "Worker Number": -1,                         +
       "Functions": 2,                              +
       "Options": {                                 +
         "Inlining": true,                          +
         "Optimization": true,                      +
         "Expressions": true,                       +
         "Deforming": true                          +
       },                                           +
       "Timing": {                                  +
         "Generation": 2.226,                       +
         "Inlining": 56.311,                        +
         "Optimization": 18.126,                    +
         "Emission": 9.969,                         +
         "Total": 86.632                            +
       }                                            +
     },                                             +
     "Execution Time": 7261.428                     +
   }                                                +
 ]
(1 row)
```

Создадим опять второй hash индекс на hash:
```sql
> create index orders_hash_idx on orders using hash(hash);
```

Посомтрим какой индекс будет использовать движок БД для запроса
select * from orders where hash LIKE 'qwertyasdasdnaksd':

```sql
testindex=# explain(analyse, verbose, format json) select * from orders where hash LIKE 'qwertyasdasdnaksd';
                            QUERY PLAN                            
------------------------------------------------------------------
 [                                                               +
   {                                                             +
     "Plan": {                                                   +
       "Node Type": "Index Scan",                                +
       "Parallel Aware": false,                                  +
       "Scan Direction": "Forward",                              +
       "Index Name": "orders_hash_idx",                          +
       "Relation Name": "orders",                                +
       "Schema": "public",                                       +
       "Alias": "orders",                                        +
       "Startup Cost": 0.56,                                     +
       "Total Cost": 8.58,                                       +
       "Plan Rows": 1,                                           +
       "Plan Width": 45,                                         +
       "Actual Startup Time": 4.517,                             +
       "Actual Total Time": 4.622,                               +
       "Actual Rows": 0,                                         +
       "Actual Loops": 1,                                        +
       "Output": ["id", "price", "hash"],                        +
       "Index Cond": "(orders.hash = 'qwertyasdasdnaksd'::text)",+
       "Rows Removed by Index Recheck": 0,                       +
       "Filter": "(orders.hash ~~ 'qwertyasdasdnaksd'::text)",   +
       "Rows Removed by Filter": 0                               +
     },                                                          +
     "Planning Time": 6.943,                                     +
     "Triggers": [                                               +
     ],                                                          +
     "Execution Time": 4.685                                     +
   }                                                             +
 ]
(1 row)
```

А при равно - select * from orders where hash = 'qwertyasdasdnaksd'

```sql
testindex=# explain(analyse, verbose, format json) select * from orders where hash = 'qwertyasdasdnaksd';
                            QUERY PLAN                            
------------------------------------------------------------------
 [                                                               +
   {                                                             +
     "Plan": {                                                   +
       "Node Type": "Index Scan",                                +
       "Parallel Aware": false,                                  +
       "Scan Direction": "NoMovement",                           +
       "Index Name": "orders_hash_idx2",                         +
       "Relation Name": "orders",                                +
       "Schema": "public",                                       +
       "Alias": "orders",                                        +
       "Startup Cost": 0.00,                                     +
       "Total Cost": 8.02,                                       +
       "Plan Rows": 1,                                           +
       "Plan Width": 45,                                         +
       "Actual Startup Time": 0.019,                             +
       "Actual Total Time": 0.029,                               +
       "Actual Rows": 0,                                         +
       "Actual Loops": 1,                                        +
       "Output": ["id", "price", "hash"],                        +
       "Index Cond": "(orders.hash = 'qwertyasdasdnaksd'::text)",+
       "Rows Removed by Index Recheck": 0                        +
     },                                                          +
     "Planning Time": 0.141,                                     +
     "Triggers": [                                               +
     ],                                                          +
     "Execution Time": 0.201                                     +
   }                                                             +
 ]
(1 row)
```


Подробнее на [https://www.postgresql.org/docs/9.6/pageinspect.html](https://www.postgresql.org/docs/9.6/pageinspect.html)


Дополнительно:
- [https://www.youtube.com/watch?v=KFcpDTpoixo&feature=youtu.be](https://www.youtube.com/watch?v=KFcpDTpoixo&feature=youtu.be)
- [https://habr.com/ru/company/postgrespro/blog/326096/](https://habr.com/ru/company/postgrespro/blog/326096/)
- [https://www.geeksforgeeks.org/introduction-of-b-tree/](https://www.geeksforgeeks.org/introduction-of-b-tree/)