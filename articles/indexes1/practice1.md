Практика 1
-----------------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> [Домашнее задание](homework.md) -> Практика 1

### План:
- Проанализировать выполнение запросов в загатовке соц сети
- Создать индексы, которые ускорят выполнение запросов
- Сравнение результатов до и после

**Количество данных в таблицу около 1200**

### Анализ запроса в БД для главной страницы сайта

База данный MySQL - работает в окружении докера, через volume.

Запрос, который выполняется при авторизации пользователя на сайте
```sql
SELECT * FROM user_users WHERE email like 'web-ali@yandex.ru'
```

```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE email like 'web-ali@yandex.ru';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "321.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "rows_examined_per_scan": 1122,
      "rows_produced_per_join": 112,
      "filtered": "10.00",
      "cost_info": {
        "read_cost": "298.96",
        "eval_cost": "22.44",
        "prefix_cost": "321.40",
        "data_read_per_join": "410K"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ],
      "attached_condition": "(`network`.`user_users`.`email` = 'web-ali@yandex.ru')"
    }
  }
}
```

Добавим индекс для email

```sql
mysql> CREATE INDEX user_users_email_idx ON user_users(email) USING BTREE;

mysql> SHOW INDEX FROM user_users;
+------------+------------+----------------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table      | Non_unique | Key_name             | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+------------+------------+----------------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user_users |          0 | PRIMARY              |            1 | id          | A         |        1122 |     NULL | NULL   |      | BTREE      |         |               |
| user_users |          1 | user_users_email_idx |            1 | email       | A         |        1122 |     NULL | NULL   |      | BTREE      |         |               |
+------------+------------+----------------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
2 rows in set (0.01 sec)
```

Смотрим план после добавления индекса:
```sql
mysql> EXPLAIN FORMAT=JSON SELECT * FROM user_users WHERE email = 'web-ali@yandex.ru';
| {
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "1.20"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ref",
      "possible_keys": [
        "user_users_email_idx"
      ],
      "key": "user_users_email_idx",
      "used_key_parts": [
        "email"
      ],
      "key_length": "302",
      "ref": [
        "const"
      ],
      "rows_examined_per_scan": 1,
      "rows_produced_per_join": 1,
      "filtered": "100.00",
      "cost_info": {
        "read_cost": "1.00",
        "eval_cost": "0.20",
        "prefix_cost": "1.20",
        "data_read_per_join": "3K"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ]
    }
  }
}
```

Запрос поиска по имени пользователя:
```sql
mysql>  select * from user_users where name like 'asd%';
```
План до добавления индекса
```sql
mysql>  EXPLAIN FORMAT=JSON select * from user_users where name like 'asd%';
 {
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "321.40"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "ALL",
      "rows_examined_per_scan": 1122,
      "rows_produced_per_join": 124,
      "filtered": "11.11",
      "cost_info": {
        "read_cost": "296.47",
        "eval_cost": "24.93",
        "prefix_cost": "321.40",
        "data_read_per_join": "455K"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ],
      "attached_condition": "(`network`.`user_users`.`name` like 'asd%')"
    }
  }
}
```

План после добавления индекса
```sql
mysql>  EXPLAIN FORMAT=JSON select * from user_users where name like 'asd%';
{
  "query_block": {
    "select_id": 1,
    "cost_info": {
      "query_cost": "2.41"
    },
    "table": {
      "table_name": "user_users",
      "access_type": "range",
      "possible_keys": [
        "user_users_name_idx"
      ],
      "key": "user_users_name_idx",
      "used_key_parts": [
        "name"
      ],
      "key_length": "302",
      "rows_examined_per_scan": 1,
      "rows_produced_per_join": 1,
      "filtered": "100.00",
      "index_condition": "(`network`.`user_users`.`name` like 'asd%')",
      "cost_info": {
        "read_cost": "2.21",
        "eval_cost": "0.20",
        "prefix_cost": "2.41",
        "data_read_per_join": "3K"
      },
      "used_columns": [
        "id",
        "created_at",
        "birth_at",
        "name",
        "last_name",
        "email",
        "gender",
        "city",
        "interests",
        "password_hash",
        "confirm_token",
        "role",
        "status",
        "reset_token_token",
        "reset_token_expires"
      ]
    }
  }
}
```