Домашнее задание
------------------------

-> [Главная](../../README.md) -> [Лекция](README.md) -> Домашнее задание


## Задание

Реализовать обновление ленты новостей по протоколу WebSocket. 
Разработать компонент, куда будет подключаться клиент при открытии страницы ленты. 
Сервис должен слушать очередь обновлений ленты. 
При получении подписанным клиентом сообщения, отправлять его в браузер по WebSocket. 
Учесть возможность масштабирования сервиса. 
То есть сообщение должно доставляться только на тот экземпляр компонента, куда соединен клиент. 
Для этого можно использовать, например Routing Key из Rabbitmq.
