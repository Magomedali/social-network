pull-dev:
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml pull

up-dev:
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up -d --build

down-dev:
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml down

init-dev: down-dev pull-dev up-dev

pull-prod:
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml pull

up-prod:
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml up -d --build

down-prod:
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml down --remove-orphans

init-prod: down-prod pull-prod up-prod

pull-mon:
	docker-compose -f docker-compose.mon.yaml pull

down-mon:
	docker-compose -f docker-compose.mon.yaml down

up-mon:
	docker-compose -f docker-compose.mon.yaml up -d --build

install-grafana-dashboards:
	./docker/monitoring/grafana/setup.sh

init-mon: down-mon up-mon

front-install:
	docker run --rm --volume ${PWD}/app:/app -w /app node:latest npm install

front-build-dev:
	docker run --rm --volume ${PWD}/app:/app -w /app node:latest npm run dev

front-build-prod:
	docker run --rm --volume ${PWD}/app:/app -w /app node:latest npm build