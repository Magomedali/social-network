Архитектура высоких нагрузок
------------------------------------

Проект разрабатывается в рамках курса "Архитектура высоких нагрузок"

1. [Лекция 1 - Введение](articles/introduction/README.md)
2. [Лекция 2 - Проблемы высоких нагрузок](articles/hl_problems/README.md)
3. [Лекция 3 - Нагрузочное тестирование](articles/load_testing/README.md)
4. [Лекция 4 - Индексы 1](articles/indexes1/README.md)
5. [Лекция 5 - Индексы 2](articles/indexes2/README.md)
6. [Лекция 6 - Репликация 1](articles/replication1/README.md)
7. [Лекция 7 - Репликация 2](articles/replication2/README.md)
8. [Лекция 8 - Репликация 3](articles/replication3/README.md)
9. [Лекция 9 - Шардинг 1](articles/sharding1/README.md)
10. [Лекция 10 - Шардинг 2](articles/sharding2/README.md)
11. [Лекция 11 - Кеширование](articles/caching/README.md)
12. [Лекция 12 - ACID Транзакции](articles/transaction_ACID/README.md)
13. [Лекция 13 - Асинхронное выполнение](articles/async_handling/README.md)
14. [Лекция 14 - In memory storage](articles/InMemoryDB/README.md)
15. [Лекция 15 - OLTP и OLAP](articles/OLTP_OLAP/README.md)
16. [Лекция 16 - OLTP и OLAP (clickhouse)](articles/OLTAP_clickhouse/README.md)
17. [Лекция 17 - HTTP часть 1](articles/http_1/README.md)
18. [Лекция 18 - HTTP часть 2](articles/http_2/README.md)
19. [Лекция 19 - Микросервисы(часть 1)](articles/microservice1/README.md)
20. [Лекция 20 - Микросервисы(часть 2)](articles/microservice1/README.md)
21. [Лекция 21 - Использование асинхронной обработки](articles/use_async_handling/README.md)
22. [Лекция 22 - Балансировка и отказоустойчивость 1](articles/balancing1/README.md)
23. [Лекция 23 - Балансировка и отказоустойчивость 2](articles/balancing1/README.md)
24. [Лекция 24 - Распределенные транзакции](articles/spread_transactions/README.md)
25. [Лекция 25 - Инфраструктура микросервисов](/articles/infrastructure_microservices/README.md)
26. [Лекция 26 - Системы конфигурации](articles/config_systems/README.md)
27. [Лекция 27 - Мониторинг и алертинг](articles/monitoring_alerting/README.md)
28. [Лекция 28 - Новостной портал](articles/news_portal/README.md)

Доп. [Kubernetes](articles/kubernetes/README.md) 

###След ДЗ:
- Поднять реплику mysql<->tarantool для любой таблицы из любой подсистемы
####Цель:
- Поднять реплику mysql<->tarantool для любой таблицы из любой подсистемы.
- Переписать 1 SELECT-запрос на тарантул. Провести нагрузочное тестирование.
