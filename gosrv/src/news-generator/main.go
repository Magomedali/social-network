package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"strconv"
	"github.com/joho/godotenv"
	"github.com/satori/go.uuid"
	"syreclabs.com/go/faker"
	_ "github.com/go-sql-driver/mysql"
	"news-generator/db"
)

var (
	dbController db.DBController
)

type Member struct {
	Id string
	Name string
	LastName string
	Email string
}

type Members struct {
	Members []Member
}

func (members *Members) addMember(member Member) {
	members.Members = append(members.Members, member)
}

func main() {
	configure()

	dbController, _ = db.NewDbController()
	port, _ := strconv.Atoi(os.Getenv("NODE_1_DB_PORT"))
	shard1, err := db.New(os.Getenv("NODE_1_DB_HOST"), port, os.Getenv("NODE_1_DB_NAME"), os.Getenv("NODE_1_DB_USER"), os.Getenv("NODE_1_DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}
	defer shard1.Connection.Close()
	dbController.AddShard(shard1)

	port, _ = strconv.Atoi(os.Getenv("NODE_2_DB_PORT"))
	shard2, err := db.New(os.Getenv("NODE_2_DB_HOST"), port, os.Getenv("NODE_2_DB_NAME"), os.Getenv("NODE_2_DB_USER"), os.Getenv("NODE_2_DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}
	defer shard2.Connection.Close()
	dbController.AddShard(shard2)

	followers := findFollowers()
	for _, follower := range followers.Members {
    	generateNews(follower)
    }
}

func generateNews(follower Member) {
	records1 := []string{}
	records2 := []string{}

	for i:=0; i<1000; i++ {
		u := uuid.NewV4().String()
		record := fmt.Sprintf("('%s','%s','%s','%s')", u, follower.Id, follower.Id, strings.Join(faker.Lorem().Words(10)[:], ", "))
		num := dbController.GetShardNumber(follower.Id)
		if num == 1 {
			records1 = append(records1, record)
		} else if num == 2 {
			records2 = append(records2, record)
		}
	}
	query := "INSERT INTO network_posts(id, target_user_id, author_id, content) VALUES "
	recordsRaw1 := strings.Join(records1[:], ",")
	recordsRaw2 := strings.Join(records2[:], ",")
	query1 := query + recordsRaw1
	query2 := query + recordsRaw2

	if len(records1) > 0 {
		shard1 := dbController.Pool[0]
		_, err := shard1.Connection.Exec(query1)
		if err != nil {
			log.Fatal(err)
		}
	}
	
	if len(records2) > 0 {
		shard2 := dbController.Pool[1]
		_, err := shard2.Connection.Exec(query2)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func findFollowers() Members {

	members := Members{}
	for _, shard := range dbController.Pool {
		rows, err := shard.Connection.Query("SELECT DISTINCT id, name, last_name, email FROM user_users as m INNER JOIN network_member_followers ON member_id = m.id")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		
		for rows.Next() {
			member := Member{}
			err = rows.Scan(&member.Id, &member.Name, &member.LastName, &member.Email)
			if err != nil {
				log.Fatal(err)
			}
			members.addMember(member)
		}
	}
	return members
}

func configure() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}