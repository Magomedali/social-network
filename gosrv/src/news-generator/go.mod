module news-generator

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/satori/go.uuid v1.2.0
	syreclabs.com/go/faker v1.2.2
)
