package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"strconv"
	"math/rand"
	"github.com/joho/godotenv"
	_ "github.com/go-sql-driver/mysql"
	"followers-generator/db"
)

type Member struct {
	Id string
	Name string
	LastName string
	Email string
}

type Members struct {
	members []Member
}

func (members *Members) addMember(member Member) []Member {
	members.members = append(members.members, member)
	return members.members
}

var (
	dbController db.DBController
)

func main() {
	configure()

	dbController, _ = db.NewDbController()
	port, _ := strconv.Atoi(os.Getenv("NODE_1_DB_PORT"))
	shard1, err := db.New(os.Getenv("NODE_1_DB_HOST"), port, os.Getenv("NODE_1_DB_NAME"), os.Getenv("NODE_1_DB_USER"), os.Getenv("NODE_1_DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}
	defer shard1.Connection.Close()
	dbController.AddShard(shard1)

	port, _ = strconv.Atoi(os.Getenv("NODE_2_DB_PORT"))
	shard2, err := db.New(os.Getenv("NODE_2_DB_HOST"), port, os.Getenv("NODE_2_DB_NAME"), os.Getenv("NODE_2_DB_USER"), os.Getenv("NODE_2_DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}
	defer shard2.Connection.Close()
	dbController.AddShard(shard2)

	email := os.Getenv("TARGET_MEMBER_EMAIL")
	
	id, err := getTargetMember(email)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\n Target mamber id: " + id)

	members := findRandomMembers()

	followForMembers(id, members)
}


func followForMembers(follower_id string, members []Member) {
	
	sql := "INSERT IGNORE INTO network_member_followers(member_id, follower_id) VALUES "
	sqls1 := []string{}
	sqls2 := []string{}
	for _, member := range members {
		sqlV := fmt.Sprintf("('%s', '%s')", member.Id, follower_id)
		num := dbController.GetShardNumber(member.Id)
		if num == 1 {
			sqls1 = append(sqls1, sqlV)
		} else if num == 2 {
			sqls2 = append(sqls2, sqlV)
		}
	}

	var rows, rows2 int64
	if len(sqls1) > 0 {
		sql1 := sql + strings.Join(sqls1[:], ",")
		shard1 := dbController.Pool[0]
		result, err := shard1.Connection.Exec(sql1)
		if err != nil {
			log.Fatal(err)
		}
		rows, err = result.RowsAffected()
		if err != nil {
			log.Fatal(err)
		}
	}
	
	
	if len(sqls2) > 0 {
		shard2 := dbController.Pool[1]
		sql2 := sql + strings.Join(sqls2[:], ",")
		result, err := shard2.Connection.Exec(sql2)
		if err != nil {
			log.Fatal(err)
		}
		
		rows2, err = result.RowsAffected()
		if err != nil {
			log.Fatal(err)
		}
	}

	fmt.Println("Affected rows: ", rows + rows2)
}

func findRandomMembers() []Member {
	limit, _ := strconv.Atoi(os.Getenv("LIMIT"))
	max, _ := strconv.Atoi(os.Getenv("MAX"))
	if limit > dbController.GetSize() {
		limit = int(limit / dbController.GetSize())
	}
	offset := rand.Intn(max)
	if offset > limit {
		offset -= limit
	} else {
		offset += limit
	}

	members := make([]Member, 0)
	for _, shard := range dbController.Pool {
		rows, err := shard.Connection.Query("SELECT id, name, last_name, email FROM user_users LIMIT ? OFFSET ?", limit, offset)
		if err != nil {
			continue
		}
		defer rows.Close()
		
		for rows.Next() {
			member := Member{}
			err = rows.Scan(&member.Id, &member.Name, &member.LastName, &member.Email)
			if err != nil {
				log.Fatal(err)
			}
			members = append(members, member)
		}
	}
	
	return members
}

func getTargetMember(email string) (string, error) {

	var id string
	var err error
	for _, shard := range dbController.Pool {
		sql := "SELECT id FROM user_users WHERE email = ?"
		row := shard.Connection.QueryRow(sql, email)
		
		err = row.Scan(&id)
		if id != "" {
			err = nil
			break
		}
	}
	
	return id, err
}

func configure() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func escapeSingleQuotes(str string) string {
	return strings.Replace(str, "'", "\\'", -1)
}