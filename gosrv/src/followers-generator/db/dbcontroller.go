package db

import (
	"crypto/md5"
	"encoding/hex"
)

type DBController struct {
	Pool []*DbConnection
}

func NewDbController() (DBController, error) {
	dbc := DBController{}
	return dbc, nil
}

func (dbc *DBController) GetSize() int {
	return len(dbc.Pool)
}

func (dbc *DBController) AddShard(conn *DbConnection) {
	dbc.Pool = append(dbc.Pool, conn)
}

func (dbc *DBController) Hash(key string) int {
	hasher := md5.New()
	bytes := []byte(key)
	hasher.Write(bytes)
	strmd := hex.EncodeToString(hasher.Sum(nil))
	bytes = []byte(strmd)
	number := ((int(bytes[3]) & 0xFF) << 24) | ((int(bytes[2]) & 0xFF) << 16) | ((int(bytes[1]) & 0xFF) << 8) | (int(bytes[0]) & 0xFF)

	return number
}

func (dbc *DBController) GetShardNumber(key string) int {
	hn := dbc.Hash(key)
	return (hn % dbc.GetSize()) + 1 
}

func (dbc *DBController) GetShardByKey(key string) *DbConnection {
	num := dbc.GetShardNumber(key)
	return dbc.Pool[num-1] 
}