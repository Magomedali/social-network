package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"strconv"
	"time"
	"github.com/joho/godotenv"
	"github.com/satori/go.uuid"
	"syreclabs.com/go/faker"
	_ "github.com/go-sql-driver/mysql"
	"member-generator/db"
)

var (
	batchSize = 1000
	insertIterations = 1000
)

func main() {
	configure()

	dbController, _ := db.NewDbController()
	port, _ := strconv.Atoi(os.Getenv("NODE_1_DB_PORT"))
	shard1, err := db.New(os.Getenv("NODE_1_DB_HOST"), port, os.Getenv("NODE_1_DB_NAME"), os.Getenv("NODE_1_DB_USER"), os.Getenv("NODE_1_DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}
	defer shard1.Connection.Close()
	dbController.AddShard(shard1)

	port, _ = strconv.Atoi(os.Getenv("NODE_2_DB_PORT"))
	shard2, err := db.New(os.Getenv("NODE_2_DB_HOST"), port, os.Getenv("NODE_2_DB_NAME"), os.Getenv("NODE_2_DB_USER"), os.Getenv("NODE_2_DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}
	defer shard2.Connection.Close()
	dbController.AddShard(shard2)

	log.Println("Start inserting data ...")
	for i := 0; i < insertIterations; i++ {
		records1 := []string{}
		recordsRaw1 := ""
		records2 := []string{}
		recordsRaw2 := ""

		for j := 0; j <  batchSize; j++ {
			u := uuid.NewV4().String()
			record := fmt.Sprintf("('%s','%s','%s','%s','%s','%s','%s','%s','%s','%d','%s','%s')",
				u,
				time.Now().Format("2006-01-02 15:04:05"),
				faker.Internet().SafeEmail(),
				escapeSingleQuotes(faker.Name().FirstName()),
				escapeSingleQuotes(faker.Name().LastName()),
				"$2adasd9hasdn9as8dhasdoasd89d.asdonoasd",
				"ROLE_USER",
				faker.Date().Birthday(18,45).Format("2006-01-02 15:04:05"),
				escapeSingleQuotes(faker.Address().City()),
				faker.RandomInt(0,1),
				strings.Join(faker.Lorem().Words(3)[:], ", "),
				"active",
			)
			
			num := dbController.GetShardNumber(string(u))
			
			if num == 1 {
				records1 = append(records1, record)
			} else if num == 2 {
				records2 = append(records2, record)
			}

		}

		sqlRaw := "INSERT INTO user_users (`id`,`created_at`,`email`,`name`,`last_name`,`password_hash`,`role`,`birth_at`,`city`,`gender`,`interests`,`status`) VALUES "
		recordsRaw1 = strings.Join(records1[:], ",")
		recordsRaw2 = strings.Join(records2[:], ",")
		sqlRaw1 := sqlRaw + recordsRaw1
		sqlRaw2 := sqlRaw + recordsRaw2

		/**
		 * Реализовать через гороутины 
		*/
		if len(records1) > 0 {
			_, err := dbController.Pool[0].Connection.Exec(sqlRaw1)
			if err != nil {
				log.Fatal(err)
			}
		} 
		
		if len(records2) > 0 {
			_, err := dbController.Pool[1].Connection.Exec(sqlRaw2)
			if err != nil {
				log.Fatal(err)
			}
		}
        

        log.Println("Data insertion is completed")
	}
}

func configure() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func escapeSingleQuotes(str string) string {
	return strings.Replace(str, "'", "\\'", -1)
}
