package db

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
)

type DbConnection struct{
	Host string
	Port int
	Database string
	Username string
	Password string
	Connection *sql.DB
}

func New(host string, port int, database string, username string, password string) (*DbConnection, error) {

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", username, password, host, port, database)
	connection, err := sql.Open("mysql",dsn)
    if err != nil {
       return nil, err
    }
	
	db := DbConnection{
		Host: host,
		Port: port,
		Database: database,
		Username: username,
		Password: password,
		Connection: connection,
	}

	return &db, nil
}
