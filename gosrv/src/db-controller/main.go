package main

import (
	"fmt"
	"crypto/md5"
	"encoding/hex"
)

var SIZE = 2

func main() {
	var id = "6f77e92c-4543-40f5-a1a4-e9717ddc6cb3";
	hn := Hash(id)
	fmt.Println(hn)
	fmt.Println(GetShardNumber(id))
}

func Hash(key string) int {
	hasher := md5.New()
	bytes := []byte(key)
	hasher.Write(bytes)
	strmd := hex.EncodeToString(hasher.Sum(nil))
	bytes = []byte(strmd)
	number := ((int(bytes[3]) & 0xFF) << 24) | ((int(bytes[2]) & 0xFF) << 16) | ((int(bytes[1]) & 0xFF) << 8) | (int(bytes[0]) & 0xFF)

	return number
}

func GetShardNumber(key string) int {
	hn := Hash(key)
	return (hn % SIZE) + 1 
}