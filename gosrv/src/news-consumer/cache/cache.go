package cache

import (
	"github.com/go-redis/redis"
	"os"
)

type Cache struct{
	Redis *redis.Client
	MemberNewsPrefix string
	Limit int64
}

func New() (*Cache){
	options := &redis.Options{
		Addr: os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT"),
	}

	client := redis.NewClient(options)
	return &Cache{
		Redis: client,
		MemberNewsPrefix: "network-member-news-",
		Limit: 20,
	}
}