package repository

import "news-consumer/db"
import "strings"

type PostsRepository struct{
	DBController *db.DBController
}

type Post struct{
	Id string
	Content string
	TargetUserId string
	AuthorId string
	AuthorName string
	CreatedAt string
}

func (rep *PostsRepository) GetById(id string) (Post, error) {
	var err error
	post := Post{}

	// Добавить столбец с именем автора в таблицу, чтоб не ходить по шардам и определять имя автора
	for _, shard :=range rep.DBController.Pool {
		row := shard.Connection.QueryRow(`
												SELECT np.id, np.content, np.target_user_id, np.author_id, np.created_at 
												FROM network_posts as np
												WHERE np.id = ?`, id)
		err = row.Scan(&post.Id, &post.Content, &post.TargetUserId, &post.AuthorId, &post.CreatedAt)
		if err != nil {
			continue
		}
		if post.Id != "" {
			err = nil
			break
		}
	}
	
	if post.AuthorId != "" {
		shard := rep.DBController.GetShardByKey(post.AuthorId)
		row := shard.Connection.QueryRow("SELECT name FROM user_users WHERE id = ?", post.AuthorId)
		err = row.Scan(&post.AuthorName)
	}

	return post, err
}

func (rep *PostsRepository) GetMemberNews(memberId string, limit int64, offset int64) ([]Post, error){
	var err error
	posts := []Post{}
	var followingsIds = []string{}
	followingsIds = append(followingsIds, "'"+memberId+"'")
	for _, shard :=range rep.DBController.Pool {
		rows, err := shard.Connection.Query("(SELECT member_id FROM network_member_followers WHERE follower_id = ?)", memberId)
		if err != nil {
			return posts, err
		}
		defer rows.Close()
		for rows.Next() {
			var id string
			err = rows.Scan(&id)
			if err != nil {
				continue
			}
			followingsIds = append(followingsIds, "'"+id+"'")
		}
	}

	
	

	if len(followingsIds) > 0 {
		strFids := strings.Join(followingsIds[:], ",")
		sql := `SELECT np.id, np.content, np.target_user_id, np.author_id, IFNULL(np.author_name, ""), np.created_at
			FROM network_posts np
			WHERE np.target_user_id IN (`+strFids+`)
			 	  OR (np.author_id = ? OR np.target_user_id = ?)
			ORDER BY np.created_at DESC
			LIMIT ? OFFSET ?
		`
		for _, shard :=range rep.DBController.Pool {
			rows, err := shard.Connection.Query(sql, memberId, memberId, limit, offset)
			if err != nil {
				continue
			}
			defer rows.Close()

			for rows.Next() {
				post := Post{}
				err = rows.Scan(&post.Id, &post.Content, &post.TargetUserId, &post.AuthorId, &post.AuthorName, &post.CreatedAt)
				if err != nil {
					continue
				}
				posts = append(posts, post)
			}
		}
	}
	
	return posts, err
}