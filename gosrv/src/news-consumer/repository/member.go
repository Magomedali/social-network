package repository

import "news-consumer/db"
import "strings"

type MembersRepository struct{
	DBController *db.DBController
}

type Follower struct{
	Id string
	Name string
	LastName string
	Email string
}

func (rep *MembersRepository) GetMemberFollowers(memberId string) ([]Follower, error) {
	var err error

	//Получаем id всех фолловеров из шарда мембера
	shard := rep.DBController.GetShardByKey(memberId)
	rows, err := shard.Connection.Query("SELECT follower_id FROM network_member_followers WHERE member_id = ?", memberId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	followerIds := []string{}
	for rows.Next() {
		var fId string
		err = rows.Scan(&fId)
		if err != nil {
			continue
		}
		followerIds = append(followerIds, "'"+fId+"'")
	}
	followers := []Follower{}

	//Пройтись по всем шардам и получить полную информацию фолловеров по айдишникам
	if len(followerIds) > 0 {
		for _, shard :=range rep.DBController.Pool {
			strFollowerIds := strings.Join(followerIds[:], ",")
			rows, err := shard.Connection.Query("SELECT id, name, last_name, email FROM user_users WHERE id IN ("+strFollowerIds+")")
			if err != nil {
				continue
			}
			defer rows.Close()
			for rows.Next() {
				var follower = Follower{}
				err = rows.Scan(&follower.Id, &follower.Name, &follower.LastName, &follower.Email)
				if err != nil {
					continue
				}
				followers = append(followers, follower)
			}
		}
	}
	
	return followers, err
}