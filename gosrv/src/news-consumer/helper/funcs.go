package helper

import (
	"news-consumer/logger"
	"github.com/joho/godotenv"
)

func Configure() error {
	err := godotenv.Load()
	return err
}

func FailOnError(loggerService *logger.LoggerService, err error, msg string) {
    if err != nil {
        loggerService.Logger.Fatalf("%s: %s", msg, err)
    }
}

func LogOnError(loggerService *logger.LoggerService, err error, msg string) {
    if err != nil {
        loggerService.Logger.Errorf("%s: %s", msg, err)
    }
}