package logger

import (
	log "github.com/sirupsen/logrus"
	"os"
)

type LoggerService struct{
	Logger *log.Logger
	FilePath string
	File *os.File
}

func New() *LoggerService {
	filePath := os.Getenv("FILE_PATH");
    logger := log.New()
    logger.SetFormatter(&log.JSONFormatter{
        FieldMap: log.FieldMap{
            log.FieldKeyTime:  "datetime",
            log.FieldKeyLevel: "level_name",
            log.FieldKeyMsg:   "message",
        },
    })
    f, _ := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0755)
    logger.SetOutput(f)
	

	return &LoggerService{
			Logger: logger,
			FilePath: filePath,
			File: f,
	}
}

func (loggerService *LoggerService) Fail(err error, msg string) {
    if err != nil {
        loggerService.Logger.Fatalf("%s: %s", msg, err)
    }
}

func (loggerService *LoggerService) Error(err error, msg string) {
    if err != nil {
        loggerService.Logger.Errorf("%s: %s", msg, err)
    }
}

func (loggerService *LoggerService) Info(msg string) {
    loggerService.Logger.Info(msg)
}