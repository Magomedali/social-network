package broker

import "github.com/streadway/amqp"
import "os"
import "fmt"
import "errors"

type Broker struct{
    protocol string
    user string
    password string
    host string
    port string
	vhost string
	Connection *amqp.Connection
	Channel  *amqp.Channel
}

func New() (*Broker, error) {
	protocol := os.Getenv("AMQP_PROTOCOL")
    user :=     os.Getenv("RABBIT_USER")
    password := os.Getenv("RABBIT_PASSWORD")
    host :=     os.Getenv("RABBIT_HOST")
    port :=     os.Getenv("RABBIT_PORT")
	vhost :=    os.Getenv("RABBIT_VHOST")
	
	if len(port) > 0 {
        port = ":" + port
	}
	amqpUrl := fmt.Sprintf("%s://%s:%s@%s%s/%s", protocol, user, password, host, port, vhost)
	
	conn, err := amqp.Dial(amqpUrl)
    if err != nil {
		err = errors.New("Failed to connect to RabbitMQ")
	}

    ch, err := conn.Channel()
    if err != nil {
		err = errors.New("Failed to open a channel")
	}

    err = ch.Qos(1,0,false,)
    if err != nil {
		err = errors.New("Failed to set QoS")
	}
	
	broker := Broker{
        protocol: os.Getenv("AMQP_PROTOCOL"),
        user:     os.Getenv("RABBIT_USER"),
        password: os.Getenv("RABBIT_PASSWORD"),
        host:     os.Getenv("RABBIT_HOST"),
        port:     os.Getenv("RABBIT_PORT"),
		vhost:    os.Getenv("RABBIT_VHOST"),
		Connection: conn,
		Channel: ch,
	}
	
    return &broker, err
}