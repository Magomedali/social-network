package main

import (
	"fmt"
    "github.com/streadway/amqp"
	"os"
    "encoding/json"
    "news-consumer/helper"
    app "news-consumer/service"
    "news-consumer/broker"
    "news-consumer/repository"
    "time"
    //"os/exec"
)

type MessageBody struct{
    PostId string `json:"post_id"`
    TargetMemberId string `json:"target_member_id"`
    MemberId string `json:"member_id"`
}

var (
    service *app.Service
    membersRepository repository.MembersRepository
    postsRepository repository.PostsRepository
)

func main() {
    var err error
    err = helper.Configure()
    if err != nil {
        fmt.Fprintf(os.Stderr, "error: %v\n", err)
        os.Exit(1)
    }

    service, err = app.New()
    if err != nil {
        fmt.Fprintf(os.Stderr, "error: %v\n", err)
        os.Exit(1)
    }
    defer service.DeferMe()()

    membersRepository = repository.MembersRepository{
        DBController: service.DBController,
    }

    postsRepository = repository.PostsRepository{
        DBController: service.DBController,
    }

    queue := declareQueue(service.Broker)
    
    msgs, err := consumeMessages(service.Broker.Channel, queue)
    helper.FailOnError(service.LoggerService, err, "Failed to register a consumer")
    
    for msg := range msgs {

		/**
		* 1) Учесть количество попытки обработать сообщение
		* 2) Перепубликовать сообщение исходя из специфики ошибки обработки
		*/

        service.LoggerService.Logger.Infof("Received message %s", msg.Body)
        
        err := handleMessage(string(msg.Body))
        helper.FailOnError(service.LoggerService, err, "Failed while handle message")
    
        if err != nil {
            _ = msg.Nack(false, false)
        } else {
            _ = msg.Ack(false)
            service.LoggerService.Logger.Infof("Successfully handle message")
        }
    }

}


func handleMessage(message string) (err error) {
	var body MessageBody
	messageBytes := []byte(message)
	json.Unmarshal(messageBytes, &body)

    if body.PostId != "" && body.TargetMemberId != "" {
        err = regenerateNewsForFollowers(body)
    } else if body.MemberId != "" {
        err = generateNews(body.MemberId)
    }
    
    helper.LogOnError(service.LoggerService, err, "Error while handle message")
    return err
}

func consumeMessages(ch *amqp.Channel, q amqp.Queue) (msgs <-chan amqp.Delivery, err error) {
    return ch.Consume(q.Name, "", false, false, false, false, nil,)
}

func declareQueue(broker *broker.Broker) (amqp.Queue) {
	err := broker.Channel.ExchangeDeclare(os.Getenv("EXCHANGE_NAME"), "direct", true, false, false, false, nil,)
	helper.FailOnError(service.LoggerService, err, "Failed to declare a exchange")
	
    queue, err := broker.Channel.QueueDeclare(os.Getenv("QUEUE_NAME"), true, false, false, false, nil,)
	helper.FailOnError(service.LoggerService, err, "Failed to declare a queue")

	err = broker.Channel.QueueBind(os.Getenv("QUEUE_NAME"), "", os.Getenv("EXCHANGE_NAME"), false, nil)
	helper.FailOnError(service.LoggerService, err, "Failed to bind queue to exchange")
	
    return queue
}

func regenerateNewsForFollowers(msg MessageBody) error {
    followers, err := membersRepository.GetMemberFollowers(msg.TargetMemberId)
    selfMember := repository.Follower{
        Id: msg.TargetMemberId,
        Name: "selfName",
        LastName: "self",
        Email: "self",
    }
    followers = append(followers, selfMember)
    if err != nil{
        return err
    }

    for _, follower := range followers {
        key := service.Cache.MemberNewsPrefix + follower.Id
        res, err := service.Cache.Redis.Exists(key).Result()
        if err != nil {
            helper.LogOnError(service.LoggerService, err, fmt.Sprintf("Failed to check exists key(%s) in cache", key))
        }
        if(res > 0) {
            err = appendPostToNewsForMember(msg.PostId, follower.Id)
        } else {
            err = generateNews(follower.Id)
        }
    }

	return err
}

func appendPostToNewsForMember(postId string, memberId string) error {
    //Когда лента у подписчика существует, добавляем в начало новый пост
    key := service.Cache.MemberNewsPrefix + memberId
    post, err := postsRepository.GetById(postId)
    if err != nil {
        return fmt.Errorf("Failed to get post by id(%s): %s", postId, err)
    }
    
    var jsonPost []byte
    jsonPost, err = json.Marshal(post)
    if err != nil {
        return fmt.Errorf("Error while Marshal found post: %s", err)
    }

    length, err := service.Cache.Redis.LPush(key, string(jsonPost)).Result()
    if length > service.Cache.Limit {
        service.Cache.Redis.RPop(key).Result()
    }

    return nil
}

func generateNews(memberId string) error {
    key := service.Cache.MemberNewsPrefix + memberId
    start := time.Now().UnixNano() / int64(time.Millisecond)
    posts, err := postsRepository.GetMemberNews(memberId, service.Cache.Limit ,0)
    end := time.Now().UnixNano() / int64(time.Millisecond)
    raz := end - start
    service.LoggerService.Info(fmt.Sprintf("GetMemberNews by: %v ms", raz))
    if err != nil {
        return fmt.Errorf("Failed to get news for follower(%s): %s", memberId, err)
    }
    if len(posts) == 0 {
        return nil
    }
    
    var jsonPosts []interface{}
    for _, post := range posts {
        var jsonPost []byte
        jsonPost, err = json.Marshal(post)
        if err != nil {
            return fmt.Errorf("Error while Marshal found post: %s", err)
        }
        stringPost := string(jsonPost)
        jsonPosts = append(jsonPosts, stringPost)
    }
    length, err := service.Cache.Redis.LPush(key, jsonPosts ...).Result()
    if err != nil {
        return fmt.Errorf("Failed to LPush post: %s", err)
    }
    if length == 0 {
        return fmt.Errorf("Null length after LPush post")
    }
    return nil
}