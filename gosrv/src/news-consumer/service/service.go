package service

import (
	"os"
	"strconv"
	"news-consumer/db"
	"news-consumer/logger"
	"news-consumer/broker"
	"news-consumer/cache"
)

type Service struct{
	DBController *db.DBController
	LoggerService *logger.LoggerService
	Broker *broker.Broker
	Cache *cache.Cache
}

func New() (*Service, error) {
	var err error

	dbController, _ := db.NewDbController()
	
	port, _ := strconv.Atoi(os.Getenv("NODE_1_DB_PORT"))
	shard1, err := db.New(os.Getenv("NODE_1_DB_HOST"), port, os.Getenv("NODE_1_DB_NAME"), os.Getenv("NODE_1_DB_USER"), os.Getenv("NODE_1_DB_PASS"))
	dbController.AddShard(shard1)
	
	port, _ = strconv.Atoi(os.Getenv("NODE_2_DB_PORT"))
	shard2, err := db.New(os.Getenv("NODE_2_DB_HOST"), port, os.Getenv("NODE_2_DB_NAME"), os.Getenv("NODE_2_DB_USER"), os.Getenv("NODE_2_DB_PASS"))
	dbController.AddShard(shard2)

	logger := logger.New()
	broker, err := broker.New()
	cache := cache.New()

	return &Service{
		DBController: &dbController,
		LoggerService: logger,
		Broker: broker,
		Cache: cache,
	}, err
}

func (service *Service) DeferMe() func() {
	return func() {
		for _, shard :=range service.DBController.Pool {
			shard.Connection.Close()
		}
		service.LoggerService.File.Close()
		service.Broker.Channel.Close()
		service.Broker.Connection.Close()
	}
}
