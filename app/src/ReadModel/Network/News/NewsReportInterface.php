<?php

declare(strict_types=1);

namespace App\ReadModel\Network\News;

interface NewsReportInterface
{
    public function collectFromMemberFollowings(string $member): array;
}
