<?php

declare(strict_types=1);

namespace App\ReadModel\Network\News;


use App\Tool\DBController;

class SqlNewsReport implements NewsReportInterface
{
    private $dbController;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
    }

    public function collectFromMemberFollowings(string $follower): array
    {
        $data = [];
        foreach ($this->dbController->nodes() as $connection) {
            $query = $connection->createQueryBuilder();
            $query->select('np.*, uu.name as author_name')
                ->from('network_posts', 'np')
                ->innerJoin('np', 'user_users' ,'uu', 'uu.id = np.author_id')
                ->where('np.target_user_id IN (SELECT member_id FROM network_member_followers WHERE follower_id = :follower)')
                ->orWhere('np.author_id = :follower OR np.target_user_id = :follower')
                ->setParameter(':follower', $follower, 'string');
                //->orderBy('created_at', 'ASC');

            $stmt = $query->execute();
            foreach ($stmt->fetchAll() as $row){
                $data[] = $row;
            }
        }

        return $data;
    }
}