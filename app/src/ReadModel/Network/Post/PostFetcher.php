<?php
declare(strict_types=1);

namespace App\ReadModel\Network\Post;

use App\Tool\DBController;
use Doctrine\DBAL\FetchMode;

class PostFetcher
{
    private $dbController;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
    }

    public function findByTargetUserId(string $id, int $page = 0, int $size = 50): array
    {
        $connection = $this->dbController->getShardConnection($id);
        $qb = $connection->createQueryBuilder()
            ->select(
                'np.id',
                'np.author_id',
                'au.name as author_name',
                'np.content'
            )
            ->from("network_posts",'np')
            ->innerJoin('np','user_users','au','np.author_id = au.id')
            ->where('target_user_id = :id')
            ->setParameter(':id', $id, 'string')
            ->setFirstResult($page)
            ->setMaxResults($size);

        $result = $qb->execute();
        $result->setFetchMode(FetchMode::CUSTOM_OBJECT, PostListView::class);

        return $result->fetchAll();
    }
}