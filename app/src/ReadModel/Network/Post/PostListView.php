<?php

declare(strict_types=1);

namespace App\ReadModel\Network\Post;

class PostListView
{
    public $id;
    public $author_id;
    public $author_name;
    public $content;
}