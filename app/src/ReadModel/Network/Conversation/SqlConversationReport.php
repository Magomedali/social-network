<?php

declare(strict_types=1);

namespace App\ReadModel\Network\Conversation;


use App\Tool\DBController;

class SqlConversationReport implements ConversationReportInterface
{
    private $dbController;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
    }

    public function findPersonalByMember(string $memberId)
    {
        $data = [];
        foreach ($this->dbController->nodes() as $connection) {
            $query = $connection->createQueryBuilder();
            $query->select('nc.id, nc.created_at, ncm.member_id, ncm.member_name')
                ->from('network_conversations', 'nc')
                ->innerJoin('nc', 'network_conversation_members', 'ncm', 'ncm.conversation_id = nc.id')
                ->where('ncm.member_id != :member_id')
                ->andWhere('exists (SELECT id from network_conversation_members as ncm WHERE ncm.conversation_id = nc.id AND ncm.member_id = :member_id)')
                ->setParameter(':member_id', $memberId)
                ->orderBy('nc.created_at', 'DESC');

            $stmt = $query->execute();
            foreach ($stmt->fetchAll() as $row){
                $data[] = $row;
            }
        }
        return $data;
    }
}
