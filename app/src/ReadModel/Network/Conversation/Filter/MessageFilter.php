<?php

declare(strict_types=1);

namespace App\ReadModel\Network\Conversation\Filter;

class MessageFilter
{
    public $from;
    public $to;
}