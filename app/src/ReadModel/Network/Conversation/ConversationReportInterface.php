<?php

declare(strict_types=1);

namespace App\ReadModel\Network\Conversation;

interface ConversationReportInterface
{
    public function findPersonalByMember(string $memberId);
}
