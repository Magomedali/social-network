<?php

declare(strict_types=1);

namespace App\ReadModel\Network\Message;


use App\Tool\DBController;

class SqlMessageReport implements MessageReportInterface
{
    private $dbController;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
    }

    public function messagesByConversation(string $conversationId, int $limit = 20, int $offset = 0): array
    {
        $connection = $this->dbController->getShardConnection($conversationId);
        $query = $connection->createQueryBuilder();
        $query->select('*')
                ->from('network_conversation_messages')
                ->where('conversation_id = :conversation_id')
                ->setParameter(':conversation_id', $conversationId, 'string')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->orderBy('created_at', 'ASC');

        $stmt = $query->execute();
        return $stmt->fetchAll();
    }
}