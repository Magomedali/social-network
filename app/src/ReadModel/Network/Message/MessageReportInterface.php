<?php

declare(strict_types=1);

namespace App\ReadModel\Network\Message;

use Doctrine\DBAL\Connection;

interface MessageReportInterface
{
    public function messagesByConversation(string $conversationId, int $limit = 20, int $offset = 0): array;
}
