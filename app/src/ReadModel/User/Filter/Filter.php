<?php
declare(strict_types=1);
namespace App\ReadModel\User\Filter;

class Filter
{
    public $name;

    public $offset;

    public $limit;

    public function __construct(string $name = null, int $offset = 0, int $limit = 50)
    {
        $this->name = $name ? trim(strip_tags($name)) : $name;
        $this->offset = $offset;
        $this->limit = $limit;
    }
}