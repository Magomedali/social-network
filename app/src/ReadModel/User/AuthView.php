<?php
declare(strict_types=1);
namespace App\ReadModel\User;



class AuthView
{
    public $id;
    public $name;
    public $last_name;
    public $city;
    public $gender;
    public $interests;
    public $birth_at;
    public $age;
    public $email;
    public $password_hash;
    public $role;
    public $status;
}