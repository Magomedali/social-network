<?php
declare(strict_types=1);

namespace App\ReadModel\User;

use App\ReadModel\User\Filter\Filter;
use App\Tool\DBController;
use Doctrine\DBAL\FetchMode;

class UserFetcher
{
    private $dbController;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
    }


    public function findForAuthByEmail($email): ?AuthView
    {
        $result = null;
        foreach ($this->dbController->nodes() as $connection) {
            $stmt = $connection->createQueryBuilder()
                ->select(
                    'id',
                    'name',
                    'last_name',
                    'gender',
                    'city',
                    'birth_at',
                    'interests',
                    'email',
                    'password_hash',
                    'role',
                    'status'
                )
                ->from("user_users")
                ->where("email = :email")
                ->setParameter(":email", $email)
                ->execute();


            $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, AuthView::class);
            $result = $stmt->fetch();

            if($result){
                break;
            }
        }

        return $result ?: null;
    }


    public function existsByResetToken(string $token): bool
    {
        $flag = false;
        foreach ($this->dbController->nodes() as $connection) {
            $flag = $connection->createQueryBuilder()
                    ->select("COUNT(*)")
                    ->from("user_users")
                    ->where("reset_token_token = :token")
                    ->setParameter(":token",$token)
                    ->execute()->fetchColumn(0) > 0;

            if($flag) {
                break;
            }
        }
        return $flag;
    }

    public function all(Filter $filter, int $offset = 0, int $size = 50): array
    {
        $data=[];
        foreach ($this->dbController->nodes() as $connection) {
            $qb = $connection->createQueryBuilder()
                ->select(
                    'id',
                    'name',
                    'last_name',
                    'gender',
                    'city',
                    'birth_at',
                    'interests',
                    'email',
                    'role',
                    'status'
                )
                ->from("user_users")
                ->setFirstResult($filter->offset)
                ->setMaxResults($filter->limit)
                ->orderBy('name', 'ASC');

            if ($filter->name) {
                $qb->where($qb->expr()->like('name', ":name"))
                    ->setParameter(':name', "%$filter->name%", 'string')
                    ->setParameter(':last_name', "%$filter->name%", 'string');
            }

            $result = $qb->execute();
            $result->setFetchMode(FetchMode::CUSTOM_OBJECT, AuthView::class);
            $data += $result->fetchAll();

            if(count($data) >= $filter->limit) {
                break;
            }
        }
        return $data;
    }

    public function findById($id):?AuthView
    {
        if(!$id)
        {
            return null;
        }
        $connection = $this->dbController->getShardConnection($id);
        $stmt = $connection->createQueryBuilder()
            ->select(
                'id',
                'name',
                'last_name',
                'gender',
                'city',
                'birth_at',
                'interests',
                'email',
                'role',
                'status'
            )
            ->from("user_users")
            ->where("id = :id")
            ->setParameter(":id", $id)
            ->execute();


        $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, AuthView::class);
        $result = $stmt->fetch();

        return $result ?: null;
    }
}
