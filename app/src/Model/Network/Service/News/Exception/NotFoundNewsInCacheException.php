<?php

declare(strict_types=1);

namespace App\Model\Network\Service\News\Exception;

use Exception;

class NotFoundNewsInCacheException extends Exception
{
}
