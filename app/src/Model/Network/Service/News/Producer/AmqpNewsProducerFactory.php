<?php

declare(strict_types=1);

namespace App\Model\Network\Service\News\Producer;

use Enqueue\AmqpLib\AmqpConnectionFactory;
use Interop\Amqp\AmqpQueue as InteropAmqpQueue;
use Interop\Amqp\AmqpTopic;
use Interop\Amqp\Impl\AmqpBind;

class AmqpNewsProducerFactory
{
    public function __invoke(string $host,int $port, string $user, string $pass): AmqpNewsProducer
    {
        $factory = new AmqpConnectionFactory([
            'host' => $host,
            'port' => $port,
            'vhost' => '/',
            'user' => $user,
            'pass' => $pass,
            'persisted' => false,
        ]);

        $context = $factory->createContext();
        $context->setQos(0, 1, false);

        $networkPostTopic = $context->createTopic('network-posts-exchange');
        $networkPostTopic->addFlag(InteropAmqpQueue::FLAG_DURABLE);
        $networkPostTopic->setType(AmqpTopic::TYPE_DIRECT);
        $context->declareTopic($networkPostTopic);

        $networkPostQueue = $context->createQueue('network-posts-queue');
        $networkPostQueue->addFlag(InteropAmqpQueue::FLAG_DURABLE);
        $context->declareQueue($networkPostQueue);
        $context->bind(new AmqpBind($networkPostTopic,$networkPostQueue));

        return new AmqpNewsProducer($context, $networkPostTopic);
    }
}