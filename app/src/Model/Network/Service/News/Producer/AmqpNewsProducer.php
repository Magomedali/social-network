<?php

declare(strict_types=1);

namespace App\Model\Network\Service\News\Producer;


use Enqueue\AmqpLib\AmqpContext;
use Interop\Amqp\AmqpTopic;
use Interop\Amqp\Impl\AmqpMessage;

class AmqpNewsProducer implements NewsProducerInterface
{
    /**
     * @var AmqpContext
     */
    private $context;

    /**
     * @var AmqpTopic
     */
    private $topic;

    public function __construct(AmqpContext $context, AmqpTopic $topic)
    {
        $this->context = $context;
        $this->topic = $topic;
    }

    public function produceEmptyNews(string $memberId): void
    {
        $message = new AmqpMessage(
            json_encode([
                'member_id' => $memberId,
            ])
        );
        $this->context->createProducer()->send($this->topic, $message);
    }
}