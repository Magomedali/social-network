<?php

declare(strict_types=1);

namespace App\Model\Network\Service\News\Producer;

interface NewsProducerInterface
{
    public function produceEmptyNews(string $memberId): void;
}
