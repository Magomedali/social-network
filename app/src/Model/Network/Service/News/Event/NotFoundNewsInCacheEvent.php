<?php

declare(strict_types=1);

namespace App\Model\Network\Service\News\Event;

class NotFoundNewsInCacheEvent
{
    protected $memeberId;

    public function __construct(string $memeberId)
    {
        $this->memeberId = $memeberId;
    }

    /**
     * @return string
     */
    public function getMemeberId(): string
    {
        return $this->memeberId;
    }
}
