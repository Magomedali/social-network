<?php

declare(strict_types=1);

namespace App\Model\Network\Service\News;

use App\Model\Network\Service\News\Event\NotFoundNewsInCacheEvent;
use App\Model\Network\Service\News\Exception\NotFoundNewsInCacheException;
use Predis\ClientInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class NewsService
{
    protected $keyPrefix;

    /**
     * @var ClientInterface
     */
    private $cache;

    /**
     * @var EventDispatcherInterface
     */
    private $dispather;

    public function __construct(ClientInterface $cache, EventDispatcherInterface $dispatcher)
    {
        $this->keyPrefix = "network-member-news-";
        $this->cache = $cache;
        $this->dispather = $dispatcher;
    }

    public function getNews(string $memberId): array
    {
        $key = $this->keyPrefix . $memberId;
        if (!$this->cache->exists($key)) {
            $this->dispather->dispatch(new NotFoundNewsInCacheEvent($memberId));
            throw new NotFoundNewsInCacheException('Not found news in cache for member');
        }

        $news = $this->cache->lrange($key, 0, 10);
        $normalized = [];
        foreach ($news as $post) {
            $normalized[] = json_decode($post, true);
        }
        return $normalized;
    }
}
