<?php

declare(strict_types=1);

namespace App\Model\Network\Service\Publisher\Post;

use App\Model\Network\Entity\Post\Post;

interface PostPublisherInterface
{
    public function publish(string $postId, string $memberId): void;
}
