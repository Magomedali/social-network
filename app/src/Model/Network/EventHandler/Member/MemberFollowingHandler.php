<?php

declare(strict_types=1);

namespace App\Model\Network\EventHandler\Member;

use App\Model\Network\Entity\Member\Event\MemberFollowingEvent;
use App\Model\Network\Repository\Member\MemberRepositoryInterface;

class MemberFollowingHandler
{
    /**
     * @var MemberRepositoryInterface
     */
    private $repository;

    public function __construct(MemberRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(MemberFollowingEvent $event)
    {
        $this->repository->relateMember($event->getFollowerId(), $event->getMemberTargetId());
    }
}