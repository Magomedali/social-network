<?php

declare(strict_types=1);

namespace App\Model\Network\EventHandler\Post;

use App\Model\Network\Entity\Post\Event\NewPostEvent;
use App\Model\Network\Service\Publisher\Post\PostPublisherInterface;
use Psr\Log\LoggerInterface;

class NewPostHandler
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PostPublisherInterface
     */
    private $postPublisher;

    public function __construct(PostPublisherInterface $postPublisher, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->postPublisher = $postPublisher;
    }

    public function handle(NewPostEvent $event)
    {
        $post = $event->getPost();
        $this->postPublisher->publish($post->getId(), $post->getTargetUserId());
    }
}