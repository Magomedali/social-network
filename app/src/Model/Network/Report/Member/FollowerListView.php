<?php

declare(strict_types=1);

namespace App\Model\Network\Report\Member;

class FollowerListView
{
    public $id;

    public $name;

    public $last_name;

    public $email;
}