<?php

declare(strict_types=1);

namespace App\Model\Network\Report\Member;

use App\Tool\DBController;
use Doctrine\DBAL\FetchMode;

class SqlMemberReportInterface implements MemberReportInterface
{
    protected $dbController;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
    }

    public function getViewById(string $id): FollowerListView
    {
        $data = null;
        foreach ($this->dbController->nodes() as $connection) {
            $qb = $connection->createQueryBuilder()
                ->select([
                    'id', 'name', 'last_name', 'email'
                ])
                ->from("user_users", 'm')
                ->where('m.id = :id')
                ->setParameter(':id', $id);

            $result = $qb->execute();
            $result->setFetchMode(FetchMode::CUSTOM_OBJECT, FollowerListView::class);

            if($data = $result->fetch())
            {
                break;
            }

        }

        return $data;
    }

    public function isFollowingTo(string $member, string $member_target): bool
    {
        $connection = $this->dbController->getShardConnection($member_target);
        return $connection->createQueryBuilder()
                ->select('COUNT(1)')
                ->from('network_member_followers')
                ->where('member_id = :member_id AND follower_id = :follower_id')
                ->setParameter(":member_id",$member_target, 'string')
                ->setParameter(":follower_id",$member, 'string')
                ->execute()->fetchColumn(0) > 0;
    }

    public function findFollowers(Filter\FollowersFilter $filter)
    {
        $connection = $this->dbController->getShardConnection($filter->getActorId());
        $followersIds = $connection->createQueryBuilder()->select('follower_id')
                                ->from('network_member_followers')
                                ->where('member_id = :actor_id')
                                ->setParameter('actor_id', $filter->getActorId(), 'string')
                                ->execute()->fetchAll();
        $stringIds = [];
        if(!$followersIds) {
            return [];
        } else {
            foreach ($followersIds as $row) {
                $stringIds[] = "'".$row['follower_id']."'";
            }
        }
        $data = [];
        foreach ($this->dbController->nodes() as $connection) {
            $qb = $connection->createQueryBuilder()
                ->select([
                        'id','name','last_name','email'
                    ])
                ->from("user_users")
                ->where('id IN ('.implode(', ', $stringIds).')')
                ->setFirstResult($filter->getOffset())
                ->setMaxResults($filter->getLimit());

            $result = $qb->execute();
            $result->setFetchMode(FetchMode::CUSTOM_OBJECT, FollowerListView::class);
            foreach ($result->fetchAll() as $row){
                $data[] = $row;
            }
        }

        return $data;
    }

    public function findFollowings(Filter\FollowersFilter $filter)
    {
        $membersIds= [];
        foreach ($this->dbController->nodes() as $connection) {
            $result = $connection->createQueryBuilder()
                ->select([
                    'member_id'
                ])
                ->from("network_member_followers")
                ->where('follower_id = :actor_id')
                ->setParameter('actor_id', $filter->getActorId(), 'string')
                ->execute()->fetchAll();

            foreach ($result as $row){
                $membersIds[] = "'".$row['member_id']."'";
            }
        }

        if(!$membersIds) {
            return [];
        }

        $data = [];
        foreach ($this->dbController->nodes() as $connection) {
            $qb = $connection->createQueryBuilder()
                ->select([
                    'id','name','last_name','email'
                ])
                ->from("user_users")
                ->where('id IN ('.implode(', ', $membersIds).')')
                ->setFirstResult($filter->getOffset())
                ->setMaxResults($filter->getLimit());

            $result = $qb->execute();
            $result->setFetchMode(FetchMode::CUSTOM_OBJECT, FollowerListView::class);
            foreach ($result->fetchAll() as $row){
                $data[] = $row;
            }
        }

        return $data;
    }
}