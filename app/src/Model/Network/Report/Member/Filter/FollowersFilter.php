<?php

declare(strict_types=1);

namespace App\Model\Network\Report\Member\Filter;

class FollowersFilter
{
    protected $actorId;

    protected $offset = 0;

    protected $limit = 100;

    public function __construct(string $actorId)
    {
        $this->actorId = $actorId;
    }

    /**
     * @return string
     */
    public function getActorId(): string
    {
        return $this->actorId;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
}