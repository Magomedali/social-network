<?php

declare(strict_types=1);

namespace App\Model\Network\Report\Member;

interface MemberReportInterface
{
    public function isFollowingTo(string $member, string $member_target): bool;

    public function findFollowers(Filter\FollowersFilter $filter);

    public function findFollowings(Filter\FollowersFilter $filter);
}