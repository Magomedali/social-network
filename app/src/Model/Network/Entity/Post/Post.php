<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Post;

use App\Model\AggregateRoot;
use App\Model\EventTrait;
use App\Model\Network\Entity\Member\Member;

class Post implements AggregateRoot
{
    use EventTrait;

    public $id;

    public $target_user;

    public $actor;

    public $content;

    /**
     * @var string
     */
    private $actor_name;

    public function __construct(string $id, string $target_user, Member $actor, string $content)
    {
        $this->id = $id;
        $this->target_user = $target_user;
        $this->actor = $actor->getId()->getValue();
        $this->actor_name = $actor->getName()->getValue();
        $this->content = $content;
        $this->recordEvent(new Event\NewPostEvent($this));
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getTargetUserId(): string
    {
        return $this->target_user;
    }

    public function getActorId(): string
    {
        return $this->actor;
    }

    /**
     * @return string
     */
    public function getActorName(): string
    {
        return $this->actor_name;
    }
}