<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Post\Event;

use App\Model\Network\Entity\Post\Post;

class NewPostEvent
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * NewPostEvent constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }
}