<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Member;

class Gender
{
    const MALE=0;
    const FEMALE=1;

    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}