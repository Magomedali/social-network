<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Member;


class Interests
{
    private $value;

    public function __construct($value) {

        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}