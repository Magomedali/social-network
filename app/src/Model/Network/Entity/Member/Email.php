<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Member;

/**
 * Description of Email
 *
 * @author ali
 */
class Email {
    private $value;
    
    
    public function __construct($value) {
        
        $this->value = mb_strtolower($value);
    }
    
    
    public function getValue()
    {
        return $this->value;
    }
}
