<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Member;

use App\Model\AggregateRoot;
use App\Model\EventTrait;
use DateTimeImmutable;
use DomainException;

class Member implements AggregateRoot
{
    use EventTrait;

    private $id;

    private $createdAt;

    private $birthDate;

    private $email;

    private $name;

    private $lastName;

    private $city;

    private $interests;

    private $gender;

    private $followers = [];

    public function __construct(Id $id,Email $email,Name $name,LastName $lastName,City $city,Gender $gender,BirthDate $birthDate = null,Interests $interests = null)
    {
        $this->id = $id;
        $this->createdAt = new DateTimeImmutable();
        $this->name = $name;
        $this->lastName = $lastName;
        $this->city = $city;
        $this->gender = $gender;
        $this->interests = $interests;
        $this->birthDate = $birthDate;
        $this->email = $email;
        $this->followers = [];
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getGender(): Gender
    {
        return $this->gender;
    }

    public function getInterests(): Interests
    {
        return $this->interests;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getBirthDate(): ?BirthDate
    {
        return $this->birthDate;
    }

    public function addFollower(Member $follower):void
    {
        if (array_key_exists($follower->getId()->getValue(), $this->followers)) {
            throw new DomainException('Member already following');
        }

        $this->followers[$follower->getId()->getValue()] = $follower;
        $this->recordEvent(new Event\MemberFollowingEvent($follower->getId(), $this->getId()));
    }

    public function getFollowers(): array
    {
        return $this->followers;
    }
}
