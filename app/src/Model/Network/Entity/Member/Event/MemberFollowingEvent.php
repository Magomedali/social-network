<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Member\Event;

use App\Model\Network\Entity\Member\Id;

class MemberFollowingEvent
{
    protected $followerId;

    protected $member_target_id;

    public function __construct(Id $followerId, Id $member_target_id)
    {
        $this->followerId = $followerId;
        $this->member_target_id = $member_target_id;
    }

    public function getFollowerId(): Id
    {
        return $this->followerId;
    }

    public function getMemberTargetId(): Id
    {
        return $this->member_target_id;
    }
}