<?php
declare(strict_types=1);

namespace App\Model\Network\Entity\Member;

use DateTimeImmutable;

class BirthDate
{
    private $value;

    public function __construct(DateTimeImmutable $value)
    {
        $this->value = $value;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getValue(): DateTimeImmutable
    {
        return $this->value;
    }
}