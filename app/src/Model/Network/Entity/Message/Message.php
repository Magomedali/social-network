<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Message;

use App\Model\AggregateRoot;
use App\Model\EventTrait;
use DateTimeImmutable;

class Message implements AggregateRoot
{
    use EventTrait;

    /**
     * @var Id
     */
    private $id;

    private $conversationId;

    private $message;

    private $memberFrom;


    private $createdAt;

    public function __construct(Id $id, string $conversationId, string $message, string $memberFrom)
    {
        $this->id = $id;
        $this->conversationId = $conversationId;
        $this->message = $message;
        $this->memberFrom = $memberFrom;
        $this->createdAt = new DateTimeImmutable();
        $this->recordEvent(new Event\NewMessage($this->getId()));
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getConversationId(): string
    {
        return $this->conversationId;
    }

    /**
     * @return string
     */
    public function getMemberFrom(): string
    {
        return $this->memberFrom;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
