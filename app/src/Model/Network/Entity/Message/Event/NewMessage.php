<?php

declare(strict_types=1);

namespace App\Model\Network\Entity\Message\Event;

use App\Model\Network\Entity\Message\Id;

class NewMessage
{
    protected $messageId;

    public function __construct(Id $messageId)
    {
        $this->messageId = $messageId;
    }

    /**
     * @return Id
     */
    public function getMessageId(): Id
    {
        return $this->messageId;
    }
}