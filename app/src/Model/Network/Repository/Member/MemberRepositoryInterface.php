<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Member;

use App\Model\Network\Entity\Member\Id;
use App\Model\Network\Entity\Member\Member;

interface MemberRepositoryInterface
{
    public function getById(Id $id): Member;

    public function relateMember(Id $followerId, Id $memberId): void;
}