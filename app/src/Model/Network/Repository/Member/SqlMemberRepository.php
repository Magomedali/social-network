<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Member;

use App\Tool\DBController;
use \DateTimeImmutable;
use App\Model\EntityNotFoundException;
use App\Model\Hydrator;
use App\Model\Network\Entity\Member\Id;
use App\Model\Network\Entity\Member\Name;
use App\Model\Network\Entity\Member\LastName;
use App\Model\Network\Entity\Member\Email;
use App\Model\Network\Entity\Member\Member;
use App\Model\Network\Entity\Member\Gender;
use App\Model\Network\Entity\Member\BirthDate;
use App\Model\Network\Entity\Member\City;
use App\Model\Network\Entity\Member\Interests;

class SqlMemberRepository implements MemberRepositoryInterface
{
    private $table = 'user_users';

    protected $dbController;
    protected $hydrator;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
        $this->hydrator = new Hydrator();
    }

    public function getById(Id $id): Member
    {
        $connection = $this->dbController->getShardConnection($id->getValue());
        $query = $connection->createQueryBuilder();

        $result = $query->select($this->getColumns())->from($this->table)
            ->where('id = :id')
            ->setParameter(':id',$id->getValue(),'string')
            ->setMaxResults(1)
            ->execute();

        if(!($data = $result->fetch())) {
            throw new EntityNotFoundException('Member not found!');
        }

        return $this->hydrate($data);
    }

    private function getColumns():array
    {
        return [
            'id',
            'created_at',
            'email',
            'name',
            'last_name',
            'city',
            'gender',
            'interests',
            'birth_at'
        ];
    }

    private function hydrate($data): Member
    {
        return $this->hydrator->hydrate(Member::class,[
            'id'=>new Id($data['id']),
            'createdAt'=> new DateTimeImmutable($data['created_at']),
            'email'=>new Email($data['email']),
            'name'=>new Name($data['name']),
            'lastName'=>new LastName($data['last_name']),
            'gender'=>new Gender((int)$data['gender']),
            'city'=>new City($data['city']),
            'interests'=>new Interests($data['interests']),
            'birthDate'=>$data['birth_at'] ? new BirthDate(new DateTimeImmutable($data['birth_at'])) : null,
        ]);
    }

    public function relateMember(Id $followerId, Id $memberId): void
    {
        $connection = $this->dbController->getShardConnection($memberId->getValue());
        $connection->createQueryBuilder()->insert('network_member_followers')
                    ->values(['member_id'=>':member_id','follower_id'=>':follower_id'])
                        ->setParameter(':member_id',$memberId->getValue(),'string')
                            ->setParameter(':follower_id',$followerId->getValue(),'string')
                                ->execute();
    }
}