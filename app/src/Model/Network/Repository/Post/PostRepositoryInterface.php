<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Post;

use App\Model\Network\Entity\Post\Post;

interface PostRepositoryInterface
{
    public function add(Post $post): void;

    public function save(Post $post): void;
}