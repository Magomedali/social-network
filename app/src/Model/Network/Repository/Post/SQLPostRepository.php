<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Post;

use App\Model\Hydrator;
use App\Model\Network\Entity\Post\Post;
use App\Tool\DBController;

class SQLPostRepository implements PostRepositoryInterface
{
    protected $dbController;
    protected $hydrator;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
        $this->hydrator = new Hydrator();
    }

    public static function tableName(): string
    {
        return 'network_posts';
    }

    public function save(Post $post): void
    {
        $connection = $this->dbController->getShardConnection($post->getTargetUserId());
        $query = $connection->createQueryBuilder();

        $query->update(self::tableName())
            ->set('id',':id')
            ->set('target_user_id',':target_user_id')
            ->set('author_id',':author_id')
            ->set('author_name',':author_name')
            ->set('content',':content')
            ->setParameter(':id',$post->getId(),'string')
            ->setParameter(':target_user_id',$post->getTargetUserId(),'string')
            ->setParameter(':author_id',$post->getActorId(),'string')
            ->setParameter(':author_name',$post->getActorName(),'string')
            ->setParameter(':content',$post->getContent(),'string')
            ->where('id = :id')
            ->setParameter(':id',$post->getId())
            ->execute();
    }

    public function add(Post $post): void
    {
        $connection = $this->dbController->getShardConnection($post->getTargetUserId());
        $query = $connection->createQueryBuilder();

        $query->insert(self::tableName())
            ->values([
                'id'=>':id',
                'target_user_id'=>':target_user_id',
                'author_id'=>':author_id',
                'author_name'=>':author_name',
                'content'=>':content',
            ])->setParameter(':id',$post->getId(),'string')
            ->setParameter(':target_user_id',$post->getTargetUserId(),'string')
            ->setParameter(':author_id',$post->getActorId(),'string')
            ->setParameter(':author_name',$post->getActorName(),'string')
            ->setParameter(':content',$post->getContent(),'string')
            ->execute();
    }
}