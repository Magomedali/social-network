<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Message;

use App\Model\Hydrator;
use App\Model\Network\Entity\Message\Message;
use App\Tool\DBController;


class SqlMessageRepository implements MessageRepositoryInterface
{
    protected $dbController;
    protected $hydrator;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
        $this->hydrator = new Hydrator();
    }

    public static function tableName(): string
    {
        return 'network_conversation_messages';
    }

    public function add(Message $message): void
    {
        $connection = $this->dbController->getShardConnection($message->getConversationId());
        $connection->createQueryBuilder()->insert(self::tableName())
            ->values([
                'id'=>':id',
                'conversation_id'=>':conversation_id',
                'from_member'=>':from_member',
                'message'=>':message',
                'created_at'=>':created_at',
            ])->setParameter(':id',$message->getId()->getValue(),'string')
            ->setParameter(':conversation_id',$message->getConversationId(),'string')
            ->setParameter(':from_member',$message->getMemberFrom(),'string')
            ->setParameter(':message',$message->getMessage(),'string')
            ->setParameter(':created_at',$message->getCreatedAt()->format('Y-m-d\TH:i:s'),'string')
            ->execute();
    }
}