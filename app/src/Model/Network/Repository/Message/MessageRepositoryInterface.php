<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Message;

use App\Model\Network\Entity\Message\Message;

interface MessageRepositoryInterface
{
    public function add(Message $message): void;
}