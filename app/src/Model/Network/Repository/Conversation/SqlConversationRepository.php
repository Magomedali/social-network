<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Conversation;

use App\Model\Hydrator;
use App\Model\Network\Entity\Member\Member;
use App\Tool\DBController;
use Doctrine\DBAL\Connection;

class SqlConversationRepository implements ConversationRepositoryInterface
{
    protected $dbController;
    protected $hydrator;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
        $this->hydrator = new Hydrator();
    }

    public function getPersonalConversation(string $member, string $withMember): array
    {
        $conversation = [];
        foreach ($this->dbController->nodes() as $connection) {
            $conversation = $connection->createQueryBuilder()->select('*')
                ->from('network_conversations', 'nc')
                ->andWhere('exists (SELECT ncm.id FROM network_conversation_members AS ncm WHERE ncm.conversation_id = nc.id AND ncm.member_id = :member)')
                ->andWhere('exists (SELECT ncm.id FROM network_conversation_members AS ncm WHERE ncm.conversation_id = nc.id AND ncm.member_id = :withMember)')
                ->setParameter(':member', $member, 'string')
                ->setParameter(':withMember', $withMember, 'string')
                ->execute()->fetch();

            if ($conversation) {
                break;
            }
        }

        return $conversation;
    }

    public function existsPersonalConversation(string $member, string $withMember): ?string
    {
        $id = null;
        foreach ($this->dbController->nodes() as $connection) {
            $id = $connection->createQueryBuilder()->select('id')
                ->from('network_conversations', 'nc')
                ->andWhere('exists (SELECT ncm.id FROM network_conversation_members AS ncm WHERE ncm.conversation_id = nc.id AND ncm.member_id = :member)')
                ->andWhere('exists (SELECT ncm.id FROM network_conversation_members AS ncm WHERE ncm.conversation_id = nc.id AND ncm.member_id = :withMember)')
                ->setParameter(':member', $member, 'string')
                ->setParameter(':withMember', $withMember, 'string')
                ->execute()->fetchColumn(0);

            if ($id) {
                break;
            }
        }
        return $id ? (string) $id : null;
    }

    public function createPersonal(string $id, Member $member, Member $withMember): void
    {
        $connection = $this->dbController->getShardConnection($id);
        $connection->transactional(function (Connection $connection) use($id, $member, $withMember){
            $query = $connection->createQueryBuilder();
            $query->insert('network_conversations')
                ->values([
                    'id' => ':id'
                ])
                ->setParameter(':id',$id,'string')
                ->execute();

            $query = $connection->createQueryBuilder();
            $query->insert('network_conversation_members')
                ->values([
                    'conversation_id' => ':conversation_id',
                    'member_id' => ':member_id',
                    'member_name' => ':member_name',
                ])->setParameter(':conversation_id',$id, 'string')
                    ->setParameter(':member_id',$member->getId()->getValue(), 'string')
                    ->setParameter(':member_name',$member->getName()->getValue(), 'string')
                    ->execute();

            $query = $connection->createQueryBuilder();
            $query->insert('network_conversation_members')
                ->values([
                    'conversation_id' => ':conversation_id',
                    'member_id' => ':with_member_id',
                    'member_name' => ':member_name',
                ])
                ->setParameter(':conversation_id',$id, 'string')
                ->setParameter(':with_member_id',$withMember->getId()->getValue(), 'string')
                ->setParameter(':member_name',$withMember->getName()->getValue(), 'string')
                ->execute();
        });
    }
}