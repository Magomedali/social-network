<?php

declare(strict_types=1);

namespace App\Model\Network\Repository\Conversation;

use App\Model\Network\Entity\Member\Member;

interface ConversationRepositoryInterface
{
    public function getPersonalConversation(string $member, string $withMember): array;

    public function existsPersonalConversation(string $member, string $withMember): ?string;

    public function createPersonal(string $id, Member $member, Member $withMember): void;
}