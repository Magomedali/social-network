<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Member\Follow;

use App\Model\EntityNotFoundException;
use App\Model\Network\Entity\Member\Id;
use App\Model\Network\Report\Member\MemberReportInterface;
use App\Model\Network\Repository\Member\MemberRepositoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
//use Symfony\Component\EventDispatcher\EventDispatcher;

class Handler
{
    /**
     * @var MemberRepositoryInterface
     */
    private $members;

    /**
     * @var MemberReportInterface
     */
    private $report;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(MemberRepositoryInterface $members, MemberReportInterface $report, EventDispatcherInterface $dispatcher)
    {
        $this->members = $members;
        $this->report = $report;
        $this->dispatcher = $dispatcher;
    }

    public function handle(Command $command)
    {
        if ($this->report->isFollowingTo($command->follower, $command->member_target)) {
           throw new \LogicException('Member already following');
        }

        if (!($member_target = $this->members->getById(new Id($command->member_target)))) {
            throw new EntityNotFoundException('Target member not found');
        }

        if (!($follower = $this->members->getById(new Id($command->follower)))) {
            throw new EntityNotFoundException('Target member not found');
        }

        $member_target->addFollower($follower);

        foreach ($member_target->releaseEvents() as $event) {
            $this->dispatcher->dispatch($event);
        }
    }
}