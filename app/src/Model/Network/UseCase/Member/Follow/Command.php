<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Member\Follow;

class Command
{
    public $follower;
    public $member_target;
}