<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Post\Create;

class Command
{
    public $target_user_id;

    public $actor;

    public $content;

    public function __construct(string $target_user_id, string $actor, string $content)
    {
        $this->target_user_id = $target_user_id;
        $this->actor = $actor;
        $this->content = $content;
    }
}