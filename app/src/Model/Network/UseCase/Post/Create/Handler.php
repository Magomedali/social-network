<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Post\Create;

use App\Model\Network\Entity\Member\Id;
use App\Model\Network\Entity\Post\Post;
use App\Model\Network\Repository\Member\MemberRepositoryInterface;
use App\Model\Network\Repository\Post\PostRepositoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;

class Handler
{
    private $posts;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var MemberRepositoryInterface
     */
    private $members;

    public function __construct(PostRepositoryInterface $posts, MemberRepositoryInterface $members, EventDispatcherInterface $dispatcher)
    {
        $this->posts = $posts;
        $this->members = $members;
        $this->dispatcher = $dispatcher;
    }

    public function handle(Command $command): void
    {
        $actor = $this->members->getById(new Id($command->actor));

        $post = new Post(Uuid::uuid4()->toString(), $command->target_user_id, $actor, $command->content);

        $this->posts->add($post);

        foreach ($post->releaseEvents() as $event) {
            $this->dispatcher->dispatch($event);
        }
    }
}