<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Conversation\Open;

class Command
{
    private $member;
    private $withMember;

    public function __construct(string $member, string $withMember)
    {
        $this->member = $member;
        $this->withMember = $withMember;
    }

    /**
     * @return mixed
     */
    public function getMember(): string
    {
        return $this->member;
    }

    /**
     * @return mixed
     */
    public function getWithMember(): string
    {
        return $this->withMember;
    }
}