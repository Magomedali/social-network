<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Conversation\Open;

use App\Model\Network\Entity\Member\Id;
use App\Model\Network\Report\Member\MemberReportInterface;
use App\Model\Network\Repository\Conversation\ConversationRepositoryInterface;
use App\Model\Network\Repository\Member\MemberRepositoryInterface;
use Ramsey\Uuid\Uuid;

class Handler
{

    /**
     * @var ConversationRepositoryInterface
     */
    private $conversations;

    /**
     * @var MemberRepositoryInterface
     */
    private $members;

    public function __construct(ConversationRepositoryInterface $conversations, MemberRepositoryInterface $members)
    {
        $this->conversations = $conversations;
        $this->members = $members;
    }

    public function handle(Command $command)
    {
        if($conversationId = $this->conversations->existsPersonalConversation($command->getMember(), $command->getWithMember())) {
            return $conversationId;
        }

        if (!($member = $this->members->getById(new Id($command->getMember())))) {
            throw new \DomainException('Model member not found');
        }

        if (!($withMember = $this->members->getById(new Id($command->getWithMember())))) {
            throw new \DomainException('Model withMember not found');
        }
        $this->conversations->createPersonal($conversationId = Uuid::uuid4()->toString(), $member, $withMember);

        return $conversationId;
    }
}