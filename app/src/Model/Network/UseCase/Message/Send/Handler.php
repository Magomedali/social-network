<?php

declare(strict_types=1);

namespace App\Model\Network\UseCase\Message\Send;

use App\Model\AggregateRoot;
use App\Model\Network\Entity\Member\Id;
use App\Model\Network\Entity\Message\Id as MessageID;
use App\Model\Network\Entity\Message\Message;
use App\Model\Network\Repository\Conversation\ConversationRepositoryInterface;
use App\Model\Network\Repository\Member\MemberRepositoryInterface;
use App\Model\Network\Repository\Message\MessageRepositoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class Handler
{
    /**
     * @var MemberRepositoryInterface
     */
    private $members;
    /**
     * @var MessageRepositoryInterface
     */
    private $messages;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ConversationRepositoryInterface
     */
    private $conversations;

    public function __construct(
        MemberRepositoryInterface $members,
        MessageRepositoryInterface $messages,
        ConversationRepositoryInterface $conversations,
        EventDispatcherInterface $dispatcher)
    {
        $this->members = $members;
        $this->messages = $messages;
        $this->conversations = $conversations;
        $this->dispatcher = $dispatcher;
    }

    public function handle(Command $command): void
    {
        $conversation = $this->conversations->getPersonalConversation($command->getFrom(),$command->getTo());
        $from = $this->members->getById(new Id($command->getFrom()));

        $message = new Message(MessageID::next(), $conversation['id'], $command->getMessage(), $from->getId()->getValue());
        $this->messages->add($message);

        $this->dispatchEvents($message);
    }

    private function dispatchEvents(AggregateRoot $entity)
    {
        foreach ($entity->releaseEvents() as $event) {
            $this->dispatcher->dispatch($event);
        }
    }
}