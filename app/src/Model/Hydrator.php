<?php
declare(strict_types=1);
namespace App\Model;


class Hydrator
{

    /**
     * Local cache of reflection class instances
     * @var array
     */
    private $reflectionClassMap = [];

    /**
     * Creates an instance of a class filled with data according to map
     *
     * @param array $data
     * @param string $className
     * @return object
     *
     * @throws \ReflectionException
     * @since 1.0.2
     */
    public function hydrate($className,array $data)
    {
        $reflection = $this->getReflectionClass($className);
        $object = $reflection->newInstanceWithoutConstructor();
        foreach ($data as $propertyName => $propertyValue) {
            if (!$reflection->hasProperty($propertyName)) {
                throw new \InvalidArgumentException("There's no $propertyName property in $className.");
            }

            $property = $reflection->getProperty($propertyName);
            $property->setAccessible(true);
            $property->setValue($object, $data[$propertyName]);

        }
        return $object;
    }

    /**
     * Fills an object passed with data according to map
     *
     * @param array $data
     * @param object $object
     * @return object
     *
     * @throws \ReflectionException
     * @since 1.0.2
     */
    public function hydrateInto($object,array $data)
    {
        $className = get_class($object);
        $reflection = $this->getReflectionClass($className);
        foreach ($data as $propertyName => $propertyValue) {
            if (!$reflection->hasProperty($propertyName)) {
                throw new \InvalidArgumentException("There's no $propertyName property in $className.");
            }

            $property = $reflection->getProperty($propertyName);
            $property->setAccessible(true);
            $property->setValue($object, $data[$propertyName]);

        }
        return $object;
    }

    /**
     * Extracts data from an object according to map
     *
     * @param object $object
     * @return array
     * @throws \ReflectionException
     */
    public function extract($object,array $map)
    {
        $data = [];
        $className = get_class($object);
        $reflection = $this->getReflectionClass($className);
        foreach ($map as $dataKey => $propertyName) {
            if ($reflection->hasProperty($propertyName)) {
                $property = $reflection->getProperty($propertyName);
                $property->setAccessible(true);
                $data[$dataKey] = $property->getValue($object);
            }
        }
        return $data;
    }
    /**
     * Returns instance of reflection class for class name passed
     *
     * @param string $className
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    protected function getReflectionClass($className)
    {
        if (!isset($this->reflectionClassMap[$className])) {
            $this->reflectionClassMap[$className] = new \ReflectionClass($className);
        }
        return $this->reflectionClassMap[$className];
    }
}