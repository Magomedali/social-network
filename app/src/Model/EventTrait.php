<?php

declare(strict_types=1);

namespace App\Model;

trait EventTrait
{
    protected $recordedEvents = [];

    public function recordEvent($event)
    {
        $this->recordedEvents[] = $event;
    }

    public function releaseEvents(): array
    {
        $events = $this->recordedEvents;
        $vents = $this->recordedEvents;
        return $events;
    }
}