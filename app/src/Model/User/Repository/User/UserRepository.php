<?php
declare(strict_types=1);
namespace App\Model\User\Repository\User;


use App\Model\EntityNotFoundException;
use App\Model\User\Entity\User\Email;
use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\User;


interface UserRepository
{

    public function findByConfirmToken(string $token): ?User;


    public function findByResetToken(string $token): ?User;


    public function get(Id $id): User;


    public function getByEmail(Email $email): User;



    public function hasByEmail(Email $email): bool;


    public function add(User $user): void;


    public function save(User $user): void;
}