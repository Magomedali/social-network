<?php
declare(strict_types=1);

namespace App\Model\User\Repository\User;

use App\Model\EntityNotFoundException;
use App\Model\Hydrator;
use App\Model\User\Entity\User\Gender;
use App\Model\User\Entity\User\City;
use App\Model\User\Entity\User\BirthDate;
use App\Model\User\Entity\User\Email;
use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\Interests;
use App\Model\User\Entity\User\LastName;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\ResetToken;
use App\Model\User\Entity\User\Role;
use App\Model\User\Entity\User\Token;
use App\Model\User\Entity\User\User;
use App\Tool\DBController;
use DateTimeImmutable;

class SqlUserRepository implements UserRepository
{
    protected $dbController;
    protected $hydrator;

    public function __construct(DBController $dbController)
    {
        $this->dbController = $dbController;
        $this->hydrator = new Hydrator();
    }

    public static function tableName(): string
    {
        return 'user_users';
    }

    public function findByConfirmToken(string $token): ?User
    {
        $data = null;
        foreach ($this->dbController->nodes() as $connection) {
            $query = $connection->createQueryBuilder();

            $result = $query->select($this->getColumns())->from(self::tableName())
                ->where('confirm_token = :confirm_token')
                ->setParameter(':confirm_token',$token,'string')
                ->setMaxResults(1)
                ->execute();

            if($data = $result->fetch()) {
                break;
            }
        }

        return $data ? $this->hydrate($data) : null;
    }

    public function findByResetToken(string $token): ?User
    {
        $data = null;
        foreach ($this->dbController->nodes() as $connection) {
            $query = $connection->createQueryBuilder();

            $result = $query->select($this->getColumns())->from(self::tableName())
                ->where('reset_token_token = :reset_token_token')
                ->setParameter(':reset_token_token',$token,'string')
                ->setMaxResults(1)
                ->execute();

            if($data = $result->fetch()) {
                break;
            }
        }

        return $data ? $this->hydrate($data) : null;
    }

    public function get(Id $id): User
    {
        $connection = $this->dbController->getShardConnection($id->getValue());
        $query = $connection->createQueryBuilder();

        $result = $query->select($this->getColumns())->from(self::tableName())
            ->where('id = :id')
            ->setParameter(':id',$id->getValue(),'string')
            ->setMaxResults(1)
            ->execute();

        if(!($data = $result->fetch())) {
            throw new EntityNotFoundException('User not found!');
        }

        return $this->hydrate($data);
    }

    public function getByEmail(Email $email): User
    {
        $data = null;
        foreach ($this->dbController->nodes() as $connection) {
            $query = $connection->createQueryBuilder();

            $result = $query->select($this->getColumns())->from(self::tableName())
                ->where('email = :email')
                ->setParameter(':email',$email->getValue(),'string')
                ->setMaxResults(1)
                ->execute();

            if($data = $result->fetch()) {
                break;
            }
        }

        return $data ? $this->hydrate($data) : null;
    }

    public function hasByEmail(Email $email): bool
    {
        $result = false;
        foreach ($this->dbController->nodes() as $connection) {
            $query = $connection->createQueryBuilder();

            $result = $query->select(['id'])->from(self::tableName())
                ->where('email = :email')
                ->setParameter(':email',$email->getValue(),'string')
                ->execute()->fetchColumn() > 0;

            if ($result) {
                break;
            }
        }

        return $result;
    }

    public function add(User $user): void
    {
        $connection = $this->dbController->getShardConnection($user->getId()->getValue());
        $query = $connection->createQueryBuilder();

        $query->insert(self::tableName())
            ->values([
                    'id'=>':id',
                    'created_at'=>':created_at',
                    'email'=>':email',
                    'name'=>':name',
                    'last_name'=>':last_name',
                    'password_hash'=>':password_hash',
                    'confirm_token'=>':confirm_token',
                    'role'=>':role',
                    'birth_at'=>':birth_at',
                    'city'=>':city',
                    'gender'=>':gender',
                    'interests'=>':interests',
                    'status'=>':status',
                    'reset_token_token'=>':reset_token_token',
                    'reset_token_expires'=>':reset_token_expires'
                ])->setParameter(':id',$user->getId()->getValue(),'string')
                    ->setParameter(':created_at',$user->getCreatedAt()->format('Y-m-d\TH:i:s'))
                    ->setParameter(':email',$user->getEmail()->getValue())
                    ->setParameter(':name',$user->getName()->getValue())
                    ->setParameter(':last_name',$user->getLastName()->getValue())
                    ->setParameter(':city',$user->getCity()->getValue())
                    ->setParameter(':gender',$user->getGender()->getValue())
                    ->setParameter(':interests',$user->getInterests()->getValue())
                    ->setParameter(':birth_at',$user->getBirthDate() ? $user->getBirthDate()->getValue()->format('Y-m-d\TH:i:s') : null)
                    ->setParameter(':password_hash',$user->getPasswordHash())
                    ->setParameter(':confirm_token',$user->getConfirmToken() ? $user->getConfirmToken()->getValue():null)
                    ->setParameter(':role',$user->getRole()->getName())
                    ->setParameter(':status',$user->getStatus())
                    ->setParameter(':reset_token_token',$user->getResetToken() ? $user->getResetToken()->getToken() : null)
                    ->setParameter(':reset_token_expires',$user->getResetToken() ? $user->getResetToken()->getExpires()->format('Y-m-d\TH:i:s'): null)
                ->execute();
    }

    public function save(User $user): void
    {
        $connection = $this->dbController->getShardConnection($user->getId()->getValue());
        $query = $connection->createQueryBuilder();

        $query->update(self::tableName())
            ->set('email',':email')
            ->set('password_hash',':password_hash')
            ->set('confirm_token',':confirm_token')
            ->set('role',':role')
            ->set('name',':name')
            ->set('last_name',':last_name')
            ->set('city',':city')
            ->set('gender',':gender')
            ->set('interests',':interests')
            ->set('birth_at',':birth_at')
            ->set('status',':status')
            ->set('reset_token_token',':reset_token_token')
            ->set('reset_token_expires',':reset_token_expires')
            ->setParameter(':email',$user->getEmail()->getValue())
            ->setParameter(':password_hash',$user->getPasswordHash())
            ->setParameter(':confirm_token',$user->getConfirmToken() ? $user->getConfirmToken()->getValue():null)
            ->setParameter(':role',$user->getRole()->getName())
            ->setParameter(':name',$user->getName()->getValue())
            ->setParameter(':last_name',$user->getLastName()->getValue())
            ->setParameter(':city',$user->getCity()->getValue())
            ->setParameter(':gender',$user->getGender()->getValue())
            ->setParameter(':interests',$user->getInterests()->getValue())
            ->setParameter(':birth_at',$user->getBirthDate() ? $user->getBirthDate()->getValue()->format('Y-m-d\TH:i:s') : null)
            ->setParameter(':status',$user->getStatus())
            ->setParameter(':reset_token_token',$user->getResetToken() ? $user->getResetToken()->getToken() : null)
            ->setParameter(':reset_token_expires',$user->getResetToken() ? $user->getResetToken()->getExpires()->format('Y-m-d\TH:i:s') : null)
            ->where('id = :id')
            ->setParameter(':id',$user->getId()->getValue())
            ->execute();
    }

    private function getColumns():array
    {
        return [
            'id',
            'created_at',
            'email',
            'name',
            'last_name',
            'city',
            'gender',
            'interests',
            'birth_at',
            'password_hash',
            'confirm_token',
            'role',
            'status',
            'reset_token_token',
            'reset_token_expires'
        ];
    }

    private function hydrate($data):User
    {

        return $this->hydrator->hydrate(User::class,[
            'id'=>new Id($data['id']),
            'createdAt'=> new DateTimeImmutable($data['created_at']),
            'email'=>new Email($data['email']),
            'passwordHash'=>$data['password_hash'],
            'confirmToken'=>new Token($data['confirm_token']),
            'role'=>new Role($data['role']),
            'name'=>new Name($data['name']),
            'lastName'=>new LastName($data['last_name']),
            'gender'=>new Gender((int)$data['gender']),
            'city'=>new City($data['city']),
            'interests'=>new Interests($data['interests']),
            'birthDate'=>$data['birth_at'] ? new BirthDate(new DateTimeImmutable($data['birth_at'])) : null,
            'status'=>$data['status'],
            'resetToken'=>$data['reset_token_token'] ? new ResetToken($data['reset_token_token'],new DateTimeImmutable($data['reset_token_expires'])):null
        ]);
    }
}