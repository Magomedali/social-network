<?php


namespace App\Model\User\Entity\User;


class Interests
{
    private $value;

    public function __construct($value) {

        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}