<?php


namespace App\Model\User\Entity\User;


class Name
{
    private $value;

    public function __construct($value) {

        $this->value = mb_strtolower($value);
    }

    public function getValue()
    {
        return $this->value;
    }
}