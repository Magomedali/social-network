<?php

namespace App\Model\User\Entity\User;

use DateTimeImmutable;
use Webmozart\Assert\Assert;


class ResetToken {
    

    private $token;
    


    private $expires;
    


    
    public function __construct(string $token, DateTimeImmutable $expires) {
        
        Assert::notEmpty($token);
        $this->token = $token;
        $this->expires = $expires;
    }
    
    
    public function getToken():string
    {
        return $this->token;
    }



    public function getExpires():DateTimeImmutable
    {
        return $this->expires;
    }
    
    
    
    public function isExpiredTo(DateTimeImmutable $date):bool
    {
        return $this->expires <= $date;
    }


    public function isEmpty(): bool
    {
        return empty($this->token);
    }
}
