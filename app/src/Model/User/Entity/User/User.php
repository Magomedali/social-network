<?php
declare(strict_types=1);
namespace App\Model\User\Entity\User;

use DateTimeImmutable;

class User {

    public const STATUS_NEW = "new";
    public const STATUS_WAIT = "wait";
    public const STATUS_ACTIVE = "active";

    private $id;

    private $createdAt;

    private $birthDate;

    private $email;
    
    private $name;

    private $lastName;

    private $city;

    private $interests;

    private $gender;

    private $passwordHash;

    private $confirmToken;

    private $resetToken;

    private $role;

    private $status;
    

    public function __construct(Id $id,Email $email,Name $name,LastName $lastName,City $city,Gender $gender,Token $confirmToken, string $hash,BirthDate $birthDate = null,Interests $interests = null)
    {
        $this->id = $id;
        $this->createdAt = new DateTimeImmutable();
        $this->name = $name;
        $this->lastName = $lastName;
        $this->city = $city;
        $this->gender = $gender;
        $this->interests = $interests;
        $this->birthDate = $birthDate;
        $this->email = $email;
        $this->passwordHash = $hash;
        $this->confirmToken = $confirmToken;
        $this->status = self::STATUS_WAIT;
        $this->role = Role::user();
    }

    
    
    public function getId(): Id
    {
        return $this->id;
    }
    
    
    
    public function getEmail(): Email
    {
        return $this->email;
    }
    
    
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }
    
    
    public function getResetToken(): ?ResetToken
    {
        return $this->resetToken;
    }
    
    
    public function getConfirmToken(): ?Token
    {
        return $this->confirmToken;
    }


    public function getRole(): Role
    {
        return $this->role;
    }
    

    public function getStatus(): string
    {
        return $this->status;
    }


    public function getGender(): Gender
    {
        return $this->gender;
    }


    public function getInterests(): Interests
    {
        return $this->interests;
    }


    public function getName(): Name
    {
        return $this->name;
    }


    public function getLastName(): LastName
    {
        return $this->lastName;
    }


    public function getCity(): City
    {
        return $this->city;
    }
    
    
    public function isWait():bool
    {
        return $this->status === self::STATUS_WAIT;
    }
    
    
    public function isNew():bool
    {
        return $this->status === self::STATUS_NEW;
    }
    
    
    public function isActive():bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }
    
    
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }



    public function getBirthDate(): ?BirthDate
    {
        return $this->birthDate;
    }
    


    
    
    public function confirmToken(): void
    {
        if(!$this->isWait())
            throw new \DomainException("User is already confirmed!");
        
        $this->status = self::STATUS_ACTIVE;
        $this->confirmToken = null;
    }

    
    
    
    public function requestPasswordReset(ResetToken $token, DateTimeImmutable $date):void
    {
        if(!$this->isActive())
            throw new \DomainException("User is not active");
        
        if(!$this->email)
            throw new \DomainException("Email is not specified.");
        
        if($this->resetToken && !$this->resetToken->isExpiredTo($date))
            throw new \DomainException("Resetting is already requested.");
        
        $this->resetToken = $token;
    }
    
    
    
    public function passwordReset(DateTimeImmutable $date, string $hash): void
    {
        if(!$this->resetToken)
            throw new \DomainException("Resetting is not requested.");
        
        if($this->resetToken->isExpiredTo($date))
            throw new \DomainException("Reset token is expired.");
        
        $this->passwordHash = $hash;
        $this->resetToken = null;
    }

}
