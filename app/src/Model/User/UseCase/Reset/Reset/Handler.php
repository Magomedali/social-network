<?php
declare(strict_types=1);
namespace App\Model\User\UseCase\Reset\Reset;

use App\Model\User\Repository\User\UserRepository;
use DateTimeImmutable;
use App\Model\User\Entity\User\{User,ResetToken};
use App\Model\Flusher;
use App\Model\User\Service\PasswordHasher;
/**
 * Description of Handler
 *
 * @author ali
 */
class Handler {
    
    private $users;
    
    private $hasher;
    
    public function __construct(UserRepository $users,PasswordHasher $hasher) {
        $this->users = $users;
        $this->hasher = $hasher;
    }


    public function handle(Command $command)
    {
        if(!$user = $this->users->findByResetToken($command->token))
                throw new \DomainException("Incorrect or confirmed token.");
        
        $user->passwordReset(new DateTimeImmutable(),$this->hasher->hash($command->password));
        
        $this->users->save($user);
    }
}
