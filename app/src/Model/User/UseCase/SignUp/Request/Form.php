<?php
namespace App\Model\User\UseCase\SignUp\Request;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;



class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', Type\TextType::class)
            ->add('last_name', Type\TextType::class)
            ->add('gender', Type\ChoiceType::class,['choices'=>['Male'=>0,'Female'=>1]])
            ->add('birth_at', Type\DateType::class,[
                'widget'=>'single_text',
                'format' => 'yyyy-MM-dd',
                'input'=>'string'
                //'mapped'=>false
            ])
            ->add('city', Type\TextType::class)
            ->add('interests', Type\TextareaType::class)
            ->add('email', Type\EmailType::class)
            ->add('password', Type\PasswordType::class);
    }
    


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}