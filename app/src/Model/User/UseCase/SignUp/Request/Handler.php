<?php
declare(strict_types=1);
namespace App\Model\User\UseCase\SignUp\Request;

use App\Model\User\Repository\User\UserRepository;
use App\Model\User\Entity\User\{BirthDate, City, Gender, Interests, LastName, Name, User, Email, Id, Token};
use App\Model\User\Service\{PasswordHasher,ConfirmTokenizer,ConfirmTokenSender};
use DateTimeImmutable;

/**
 * Description of Handler
 *
 * @author ali
 */
class Handler {
    
    private $users;
    
    
    private $hasher;
    
    
    private $sender;
    
    
    private $tokenizer;
    
    
    public function __construct(UserRepository $users,PasswordHasher $hasher,ConfirmTokenizer $tokenizer,ConfirmTokenSender $sender)
    {
        $this->users = $users;
        $this->hasher = $hasher;
        $this->sender = $sender;
        $this->tokenizer = $tokenizer;
    }
    
    public function handle(Command $command): void
    {
        $email = new Email($command->email);
        $token = $this->tokenizer->generate();
        
        if($this->users->hasByEmail($email))
            throw new \DomainException("User already exists!");
        
        $user = new User(
                Id::next(),
                $email,
                new Name($command->name),
                new LastName($command->last_name),
                new City($command->city),
                new Gender($command->gender),
                new Token($token),
                $this->hasher->hash($command->password),
                $command->birth_at ?new BirthDate(new DateTimeImmutable($command->birth_at)) : null,
                $command->interests ?new Interests($command->interests) : null
        );
        
        $this->users->add($user);
        
        $this->sender->send($email,$token);
    }
}
