<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200101181429 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE user_users (
                            id VARCHAR(36) NOT NULL, 
                            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            birth_at TIMESTAMP NULL DEFAULT NULL,
                            name VARCHAR(100) NOT NULL,
                            last_name VARCHAR(100) NOT NULL, 
                            email VARCHAR(100) NOT NULL,
                            gender TINYINT NOT NULL,
                            city VARCHAR(100),
                            interests TEXT DEFAULT NULL,
                            password_hash VARCHAR(255) DEFAULT NULL, 
                            confirm_token VARCHAR(255) DEFAULT NULL, 
                            role VARCHAR(16) NOT NULL, 
                            status VARCHAR(16) NOT NULL, 
                            reset_token_token VARCHAR(255) DEFAULT NULL, 
                            reset_token_expires TIMESTAMP NULL DEFAULT NULL, 
                            PRIMARY KEY (id)
        ) CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE user_users');
    }
}
