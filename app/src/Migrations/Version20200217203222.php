<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217203222 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Index for user name and last name';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE INDEX last_name_idx ON user_users(last_name) USING BTREE');
        $this->addSql('CREATE INDEX name_idx ON user_users(name) USING BTREE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX last_name_idx ON user_users;');
        $this->addSql('DROP INDEX name_idx ON user_users;');
    }
}
