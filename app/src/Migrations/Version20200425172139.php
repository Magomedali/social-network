<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425172139 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add author_name to network_posts';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE network_posts ADD COLUMN author_name varchar(255)  NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE network_posts DROP COLUMN author_name');
    }
}
