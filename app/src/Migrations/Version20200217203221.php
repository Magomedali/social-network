<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217203221 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Index for user email';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE INDEX email_idx ON user_users(email) USING BTREE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX email_idx ON user_users;');
    }
}
