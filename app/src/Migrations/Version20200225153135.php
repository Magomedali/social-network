<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200225153135 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'new table for network_posts';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE network_posts(
                id VARCHAR(36) NOT NULL,
                target_user_id varchar(36) not null,
                author_id varchar(36) not null,
                content text,
                CONSTRAINT network_post_id_idx UNIQUE INDEX (id) USING HASH,
                CONSTRAINT network_post_fk_target_user_id FOREIGN KEY (target_user_id) REFERENCES user_users(id)
        ) CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE network_posts');
    }
}
