<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217203047 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Hash index for user id';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("DROP INDEX `PRIMARY` ON user_users");
        $this->addSql('CREATE UNIQUE INDEX user_idx ON user_users(id) USING HASH');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX user_idx ON user_users;');
    }
}
