<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200407193538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add created_at column to network_posts';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE network_posts ADD COLUMN created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE network_posts DROP COLUMN created_at');
    }
}
