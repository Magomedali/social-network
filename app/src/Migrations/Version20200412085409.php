<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200412085409 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Index for network_posts.target_user_id';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE INDEX target_user_id_idx ON network_posts(target_user_id) USING HASH');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX target_user_id_idx ON network_posts;');
    }
}
