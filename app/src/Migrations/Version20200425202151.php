<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425202151 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add member to network_conversation_members';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE network_conversation_members ADD COLUMN member_name varchar(255)  NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE network_conversation_members DROP COLUMN member_name');
    }
}
