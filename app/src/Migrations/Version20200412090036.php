<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200412090036 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Index for network_member_followers.follower_id';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE INDEX nmf_idx_follower_id ON network_member_followers(follower_id) USING HASH');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX nmf_idx_follower_id ON network_member_followers;');
    }
}
