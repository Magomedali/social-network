<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200314221348 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Added table for member followers relation';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE network_member_followers(
            member_id VARCHAR(36) NOT NULL,
            follower_id VARCHAR(36) NOT NULL,
            CONSTRAINT nmf_udx_member_follower UNIQUE INDEX (member_id, follower_id),
            CONSTRAINT nmf_fk_member_follower_member_id FOREIGN KEY (member_id) REFERENCES user_users(id)
        ) CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE network_member_followers');
    }
}
