<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200314215120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'New table for member conversation';
    }

    public function up(Schema $schema) : void
    {
        $this->connection->transactional(function (){
            $this->addSql('CREATE TABLE network_conversations(
                id VARCHAR(36) NOT NULL,
                created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                CONSTRAINT network_conv_id_idx UNIQUE INDEX (id) USING HASH
            ) CHARACTER SET utf8 COLLATE utf8_general_ci');

            $this->addSql('CREATE TABLE network_conversation_members(
                id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                conversation_id VARCHAR(36) NOT NULL,
                member_id VARCHAR(36) NOT NULL,
                UNIQUE INDEX (conversation_id, member_id) USING HASH,
                CONSTRAINT network_conversation_members_c_id_idx FOREIGN KEY (conversation_id) REFERENCES network_conversations(id)
            ) CHARACTER SET utf8 COLLATE utf8_general_ci');

            $this->addSql('CREATE TABLE network_conversation_messages(
                id VARCHAR(36) NOT NULL,
                conversation_id VARCHAR(36) NOT NULL,
                from_member VARCHAR(36) NOT NULL,
                message TEXT NOT NULL,
                created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                CONSTRAINT network_conv_msg_id_idx UNIQUE INDEX (id) USING HASH,
                CONSTRAINT network_conversation_messages_c_id_idx FOREIGN KEY (conversation_id) REFERENCES network_conversations(id)
            ) CHARACTER SET utf8 COLLATE utf8_general_ci');
        });
    }

    public function down(Schema $schema) : void
    {
        $this->connection->transactional(function () {
            $this->addSql('DROP TABLE network_conversation_messages');
            $this->addSql('DROP TABLE network_conversation_members');
            $this->addSql('DROP TABLE network_conversations');
        });
    }
}
