<?php

declare(strict_types=1);

namespace App\Container;

use Predis\Client;
use Predis\ClientInterface;

class RedisClientFactory
{
    public function __invoke(string $host, int $port): ClientInterface
    {
        return new Client([
            'scheme'=>'tcp',
            'host'=>$host,
            'port'=>$port,
        ]);
    }
}