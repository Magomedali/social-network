<?php

declare(strict_types=1);

namespace App\Container\Framework\Cache;

use Symfony\Component\Cache\Adapter\RedisAdapter;

class RedisAdapterFactory
{
    public function create($dsn): RedisAdapter
    {
        $connection = RedisAdapter::createConnection($dsn);
        return  new RedisAdapter($connection);
    }
}
