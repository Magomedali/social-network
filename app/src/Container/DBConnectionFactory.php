<?php

declare(strict_types=1);

namespace App\Container;

use Doctrine\DBAL\Connection;

class DBConnectionFactory
{

    public function create(string $dsn, string $username, string $password): Connection
    {
        return new Connection([
            'pdo'=> new \PDO($dsn,$username,$password)
        ]);
    }
}