<?php
declare(strict_types=1);
namespace App\Controller\Member;

use App\Model\Network\UseCase\Post\Create\Command;
use App\Model\Network\UseCase\Post\Create\Handler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PostsController extends AbstractController
{
    /**
     * @Route("/members/posts", name="members.posts.create", methods={"POST"})
     * @param Request $request
     * @param Handler $handler
     * @return Response
     * @throws \Exception
     */
    public function create(Request $request, Handler $handler): Response
    {
        $command = $this->unserialize($request);
        try {
            $handler->handle($command);
        } catch (\Exception $exception) {
            throw $exception;
        }

        return $this->redirectToRoute('members.show',['id'=>$command->target_user_id]);
    }

    private function unserialize(Request $request): Command
    {
        return new Command($request->get('target_user_id'),$this->getUser()->getId(),$request->get('content'));
    }
}