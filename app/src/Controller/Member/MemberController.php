<?php
declare(strict_types=1);
namespace App\Controller\Member;

use App\Model\Network\UseCase\Member\Follow\Command;
use App\Model\Network\UseCase\Member\Follow\Handler;
use App\ReadModel\Network\Post\PostFetcher;
use App\ReadModel\User\Filter\Filter;
use App\ReadModel\User\UserFetcher;
use Exception;
use http\Exception\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MemberController extends AbstractController
{
    const PER_PAGE = 50;

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/members", name="members")
     * @param Request $request
     * @param UserFetcher $fetcher
     * @return Response
     */
    public function index(Request $request, UserFetcher $fetcher): Response
    {
        $page = $request->query->getInt('page', 1);
        $filter = new Filter(
            $request->get('key', null),
            ($page * self::PER_PAGE) - self::PER_PAGE,
            self::PER_PAGE
        );
        $members = $fetcher->all($filter);

        return $this->render('app/member/list.html.twig',[
            'members'=>$members,
            'filter'=>$filter
        ]);
    }


    /**
     * @Route("/members/show", name="members.show")
     * @param Request $request
     * @param UserFetcher $userFetcher
     * @param PostFetcher $postFetcher
     * @return Response
     * @throws Exception
     */
    public function show(Request $request, UserFetcher $userFetcher, PostFetcher $postFetcher): Response
    {
        $member = $userFetcher->findById($request->query->get('id'));

        if(!$member)
        {
            throw new Exception('Member not found!');
        }

        $posts = $postFetcher->findByTargetUserId($member->id);

        return $this->render('app/member/show.html.twig',[
            'member'=>$member,
            'posts'=>$posts
        ]);
    }

    /**
     * @Route("/members/follow", name="members.follow", methods={"POST"})
     * @param Request $request
     * @param Handler $handler
     * @return Response
     * @throws Exception
     */
    public function follow(Request $request, Handler $handler): Response
    {
        $command = new Command();
        $command->member_target = $request->get('member_target');
        $command->follower = $this->getUser()->getId();
        
        try {
            if($command->member_target == $command->follower) {
                throw new \InvalidArgumentException('Can`t follow to self');
            }

            if(!$command->member_target || !$command->follower) {
                throw new \InvalidArgumentException('Missed required parameters');
            }

            $handler->handle($command);
            $this->addFlash('success', 'Now you following to new member');
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => $exception]);
            $this->addFlash('error', $exception->getMessage());
        }

        return $this->redirectToRoute('members.show',['id'=>$command->member_target]);
    }
}