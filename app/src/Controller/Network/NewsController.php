<?php
declare(strict_types=1);
namespace App\Controller\Network;

use App\Model\Network\Service\News\Exception\NotFoundNewsInCacheException;
use App\Model\Network\Service\News\NewsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="network.news")
     * @param NewsService $news
     * @return Response
     * @throws \App\Model\Network\Service\News\Exception\NotFoundNewsInCacheException
     */
    public function index(NewsService $news): Response
    {
        try {
            $news = $news->getNews($this->getUser()->getId());
        } catch (NotFoundNewsInCacheException $exception) {
            $news = [];
            $this->addFlash("error", "Ваша лента новостей формируется, пожалуйста подождите");
        }

        return $this->render('app/news.html.twig', ['news'=>$news]);
    }
}