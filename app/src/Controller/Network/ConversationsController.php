<?php
declare(strict_types=1);
namespace App\Controller\Network;

use App\Model\Network\Report\Member\MemberReportInterface;
use App\Model\Network\UseCase\Message\Send\Command;
use App\Model\Network\UseCase\Message\Send\Handler;
use App\ReadModel\Network\Conversation\ConversationReportInterface;
use App\ReadModel\Network\Message\MessageReportInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Network\UseCase\Conversation;
class ConversationsController extends AbstractController
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/conversations", name="network.conversations")
     * @return Response
     */
    public function index(Request $request, ConversationReportInterface $conversations): Response
    {
        $conversations = $conversations->findPersonalByMember($this->getUser()->getId());

        return $this->render('app/conversations.html.twig', [
            'conversations'=>$conversations
        ]);
    }

    /**
     * @Route("/coversation", name="network.conversation", methods={"POST","GET"})
     * @param Request $request
     * @param Conversation\Open\Handler $handler
     * @param MemberReportInterface $members
     * @return Response
     * @throws \Exception
     */
    public function coversation(Request $request, Conversation\Open\Handler $handler, MemberReportInterface $members, MessageReportInterface $messages): Response
    {
        $command = new Conversation\Open\Command($this->getUser()->getId(), $request->get('member_target'));
        try {
            $conversationId = $handler->handle($command);
        } catch (\Exception $exception) {
            throw $exception;
        }

        $member = $members->getViewById($request->get('member_target'));
        $messages = $messages->messagesByConversation($conversationId);
        return $this->render('app/conversation.html.twig',[
            'member'=>$member,
            'messages'=>$messages
        ]);
    }

    /**
     * @Route("/message", name="network.conversation.send", methods={"POST"})
     * @param Request $request
     * @param Handler $handler
     * @return Response
     * @throws \Exception
     */
    public function message(Request $request, Handler $handler)
    {
        $message = trim($request->get('message'));
        $to = $request->get('to');
        $command = new Command($this->getUser()->getId(), $to, $message);
        try {
            $handler->handle($command);
        } catch (\Exception $exception) {
            $this->addFlash('error', 'Error while sand message');
            $this->logger->error($exception->getMessage(),['exception'=>$exception]);
            throw $exception;
        }

        return $this->redirectToRoute('network.conversation',['member_target'=>$request->get('to')]);
    }
}