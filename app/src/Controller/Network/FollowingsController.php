<?php
declare(strict_types=1);
namespace App\Controller\Network;

use App\Model\Network\Report\Member\MemberReportInterface;
use App\Model\Network\Report\Member\Filter\FollowersFilter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FollowingsController extends AbstractController
{
    private $report;

    public function __construct(MemberReportInterface $report)
    {
        $this->report = $report;
    }

    /**
     * @Route("/followings", name="network.followings")
     * @return Response
     */
    public function index(): Response
    {
        $filter = new FollowersFilter($this->getUser()->getId());
        $followers = $this->report->findFollowings($filter);

        return $this->render('app/followings.html.twig', [
            'filter'=>$filter,
            'followers'=>$followers,
        ]);
    }
}