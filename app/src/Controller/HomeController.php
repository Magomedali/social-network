<?php
declare(strict_types=1);
namespace App\Controller;

use App\ReadModel\User\Filter\Filter;
use App\ReadModel\User\UserFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    const PER_PAGE = 50;

    /**
     * @Route("/", name="home")
     * @param Request $request
     * @param UserFetcher $fetcher
     * @return Response
     */
    public function index(Request $request, UserFetcher $fetcher): Response
    {
        $page = $request->query->getInt('page', 1);
        $filter = new Filter(
            $request->get('key', null),
            ($page * self::PER_PAGE) - self::PER_PAGE,
            self::PER_PAGE
        );
        $members = $fetcher->all($filter);

        return $this->render('app/home.html.twig',[
            'members'=>$members,
            'filter'=>$filter
        ]);
    }
}