<?php
declare(strict_types=1);

namespace App\Command\Member;

use App\Model\User\Entity\User\BirthDate;
use App\Model\User\Entity\User\City;
use App\Model\User\Entity\User\Email;
use App\Model\User\Entity\User\Gender;
use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\Interests;
use App\Model\User\Entity\User\LastName;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\Token;
use App\Model\User\Entity\User\User;
use App\Model\User\Repository\User\UserRepository;
use App\Model\User\Service\PasswordHasher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Faker\Factory;
use DateTimeImmutable;

class GenerateCommand extends Command
{
    private $users;
    private $hasher;
    private $faker;

    public function __construct(UserRepository $users,PasswordHasher $hasher)
    {
        $this->users = $users;
        $this->hasher = $hasher;
        $this->faker = Factory::create();
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('member:generate')
            ->setDescription('Generate members')
            ->addArgument('count', InputArgument::OPTIONAL, 'Count?')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $count = $input->getArgument('count') ?: 100;

        for ($i=0;$i<$count;$i++)
        {
            $user = new User(
                Id::next(),
                new Email($this->faker->email),
                new Name($this->faker->name),
                new LastName($this->faker->lastName),
                new City($this->faker->city),
                new Gender($this->faker->numberBetween(0,1)),
                $token = new Token($this->faker->words()),
                $this->hasher->hash($this->faker->password),
                new BirthDate(new DateTimeImmutable($this->faker->dateTimeBetween('-50 years','-18 years')->format('Y-m-d'))),
                new Interests($this->faker->text)
            );

            $user->confirmToken();

            $this->users->add($user);
        }

        $output->writeln('<info>Done!</info>');

        return 0;
    }
}