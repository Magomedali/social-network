<?php
declare(strict_types=1);

namespace App\Command;

use App\Model\User\Entity\User\BirthDate;
use App\Model\User\Entity\User\City;
use App\Model\User\Entity\User\Email;
use App\Model\User\Entity\User\Gender;
use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\Interests;
use App\Model\User\Entity\User\LastName;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\Token;
use App\Model\User\Entity\User\User;
use App\Model\User\Repository\User\UserRepository;
use App\Model\User\Service\PasswordHasher;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Faker\Factory;
use DateTimeImmutable;

class ShardHashKeyCommand extends Command
{
    const SIZE = 4;
    protected function configure(): void
    {
        $this
            ->setName('shard:hash:key')
            ->setDescription('Generate shard key')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = "6f77e92c-4543-40f5-a1a4-e9717ddc6cb3";//Uuid::uuid4()->toString();
        $hash = $this->hash($id);
        $shard = $this->getShardNumber($id);

        $output->writeln($hash);
        $output->writeln($shard);

        return 0;
    }

    protected function hash(string $key) {
        $md = md5($key);
        $bytes = unpack('C*', $md);

        return (($bytes[4] & 0xFF) << 24) | (($bytes[3] & 0xFF) << 16) | (($bytes[2] & 0xFF) << 8) | ($bytes[1] & 0xFF);
    }

    public function getShardNumber($key) {
        $hashId = $this->hash($key);
        return ($hashId % self::SIZE) + 1;
    }
}