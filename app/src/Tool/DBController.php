<?php

declare(strict_types=1);

namespace App\Tool;

use Doctrine\DBAL\Connection;

class DBController
{
    private $nodes;

    /**
     * DBController constructor.
     * @param Connection[] $nodes
     */
    public function __construct(array $nodes)
    {
        foreach ($nodes as $node) {
            $this->addShard($node);
        }
    }

    public function addShard(Connection $connection)
    {
        $this->nodes[] = $connection;
    }

    public function getShardNumber($key) {
        $hashId = $this->hash($key);
        return ($hashId % $this->getSize()) + 1;
    }

    public function getShardConnection($key): Connection
    {
        $num = $this->getShardNumber($key);
        return $this->nodes[$num-1];
    }

    protected function hash(string $key) {
        $md = md5($key);
        $bytes = unpack('C*', $md);

        return (($bytes[4] & 0xFF) << 24) | (($bytes[3] & 0xFF) << 16) | (($bytes[2] & 0xFF) << 8) | ($bytes[1] & 0xFF);
    }

    /**
     * @return Connection[]
     */
    public function nodes(): array
    {
        return $this->nodes;
    }

    public function getSize():int
    {
        return count($this->nodes);
    }
}