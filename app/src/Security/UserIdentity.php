<?php

declare(strict_types=1);

namespace App\Security;

use App\Model\User\Entity\User\User;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserIdentity implements UserInterface, EquatableInterface
{
    private $id;
    private $username;
    private $password;
    private $name;
    private $last_name;
    private $city;
    private $gender;
    private $birth_at;
    private $interests;
    private $role;
    private $status;

    public function __construct(
        string $id,
        string $username,
        string $password,
        string $name,
        string $last_name,
        string $city,
        int $gender,
        string $role,
        $status,
        ?string $birth_at = null,
        ?string $interests = null
    )
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->name = $name;
        $this->last_name = $last_name;
        $this->city = $city;
        $this->gender = $gender;
        $this->role = $role;
        $this->status = $status;
        $this->birth_at = $birth_at;
        $this->interests = $interests;
    }


    public function getId(): string
    {
        return $this->id;
    }

    public function getRoles(): array
    {
        return [$this->role];
    }


    public function getPassword(): string
    {
        return $this->password;
    }


    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getBirthAt(): ?string
    {
        return $this->birth_at;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }


    /**
     * @return string|null
     */
    public function getInterests(): ?string
    {
        return $this->interests;
    }


    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    public function isActive(): bool
    {
        return $this->status === User::STATUS_ACTIVE;
    }


    public function eraseCredentials(): void
    {

    }


    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof self) {
            return false;
        }

        return
            $this->id === $user->id &&
            $this->username === $user->username &&
            $this->password === $user->password &&
            $this->role === $user->role &&
            $this->status === $user->status;
    }
}