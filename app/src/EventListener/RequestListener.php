<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener
{
    public function __construct()
    {
    }

    public function onKernelRequest(RequestEvent $event)
    {
    }
}