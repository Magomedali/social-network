<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function __construct()
    {
    }

    public function onKernelException(ExceptionEvent $event)
    {
    }
}