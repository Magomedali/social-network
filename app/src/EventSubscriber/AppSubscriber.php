<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerArgumentsEvent;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AppSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest',10],
            KernelEvents::VIEW => 'onKernelView',
            KernelEvents::RESPONSE => ['onKernelResponse'],
            KernelEvents::CONTROLLER => ['onKernelController'],
            KernelEvents::CONTROLLER_ARGUMENTS => ['onKernelControllerArguments'],
            KernelEvents::TERMINATE => ['onKernelTerminate'],
            KernelEvents::FINISH_REQUEST => ['onKernelFinishRequest'],
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelRequest(RequestEvent $event)
    {
    }

    public function onKernelView(ViewEvent $event)
    {
    }

    public function onKernelResponse(ResponseEvent $event)
    {
    }

    public function onKernelController(ControllerEvent $event)
    {
    }

    public function onKernelControllerArguments(ControllerArgumentsEvent $event)
    {
    }

    public function onKernelTerminate(TerminateEvent $event)
    {
    }

    public function onKernelFinishRequest(FinishRequestEvent $event)
    {
    }

    public function onKernelException(ExceptionEvent $event)
    {
    }
}