<?php

declare(strict_types=1);

namespace App\EventSubscriber\Model\Network;

use App\Model\Network\Service\News\Event\NotFoundNewsInCacheEvent;
use App\Model\Network\Service\News\Producer\NewsProducerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NewsEventSubscriver implements EventSubscriberInterface
{

    /**
     * @var NewsProducerInterface
     */
    private $producer;

    public function __construct(NewsProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    public static function getSubscribedEvents()
    {
        return [
            NotFoundNewsInCacheEvent::class => 'onNotFoundNews'
        ];
    }

    public function onNotFoundNews(NotFoundNewsInCacheEvent $event)
    {
        $this->producer->produceEmptyNews($event->getMemeberId());
    }
}