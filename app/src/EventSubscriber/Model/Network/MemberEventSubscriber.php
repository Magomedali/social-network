<?php

declare(strict_types=1);

namespace App\EventSubscriber\Model\Network;

use App\Model\Network\Entity\Member\Event\MemberFollowingEvent;
use App\Model\Network\EventHandler\Member\MemberFollowingHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MemberEventSubscriber implements EventSubscriberInterface
{
    private $memberFollowingHandler;

    public function __construct(MemberFollowingHandler $memberFollowingHandler)
    {
        $this->memberFollowingHandler = $memberFollowingHandler;
    }

    public static function getSubscribedEvents()
    {
        return [
            MemberFollowingEvent::class => 'onMemberFollowingEvent'
        ];
    }

    public function onMemberFollowingEvent(MemberFollowingEvent $event)
    {
        try {
            $this->memberFollowingHandler->handle($event);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}