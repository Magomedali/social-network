<?php

declare(strict_types=1);

namespace App\EventSubscriber\Model\Network;

use App\Model\Network\Entity\Post\Event\NewPostEvent;
use App\Model\Network\EventHandler\Post\NewPostHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PostEventSubscriber implements EventSubscriberInterface
{
    private $newPostHandler;

    public function __construct(NewPostHandler $newPostHandler)
    {
        $this->newPostHandler = $newPostHandler;
    }

    public static function getSubscribedEvents()
    {
        return [
            NewPostEvent::class => 'onNewPostEvent'
        ];
    }

    public function onNewPostEvent(NewPostEvent $event)
    {
        try {
            $this->newPostHandler->handle($event);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}